#! /usr/bin/env bash

# Localstack based integration test
# See https://gitlab.com/crossref/integration
# Using the integration project we can set up all necessary dependencies using:
#
# ./integration.sh dev-manifold

export DB_URI="jdbc:postgresql://localhost:5432/nexus?user=nexus&password=nexus"
export AWS_ACCESS_KEY_ID="test"
export AWS_SECRET_ACCESS_KEY="test"
export AWS_REGION="us-east-1"
export UNIXML_AWS_ENDPOINT_OVERRIDE="http://localhost:4566"
export UNIXML_INGEST_S3_ENABLED=true
export UNIXML_QUEUE_URL="http://integration-localstack:4566/000000000000/manifold-mainq"
export UNIXML_DLQ_URL="http://integration-localstack:4566/000000000000/manifold-dlq"
export UNIXML_AUDIT_URL="http://integration-localstack:4566/000000000000/manifold-auditq"
export UNIXML_WAIT_TIME_SECONDS=10

mvn test-compile failsafe:integration-test failsafe:verify