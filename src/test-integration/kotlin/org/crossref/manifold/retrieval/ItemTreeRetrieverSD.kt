package org.crossref.manifold.retrieval

import com.fasterxml.jackson.databind.ObjectMapper
import io.cucumber.datatable.DataTable
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.Statements
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.registries.AuthorityRegistry
import org.junit.jupiter.api.Assertions.assertTrue
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.net.URI


@CucumberContextConfiguration
@SpringBootTest
@Import(ApiResponseContext.Configuration::class)
class ItemTreeRetrieverSD(
    val authorityRegistry: AuthorityRegistry,
    private val apiResponseContext: ApiResponseContext,
    private val mapper: ObjectMapper
) {

    @Given("the following parties are authority roots")
    fun the_following_parties_are_authority_roots(dataTable: DataTable) {
        // The simplest thing to do is let the default file load and then assert that it contains what the test
        // expects. This way the real authority trust list is used.
        dataTable.asList().map { expectedIdentifierUri ->
            assertTrue(authorityRegistry.authorityRoots.any { item ->
                item.identifiers.any { identifier ->
                    identifier.uri == expectedIdentifierUri
                }
            }, "Expected to find $expectedIdentifierUri as authority root in ${authorityRegistry.authorityRoots}")
        }
    }

    @Given("the following parties are authorities")
    fun the_following_parties_are_authorities(dataTable: DataTable) {
        dataTable.asList().map { expectedIdentifierUri ->
            assertTrue(
                authorityRegistry.authorities.any { it.identifiers.any { it.uri == expectedIdentifierUri } },
                "Expected to find $expectedIdentifierUri as authority in ${authorityRegistry.authorities}"
            )
        }
    }

    @When("a user requests an item tree for {string} with {string} perspective")
    fun the_item_tree_for_is_retrieved_with_perspective(identifier: String, perspective: String) {
        // Use a URI instead of a string because the URL will include double-slashes which would get filtered out otherwise.
        // The port number isn't significant, so it's given as 1234 here.
        apiResponseContext.perform(get(URI("http://localhost:1234/v2/perspective/$perspective/item-tree/$identifier")))
    }

    @When("a user requests an item tree for {string} with default perspective")
    fun a_user_requests_an_item_tree_for_with_default_perspective(identifier: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234/v2/item-tree/$identifier")))
    }

    @When("a user requests an item tree for non-prefixed {string} with default perspective")
    fun a_user_requests_an_item_tree_for_non_prefixed_with_default_perspective(identifier: String) {
        apiResponseContext.perform(get(URI("http://localhost:1234/v2/item-tree/$identifier")))
    }

    @Then("the item tree should contain the following relationship statements")
    fun the_item_tree_should_contain_the_following_relationship_statements(statements: List<Statements.RelationshipStatement>) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn()

        val tree = mapper.readValue(result.response.contentAsString, Item::class.java)
        for (statement in statements) {
            assertTrue(
                tree.containsTriple(
                    statement.subjectId,
                    statement.relationshipType,
                    statement.objectId,
                    statement.assertedBy
                ),
                "Expected to find relationship $statement in tree $tree."
            )
        }
    }

    @Then("the item tree should contain the following property statements")
    fun the_item_tree_should_contain_the_following_property_statements(statements: List<Statements.PropertyStatement>) {
        val result = apiResponseContext.andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andReturn()

        val tree = mapper.readValue(result.response.contentAsString, Item::class.java)
        for (statement in statements) {
            assertTrue(
                tree.containsProperty(
                    statement.subjectId,
                    statement.property,
                    statement.value,
                    statement.assertedBy
                ),
                "Expected to find property $statement in tree $tree."
            )
        }
    }

    @Then("resource {string} is moved permanently")
    fun resource_is_moved_permanently(identifier: String) {
        apiResponseContext
            .andExpect(status().isMovedPermanently)
            .andExpect(
                header().string(
                    HttpHeaders.LOCATION,
                    "/v2/item-tree/https://doi.org/$identifier"
                )
            )
    }
}
