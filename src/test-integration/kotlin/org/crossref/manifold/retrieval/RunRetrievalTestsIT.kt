package org.crossref.manifold.retrieval

import io.cucumber.junit.platform.engine.Constants
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite

@Suite
@SelectClasspathResource("features/retrieval")
@ConfigurationParameter(
    key = Constants.GLUE_PROPERTY_NAME,
    value = "org.crossref.manifold.retrieval,org.crossref.manifold.common"
)
class RunRetrievalTestsIT
