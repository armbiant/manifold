package org.crossref.manifold.relationships

import com.fasterxml.jackson.databind.ObjectMapper
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.api.RelationshipListView
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.Statements
import org.junit.jupiter.api.Assertions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@CucumberContextConfiguration
@SpringBootTest
@Import(ApiResponseContext.Configuration::class)
class RelationshipsSD(private val apiResponseContext: ApiResponseContext, private val mapper: ObjectMapper) {
    @When("relationship assertions for identifier {string} are requested")
    fun relationship_assertions_for_doi_are_requested(identifier: String) {
        apiResponseContext.perform(get("/v2/relationships/").param("uri", identifier))
    }

    @Then("the following relationship assertions are returned from the item graph")
    fun the_following_relationship_assertions_are_returned_from_the_item_graph(statements: List<Statements.RelationshipStatement>) {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipListView =
            mapper.readValue(apiResponseContext.andReturn().response.contentAsString, RelationshipListView::class.java)
        Assertions.assertEquals(statements.size, relationshipListView.values.size)
        statements.forEach { Assertions.assertTrue(relationshipListContainsStatementOnce(relationshipListView, it)) }
    }

    @Then("no relationship assertions are returned from the item graph")
    fun no_relationship_assertions_are_returned_from_the_item_graph() {
        apiResponseContext.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk).andExpect(
            content().contentType(MediaType.APPLICATION_JSON)
        )
        val relationshipListView =
            mapper.readValue(apiResponseContext.andReturn().response.contentAsString, RelationshipListView::class.java)
        Assertions.assertTrue(relationshipListView.values.isEmpty())
    }

    private fun relationshipListContainsStatementOnce(
        relationshipListView: RelationshipListView,
        relationshipStatement: Statements.RelationshipStatement
    ): Boolean =
        relationshipListView.values.count {
            it.fromItem.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.subjectId)
                    && it.toItem.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.objectId)
                    && it.assertingItem.identifiers.map { itemIdentifierView -> itemIdentifierView.identifier }
                .contains(relationshipStatement.assertedBy)
                    && it.assertedAt == relationshipStatement.assertedAt.toString()
                    && it.relationshipTypeName == relationshipStatement.relationshipType
        } == 1
}
