package org.crossref.manifold.common

import io.cucumber.java.DataTableType
import org.crossref.manifold.retrieval.ItemTreeRetrieverSD
import java.time.OffsetDateTime

/**
 * Statements are used in a lot of places: ingestion, retrieval, archives, Given, Then, etc.
 * These classes can be used to parse data tables.
 */
object Statements {
    private const val SUBJECT_ID = "Subject ID"

    // When this is a relationship, the object ID.
    private const val OBJECT_ID = "Object ID"

    // When this is a relationship, the relationship type.
    private const val RELATIONSHIP_TYPE = "Relationship Type"

    // When this is a property, the property type.
    private const val PROPERTY = "Property"

    // When this is a property, the property value.
    private const val VALUE = "Value"

    private const val ASSERTED_AT = "Asserted At"
    private const val ASSERTED_BY = "Asserted By"

    private const val CURRENT = "Current"
    private const val COUNT = "Count"


    /**
     * Get asserted at, or now.
     */
    fun assertedAt(statement: Map<String, String>) : OffsetDateTime =
        statement[ASSERTED_AT]?.let {
            OffsetDateTime.parse(statement[ASSERTED_AT])
        } ?: OffsetDateTime.now()

    /**
     * Get count or 1.
     */
    private fun count(statement: Map<String, String>) =
        statement[COUNT]?.toInt() ?: 1


    data class RelationshipStatement(
        val assertedBy: String,
        val subjectId: String,
        val relationshipType: String,
        val objectId: String,
        val assertedAt: OffsetDateTime,

        /**
         * Optional, the number of assertions expected.
         */
        val count: Int = 1,

        /**
         * Optional, whether the assertions are expected to be current.
         */
        val current: Boolean = true
    )

    @DataTableType
    fun relationshipStatement(line: Map<String, String>): RelationshipStatement = RelationshipStatement(
        line[ASSERTED_BY]!!,
        line[SUBJECT_ID]!!,
        line[RELATIONSHIP_TYPE]!!,
        line[OBJECT_ID]!!,
        assertedAt(line),
        count(line),
        line[CURRENT] == "true"
    )

    data class PropertyStatement(
        val assertedBy: String,
        val subjectId: String,
        val property: String,
        val value: String,
        val assertedAt: OffsetDateTime,

        /**
         * Optional, the number of assertions expected.
         */
        val count: Int = 1,

        /**
         * Optional, whether the assertions are expected to be current.
         */
        val current: Boolean = true
    )

    @DataTableType
    fun propertyStatement(line: Map<String, String>): PropertyStatement = PropertyStatement(
        line[ASSERTED_BY]!!,
        line[SUBJECT_ID]!!,
        line[PROPERTY]!!, line[VALUE]!!,
        assertedAt(line),
        count(line),
        line[CURRENT] == "true"
    )
}
