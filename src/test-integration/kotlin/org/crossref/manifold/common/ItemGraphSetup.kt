package org.crossref.manifold.common

import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.springframework.jdbc.core.JdbcTemplate

class ItemGraphSetup(
    private val itemGraph: ItemGraph,
    private val jdbcTemplate: JdbcTemplate
) {
    companion object {
        private val PROVENANCE = EnvelopeBatchProvenance("Crossref Test", "1234")
    }

    // By convention, scenarios start with an empty item graph.
    @Before
    fun truncateItemGraphTables() {
        jdbcTemplate.execute("TRUNCATE TABLE envelope_batch RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item RESTART IDENTITY CASCADE")
    }

    @Given("the following relationship assertions in the item graph")
    fun the_following_relationship_assertions_in_the_item_graph(statements: List<Statements.RelationshipStatement>) {
        val envelopes = statements.map { statement ->
            val item = Item()
                .withIdentifier(Identifier(statement.subjectId))
                .withRelationship(
                    Relationship(
                        statement.relationshipType,
                        Item().withIdentifier(Identifier(statement.objectId))
                    )
                )

            val assertion = ItemTreeAssertion(
                statement.assertedAt,
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(statement.assertedBy))
            )

            Envelope(listOf(item), assertion)
        }

        itemGraph.assert(
            EnvelopeBatch(
                envelopes, PROVENANCE
            )
        )
    }

    @Given("the following property assertions in the item graph")
    fun the_following_property_assertions_in_the_item_graph(statements: List<Statements.PropertyStatement>) {

        val envelopes = statements.map { statement ->
            val item = Item()
                .withIdentifier(Identifier(statement.subjectId))
                .withPropertiesFromMap(mapOf(statement.property to statement.value))

            val assertion = ItemTreeAssertion(
                statement.assertedAt,
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(statement.assertedBy))
            )

            Envelope(listOf(item), assertion)
        }

        itemGraph.assert(
            EnvelopeBatch(
                envelopes, PROVENANCE
            )
        )
    }

    /**
     * This one specifically asserts all the property values in one assertion.
     */
    @Given("the following multi-valued property assertion in the item graph")
    fun the_following_multi_property_assertions_in_the_item_graph(statements: List<Statements.PropertyStatement>) {

        Assert.assertTrue(
            "All statements must be asserted by the same party",
            statements.map { it.assertedBy }.toSet().count() == 1
        )

        Assert.assertTrue(
            "All subject ids should be the same",
            statements.map { it.subjectId }.toSet().count() == 1
        )

        val assertedBy = statements.first().assertedBy
        val subjectId = statements.first().subjectId
        val assertedAt = statements.first().assertedAt

        val allProperties = statements.map { it.property to it.value }.toMap()
        val item = Item()
            .withIdentifier(Identifier(subjectId))
            .withPropertiesFromMap(allProperties)

        val assertion =
            ItemTreeAssertion(assertedAt, MergeStrategy.NAIVE, Item().withIdentifier(Identifier(assertedBy)))

        itemGraph.assert(
            EnvelopeBatch(
                listOf(Envelope(listOf(item), assertion)), PROVENANCE
            )
        )
    }


    /**
     * This Given doesn't do anything other than serve as a placeholder to make tests clear when there is no prior data
     * in the item graph.
     * The clearing is done by [truncateItemGraphTables].
     */
    @Given("an empty Item Graph database")
    fun empty_item_graph() = Unit


    @Then("the following relationship statements are found in the item graph")
    fun the_item_graph_contains_non_current_relationship_statements(
        expectedStatements: List<Statements.RelationshipStatement>
    ) {
        expectedStatements.forEach {
            val foundStatements = itemGraph.getRelationshipStatements(
                Identifier(it.subjectId),
                it.relationshipType,
                Identifier(it.objectId),
                Identifier(it.assertedBy),
                current = it.current,
                state = true
            )

            Assertions.assertEquals(it.count, foundStatements.count())
        }
    }

    @Then("the following property statements are found in the item graph")
    fun the_item_graph_contains_non_current_property_statements(
        expectedStatements: List<Statements.PropertyStatement>
    ) {
        expectedStatements.forEach { statement ->
            val foundStatements = itemGraph.getPropertyStatements(
                Identifier(statement.subjectId),
                Identifier(statement.assertedBy),
                statement.current
            )
            val matching = foundStatements.filter {
                it.values.get(statement.property)?.asText() == statement.value
            }

            Assertions.assertEquals(statement.count, matching.count())
        }
    }

}
