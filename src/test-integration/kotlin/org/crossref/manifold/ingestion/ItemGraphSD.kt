package org.crossref.manifold.ingestion

import io.cucumber.java.en.Then
import io.cucumber.java.en.When

import org.crossref.manifold.common.ItemGraphSetup
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.junit.jupiter.api.Assertions.assertNotNull
import org.springframework.beans.factory.annotation.Autowired
import org.junit.Before

import java.time.OffsetDateTime

class ItemGraphSD(
    @Autowired
    val itemGraph: ItemGraph,

    @Autowired
    val itemGraphSetup: ItemGraphSetup
) {
    @Before
    fun truncateItemGraphTables(){
        itemGraphSetup.truncateItemGraphTables()
    }

    @When("{string} asserts relationship {string} {string} {string} in the Item Graph with {string} merge")
    fun asserts_relationship(assertedBy: String, subj: String, relationship: String, obj: String, merge: String) {
        val mergeStrategy = getMergeStrategy(merge)

        assertNotNull(mergeStrategy, "Not recognised merge strategy")

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(subj))
                    .withRelationship(
                        Relationship(
                            relationship, Item()
                                .withIdentifier(Identifier(obj))
                        )
                    )
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(), mergeStrategy,
                Item().withIdentifier(Identifier(assertedBy))
            )
        )


        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234")
        )

        itemGraph.assert(envelopeBatch)
    }

    @When("{string} asserts property {string} {string}: {string} in the Item Graph with {string} merge")
    fun asserts_property(assertedBy: String, subj: String, property: String, value: String, merge: String) {
        val mergeStrategy = getMergeStrategy(merge)

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(subj))
                    .withPropertiesFromMap(mapOf(property to value))
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                mergeStrategy,
                Item().withIdentifier(Identifier(assertedBy))
            )
        )

        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234")
        )

        itemGraph.assert(envelopeBatch)
    }

    private fun getMergeStrategy(merge: String): MergeStrategy {
        val mergeStrategy = when (merge) {
            "naive" -> MergeStrategy.NAIVE
            "none" -> MergeStrategy.NONE
            "union-closed" -> MergeStrategy.UNION_CLOSED
            "closed" -> MergeStrategy.CLOSED
            else -> null
        }
        assertNotNull(mergeStrategy, "Not recognised merge strategy")
        return mergeStrategy!!
    }

    @Then("the Item Graph contains Item {string}")
    fun the_item_graph_contains_item(identifier: String) {

        val result = itemGraph.resolver.resolveRO(Identifier(identifier))

        assertNotNull(result.pk, "Expected to  find Item $identifier in the Item Graph")
    }
}