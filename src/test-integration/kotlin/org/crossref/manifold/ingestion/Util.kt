package org.crossref.manifold.ingestion

import io.cucumber.java.ParameterType
import org.junit.Assert
import org.springframework.stereotype.Component
import java.io.File

class Util {
    @ParameterType("current|non-current")
    fun current(value: String): Boolean =
        when (value) {
            "current" -> true
            "non-current" -> false
            else -> {
                Assert.fail("Expected 'current' or 'non-current'")
                false
            }
        }
}

@Component
/**
 * Temporary file to share between step definitions.
 * These are deleted automatically on exit.
 */
class TempFile {
    val files = mutableMapOf<String, File>()

    /** Get a temporary file with the name. Create if needed
     */
    operator fun get(name: String): File =

        files[name] ?: run {
            val newFile = File.createTempFile("/tmp", name)
            files[name] = newFile
            newFile
        }
}