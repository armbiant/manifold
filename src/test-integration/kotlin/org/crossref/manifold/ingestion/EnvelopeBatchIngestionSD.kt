package org.crossref.manifold.ingestion

import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import io.cucumber.spring.CucumberContextConfiguration
import org.crossref.manifold.bulk.EnvelopeBatchArchiveIngester
import org.crossref.manifold.bulk.EnvelopeBatchArchiveWriter
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.junit.jupiter.api.TestInstance
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.io.File
import java.time.OffsetDateTime

@CucumberContextConfiguration
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EnvelopeBatchIngestionSD {
    @Autowired
    lateinit var ingester: EnvelopeBatchArchiveIngester

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    // This is used for constructing entries in a bulk file.
    var envelopesInBatch = mutableListOf<EnvelopeBatch>()

    var currentFile: File? = null

    @Autowired
    lateinit var tempFiles: TempFile

    @Given("a bulk import file called {string}")
    fun a_bulk_import_file(name: String) {
        // This step gets a bulk file ready.
        envelopesInBatch.clear()
        currentFile = tempFiles.get(name)
    }

    @Given("{string} asserts relationship {string} {string} {string} in the bulk file")
    fun the_bulk_file_asserts_relationship(
        assertedBy: String,
        subj: String,
        relationship: String,
        obj: String
    ) {

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(subj))
                    .withRelationship(
                        Relationship(
                            relationship, Item()
                                .withIdentifier(Identifier(obj))
                        )
                    )
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(), MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(assertedBy))
            )
        )


        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234")
        )

        envelopesInBatch.add(envelopeBatch)
    }

    @Given("{string} asserts property {string} {string}: {string} in the bulk file")
    fun the_bulk_file_asserts_property(
        assertedBy: String, subj: String,
        property: String,
        value: String,
    ) {

        val envelope = Envelope(
            listOf(
                Item().withIdentifier(Identifier(subj))
                    .withPropertiesFromMap(mapOf(property to value))
            ),
            ItemTreeAssertion(
                OffsetDateTime.now(),
                MergeStrategy.NAIVE,
                Item().withIdentifier(Identifier(assertedBy))
            )
        )


        val envelopeBatch = EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance("Crossref Test", "1234")
        )

        envelopesInBatch.add(envelopeBatch)
    }

    /**
     * Write the bulk file we've been accumulating.
     */
    fun writeBulkFile(file: File) {
        EnvelopeBatchArchiveWriter(file).use {
            envelopesInBatch.forEachIndexed { i, envelopeBatch ->
                it.add("TEST-$i.XML", listOf(envelopeBatch))
            }
        }

        logger.info("Written temporary bulk XML file: ${file.absolutePath} ${file.name}")
    }

    @When("the bulk import is triggered for {string}")
    fun the_bulk_import_is_triggered(filename: String) {
        val file = tempFiles[filename]
        writeBulkFile(file)
        ingester.ingest(listOf(file))
    }
}