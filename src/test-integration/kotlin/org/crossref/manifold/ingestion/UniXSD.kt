package org.crossref.manifold.ingestion

import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import org.apache.commons.io.IOUtils
import org.crossref.manifold.bulk.EnvelopeBatchArchiveIngester
import org.crossref.manifold.bulk.TarGzWriter
import org.crossref.manifold.modules.unixml.UniXMLSnapshotConverter
import org.crossref.manifold.modules.unixml.XmlSingleIngester
import org.junit.jupiter.api.Assertions
import org.springframework.beans.factory.annotation.Autowired
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

fun resource(filename: String) = Thread.currentThread().contextClassLoader
    .getResourceAsStream("xml/$filename")!!

/** Does the given XML contain the XPath?)
 */
fun hasXpath(filename: String, xPath: String): Boolean {
    resource(filename).use {
        val factory = DocumentBuilderFactory.newInstance()
        val builder = factory.newDocumentBuilder()
        val document = builder.parse(it)
        val xpath: XPath = XPathFactory.newInstance().newXPath()
        val result = xpath.evaluate(xPath, document, XPathConstants.NODE)
        return result != null
    }
}

/**
 * The Given step definitions here refer to an existing file in the test resources directory, which we expect to be
 * ready-made for the test. They don't construct the XML file, but they do verify the content to be sure that it says
 * what we expected it to say.
 */
class UniXML(
    @Autowired
    val ingester: XmlSingleIngester,

    @Autowired
    var archiveIngester: EnvelopeBatchArchiveIngester,

    val tempFile: TempFile
) {
    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Journal DOI {string}")
    fun xml_file_contains_journal(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/journal/journal_metadata/doi_data/doi[text()='$doi']"),
            "Expected Journal DOI $doi"
        )
    }

    /**
     * DOI is expressed without resolver (e.g. 10.5555/12345678) in the XML.
     */
    @Given("UniXSD file {string} registers Article DOI {string}")
    fun xml_file_contains_article_doi(filename: String, doi: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//doi_record/crossref/journal/journal_article/doi_data/doi[text()='$doi']"),
            "Expected Article DOI $doi"
        )
    }

    @Given("UniXSD file {string} has crm-item with Member ID {string}")
    fun xml_file_has_crm_item_with_member_id(filename: String, memberId: String) {
        Assertions.assertTrue(
            hasXpath(filename, "//crm-item[@name='member-id'][text()='$memberId']"),
            "Expected to find member id $memberId"
        )
    }

    @When("UniXSD file {string} is ingested")
    fun the_xml_file_is_processed_by_the_uni_xml_snapshot_parser(filename: String) {
        ingester.ingest(filename, resource(filename).reader().use { it.readText() })
    }


    @Given("an XML Snapshot file named {string} containing XML file {string}")
    fun an_xml_snapshot_file_named_containing(snapshotFilename: String, xmlFilename: String) {
        // Just create a tar file with a single entry.
        TarGzWriter(tempFile[snapshotFilename]).use {
            it.add(xmlFilename) { outputStream ->
                IOUtils.copy(resource(xmlFilename), outputStream)
            }
        }
    }

    @When("XML Snapshot file {string} is converted to a bulk file {string} and the bulk file is ingested")
    fun the_xml_snapshot_file_is_converted_to_a_bulk_file_and_the_bulk_file_is_ingested(
        snapshotFilename: String,
        bulkFilename: String
    ) {
        UniXMLSnapshotConverter.run(tempFile[snapshotFilename], tempFile[bulkFilename])
        archiveIngester.ingest(listOf(tempFile[bulkFilename]))
    }

}