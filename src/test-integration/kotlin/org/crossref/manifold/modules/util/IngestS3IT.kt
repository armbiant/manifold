package org.crossref.manifold.modules.util

import org.crossref.manifold.itemgraph.IdentifierPkItemPk
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemtree.Tokenized
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.env.Environment
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.PutObjectRequest


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class IngestS3IT {
    @Autowired
    lateinit var env: Environment

    @Autowired
    lateinit var itemDao: ItemDao

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Autowired
    lateinit var s3client: S3Client

    @Test
    fun testIngestS3() {

        val objectRequest = PutObjectRequest.builder()
            .bucket("md-bucket")
            .key("test.xml")
            .build()

        val resourceStream = Thread.currentThread().contextClassLoader
            .getResourceAsStream("test.xml")

        val requestBody = RequestBody
            .fromString(resourceStream.reader().use { it.readText() })

        logger.info("Posting file to S3...")
        s3client.putObject(objectRequest, requestBody)


        var response: IdentifierPkItemPk?
        var retries = 0
        do {
            logger.info("Searching for identifier...")
            response = itemDao.findItemAndIdentifierPk(Tokenized("doi", "10.1002/jnr.23992"))
            if (response == null) {
                Thread.sleep(500)
            }
        } while (response == null && ++retries < 30)

        assertNotNull(response)

    }

}