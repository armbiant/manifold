Feature: UniXML and Maintaining Organizations

  UniXML is the data format that Crossref Members use to send us metadata, and the format we expose in our "XML APIs". It is currently our main source of truth as asserted by our members. It is also converted by Cayenne parser into Item Trees for ingestion into the /works endpoint of the REST API.

  The UniXML is wrapped in UniXSD, which is a container that includes extra information from Content System. For the purpose of this feature, the only part of this UniXSD wrapper we're interested in is the "crm-item" element with the name "member-id". This "member-id" field indicates which Crossref Member we believe the given UniXSD record is associated with. As the "crm-items" data comes from a different source, and has different provenance to the UniXML it wraps, it can change independent of the UniXML. Note that a given UniXML record can register and represent metadata about multiple DOIs (e.g. journal titles, articles, components, etc).

  UniXML is ingested into the Item Graph in two contexts. Initially we do a bulk load of a snapshot of the whole corpus. After that, Content System sends XML files on a continual basis whenever then are updated. These two timescales involve different contexts:

  - The initial bulk load is done from a snapshot that represents what we know at a certain point in time, including the member ID associated with the DOIs.
  - The continual load is done thereafter. In this time-frame, the XML content can change independent of the member IDs associated with them.

  We can assume that the member crm-item information is correctly represented at the time of snapshot creation. We also know that, due to the "Work to Member Relationships" feature in Content System, the XML is always re-pushed whenever this connection changes.

  Scenario: Ingestion asserts Items and Maintainer

    Given UniXSD file "silly-string.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "silly-string.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string.xml" has crm-item with Member ID "7822"
    When UniXSD file "silly-string.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                         | Relationship Type | Object ID                           |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/member/7822 |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/member/7822 |

  Scenario: Ownership can change

  If the member ID changes, the new stewardship is asserted by Crossref, and the old maintainer is marked as historical and no longer current.

  We check that the DOI has the new current stewardship, that it no longer has the old one. We also verify that the previous maintainership is still present as non-current in the historical record.

  This is applied to both DOIs registered in the XML file.

    Given the following relationship assertions in the item graph
      | Asserted By               | Subject ID                         | Relationship Type | Object ID                           |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/member/7822 |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/member/7822 |
    Given UniXSD file "silly-string-moved.xml" registers Journal DOI "10.5555/1234567890"
    And UniXSD file "silly-string-moved.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string-moved.xml" has crm-item with Member ID "99999"
    And UniXSD file "silly-string-moved.xml" is ingested
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                         | Relationship Type | Object ID                            |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/member/99999 |
      | 0     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/member/7822  |
      | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/1234567890 | steward           | https://id.crossref.org/member/7822  |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/member/99999 |
      | 0     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/member/7822  |
      | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678   | steward           | https://id.crossref.org/member/7822  |

