Feature: Envelope Batch Bulk Ingestion
  The Item Graph is constructed from data from several sources via Envelope Batches. In many cases each set of envelope batches comprises a particular corpus, e.g.:

  - The corpus of XML documents
  - The corpus of Event Data Events
  - The corpus of Crossref Members

  At runtime this corpus may be continually updated and ingested. But at the start of an Item Graph deployment (i.e. once in the lifetime of an instance, or for test deployment) we want to ingest a whole corpus in one go. For this the primary aim is the fastest possible ingestion.

  The historical data can be very large (terabytes) and conventional ingestion would take an impractical amount
  of time. Therefore the bulk data load is a specific process optimised for one-time importing.

  Scenario: Bulk import works on an empty Item Graph database
  An empty Item Graph database is the starting point for the first bulk ingestion. A bulk ingestion should be able to create all of the Items and Statements from scratch.

    Given an empty Item Graph database
    And a bulk import file called "bulk.tar.gz"
    And "https://ror.org/02twcfp32" asserts relationship "example.com" "discusses" "https://doi.org/10.5555/12345678" in the bulk file
    And "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the bulk file
    When the bulk import is triggered for "bulk.tar.gz"
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
      | 1     | true    | https://ror.org/02twcfp32 | example.com | discusses         | https://doi.org/10.5555/12345678 |
      | 0     | false   | https://ror.org/02twcfp32 | example.com | discusses         | https://doi.org/10.5555/12345678 |
    And the following property statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
      | 0     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |

  Scenario: Repeated imports should result in no new Items, but repeated statements are asserted.
  If a snapshot file is ingested twice, all assertions should be made twice, according to the merge strategy.

    Given the following relationship assertions in the item graph
      | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
      | https://ror.org/02twcfp32 | example.com | discusses         | https://doi.org/10.5555/12345678 |
    And the following property assertions in the item graph
      | Asserted By               | Subject ID                       | Property | Value          |
      | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
    And a bulk import file called "bulk.tar.gz"
    And "https://ror.org/02twcfp32" asserts relationship "example.com" "discusses" "https://doi.org/10.5555/12345678" in the bulk file
    And "https://ror.org/02twcfp32" asserts property "https://doi.org/10.5555/12345678" "title": "Psychoceramics" in the bulk file
    When the bulk import is triggered for "bulk.tar.gz"
    Then the following relationship statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID  | Relationship Type | Object ID                        |
      | 1     | true    | https://ror.org/02twcfp32 | example.com | discusses         | https://doi.org/10.5555/12345678 |
      | 1     | false   | https://ror.org/02twcfp32 | example.com | discusses         | https://doi.org/10.5555/12345678 |
    And the following property statements are found in the item graph
      | Count | Current | Asserted By               | Subject ID                       | Property | Value          |
      | 1     | true    | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |
      | 1     | false   | https://ror.org/02twcfp32 | https://doi.org/10.5555/12345678 | title    | Psychoceramics |