Feature: Get DOI Relationships

  Query an API for a DOI and return all relationships where that DOI is the subject or object.
  The query should use the current statement (i.e. latest by statement created date) asserting the relationship.
  Assertions are considered to be versions of the same assertion if the Subject IDs, Relationship Types, Object IDs and Asserting IDs are the same.

  Scenario: Query for DOI returns current versions of assertions where DOI is subject or object

    Given the following relationship assertions in the item graph
      | Subject ID                           | Relationship Type | Object ID                        | Asserted At          | Asserted By                 |
      | https://doi.org/10.5555/12345678     | author            | https://orcid.org/120398476      | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3    |
      | https://doi.org/10.5555/12345678     | references        | https://doi.org/10.5555/ABCDEF   | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3    |
      | https://doi.org/10.5555/12345678     | references        | https://doi.org/10.5555/ABCDEF   | 2021-11-11T12:00:00Z | https://id.crossref.org/100 |
      | https://doi.org/10.3390/atoms1010001 | references        | https://doi.org/10.5555/12345678 | 2021-11-13T12:00:00Z | https://id.crossref.org/100 |
      | https://doi.org/10.3390/atoms1010001 | references        | https://doi.org/10.5555/12345678 | 2021-11-15T12:00:00Z | https://id.crossref.org/100 |

    When relationship assertions for identifier "https://doi.org/10.5555/12345678" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                           | Relationship Type | Object ID                        | Asserted At          | Asserted By                 |
      | https://doi.org/10.5555/12345678     | author            | https://orcid.org/120398476      | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3    |
      | https://doi.org/10.5555/12345678     | references        | https://doi.org/10.5555/ABCDEF   | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3    |
      | https://doi.org/10.5555/12345678     | references        | https://doi.org/10.5555/ABCDEF   | 2021-11-11T12:00:00Z | https://id.crossref.org/100 |
      | https://doi.org/10.3390/atoms1010001 | references        | https://doi.org/10.5555/12345678 | 2021-11-15T12:00:00Z | https://id.crossref.org/100 |
