Feature: Get Non-DOI Identifier Relationships

  Query the relationships API for identifiers of varying non-DOI types and return all relationships where those identifiers are the subject or object.

  Background:

    Given the following relationship assertions in the item graph
      | Subject ID                            | Relationship Type | Object ID                             | Asserted At          | Asserted By                        |
      | 10.1364/josab.476006                  | author            | https://orcid.org/0000-0003-1527-4533 | 2022-12-19T20:34:03Z | https://id.crossref.org/member/285 |
      | https://orcid.org/0000-0003-1527-4533 | affiliation       | https://ror.org/04ct4d772             | 2022-12-19T20:34:03Z | https://id.crossref.org/member/285 |
      | hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg  | discusses         | 10.1016/j.celrep.2022.111934          | 2023-01-02T11:17:03Z | https://ror.org/02twcfp3           |

  Scenario: Report relationship assertions for an ORCID

    When relationship assertions for identifier "https://orcid.org/0000-0003-1527-4533" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                            | Relationship Type | Object ID                             | Asserted At          | Asserted By                        |
      | 10.1364/josab.476006                  | author            | https://orcid.org/0000-0003-1527-4533 | 2022-12-19T20:34:03Z | https://id.crossref.org/member/285 |
      | https://orcid.org/0000-0003-1527-4533 | affiliation       | https://ror.org/04ct4d772             | 2022-12-19T20:34:03Z | https://id.crossref.org/member/285 |

  Scenario: Report relationship assertions for a ROR ID

    When relationship assertions for identifier "https://ror.org/04ct4d772" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                            | Relationship Type | Object ID                 | Asserted At          | Asserted By                        |
      | https://orcid.org/0000-0003-1527-4533 | affiliation       | https://ror.org/04ct4d772 | 2022-12-19T20:34:03Z | https://id.crossref.org/member/285 |

  Scenario: Report relationships for a URL

    When relationship assertions for identifier "hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                           | Relationship Type | Object ID                    | Asserted At          | Asserted By              |
      | hypothes.is/a/FMqYpoo2Ee2H3yuF69ckYg | discusses         | 10.1016/j.celrep.2022.111934 | 2023-01-02T11:17:03Z | https://ror.org/02twcfp3 |
