Feature: Handle reified relationships for blank nodes by following object relationships

  Many relationships in the item graph are reified. The relationships endpoint should show the 'start' and 'end' points of the reification, without the blank node that holds properties of the relationship.

  Background:

    Given the following relationship assertions in the item graph
      | Subject ID                                       | Relationship Type | Object ID                               | Asserted At          | Asserted By                          |
      #Crossref-asserted reference match
      | https://doi.org/10.1016/j.gca.2022.07.018        | references        | https://id.crossref.org/blank/1         | 2022-09-09T23:12:00Z | https://ror.org/02twcfp32            |
      | https://id.crossref.org/blank/1                  | object            | https://doi.org/10.1021/acs.est.1c04664 | 2022-09-09T23:12:00Z | https://ror.org/02twcfp32            |
      #Author with ORCID
      | https://doi.org/10.1344/waterfront2022.64.9.01   | author            | https://id.crossref.org/blank/2         | 2022-10-26T05:21:14Z | https://id.crossref.org/member/17854 |
      | https://id.crossref.org/blank/2                  | object            | https://orcid.org/0000-0003-3309-1968   | 2022-10-26T05:21:14Z | https://id.crossref.org/member/17854 |
      #Funding with a grant identifier
      | https://doi.org/10.12688/wellcomeopenres.18052.1 | funder            | https://id.crossref.org/blank/3         | 2022-10-05T00:00:00Z | https://id.crossref.org/member/2560  |
      | https://id.crossref.org/blank/3                  | object            | https://doi.org/10.37717/220020589      | 2022-10-26T05:21:14Z | https://id.crossref.org/member/2560  |

  Scenario: Report Crossref asserted reference match

    When relationship assertions for identifier "https://doi.org/10.1016/j.gca.2022.07.018" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                                | Relationship Type | Object ID                               | Asserted At          | Asserted By               |
      | https://doi.org/10.1016/j.gca.2022.07.018 | references        | https://doi.org/10.1021/acs.est.1c04664 | 2022-09-09T23:12:00Z | https://ror.org/02twcfp32 |

  Scenario: Report author with ORCID

    When relationship assertions for identifier "https://doi.org/10.1344/waterfront2022.64.9.01" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                                     | Relationship Type | Object ID                             | Asserted At          | Asserted By                          |
      | https://doi.org/10.1344/waterfront2022.64.9.01 | author            | https://orcid.org/0000-0003-3309-1968 | 2022-10-26T05:21:14Z | https://id.crossref.org/member/17854 |

  Scenario: Report funding with a grant identifier

    When relationship assertions for identifier "https://doi.org/10.12688/wellcomeopenres.18052.1" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                                       | Relationship Type | Object ID                          | Asserted At          | Asserted By                         |
      | https://doi.org/10.12688/wellcomeopenres.18052.1 | funder            | https://doi.org/10.37717/220020589 | 2022-10-05T00:00:00Z | https://id.crossref.org/member/2560 |

  Scenario: Query for the object of a reified relationship.

    When relationship assertions for identifier "https://doi.org/10.1021/acs.est.1c04664" are requested

    Then the following relationship assertions are returned from the item graph
      | Subject ID                                | Relationship Type | Object ID                               | Asserted At          | Asserted By               |
      | https://doi.org/10.1016/j.gca.2022.07.018 | references        | https://doi.org/10.1021/acs.est.1c04664 | 2022-09-09T23:12:00Z | https://ror.org/02twcfp32 |
