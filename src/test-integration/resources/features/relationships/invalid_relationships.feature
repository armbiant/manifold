Feature: Handle invalid relationships by ignoring them

  Scenario Outline: Ignore invalid relationships

    Given the following relationship assertions in the item graph
      | Subject ID                                    | Relationship Type | Object ID                                                                                     | Asserted At          | Asserted By                         |
      #Relationships to items with no identifier
      | 10.7358/ling-2020-002-pett                    | has-manifestation | https://id.crossref.org/blank4                                                                | 2022-03-30T23:25:10Z | https://id.crossref.org/member/5994 |
      | 10.1021/acsomega.2c04734                      | funding-source    | https://id.crossref.org/blank5                                                                | 2022-10-05T19:20:11Z | https://id.crossref.org/member/301  |
      #Relationships from blank nodes where the relationship type isn't object
      | https://doi.org/10.1098/rsbl.2012.0298        | references        | https://id.crossref.org/blank/6                                                               | 2022-09-30T23:57:55Z | https://ror.org/02twcfp32           |
      | https://id.crossref.org/blank/6               | source            | https://ca.wikipedia.org/api/rest_v1/page/html/Sopa_de_pl%C3%A0stic_del_Pac%C3%ADfic/30711606 | 2022-09-30T23:57:55Z | https://ror.org/02twcfp32           |
      | https://doi.org/10.1080/01630563.2022.2132510 | funding-source    | https://id.crossref.org/blank/7                                                               | 2022-10-26T05:15:31Z | https://id.crossref.org/member/301  |
      | https://id.crossref.org/blank/7               | funder            | https://doi.org/10.13039/100007224                                                            | 2022-10-26T05:15:31Z | https://id.crossref.org/member/301  |

    When relationship assertions for identifier <Identifier> are requested

    Then no relationship assertions are returned from the item graph

    Examples:
      | Identifier                                      |
      | "10.7358/ling-2020-002-pett"                    |
      | "10.1021/acsomega.2c04734"                      |
      | "https://doi.org/10.1098/rsbl.2012.0298"        |
      | "https://id.crossref.org/blank/6"               |
      | "https://doi.org/10.1080/01630563.2022.2132510" |
      | "https://id.crossref.org/blank/7"               |
