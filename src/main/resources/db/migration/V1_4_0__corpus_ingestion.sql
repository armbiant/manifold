-- Changes to enable ingestion the size and shape of the whole corpus.

-- Convert Item Identifier table to use a foreign key to identifier_type
-- instead of a duplicate string value.
-- This is meant to be run when the table is empty (written prior to production release).
ALTER TABLE item_identifier
  ALTER COLUMN prefix TYPE SMALLINT USING prefix::smallint;

ALTER TABLE item_identifier
  ALTER COLUMN prefix SET NOT NULL;

ALTER TABLE item_identifier
  ADD FOREIGN KEY (prefix) REFERENCES identifier_type(pk);

CREATE INDEX ON item_identifier (prefix);

-- Identifiers from arbitrary websites can get big. Remove the length constraint.
-- There is no performance difference with PostgreSQL.
ALTER TABLE item_identifier
  ALTER COLUMN suffix TYPE VARCHAR;

-- We get a lot of Item Identifiers, especially with Blank Nodes.
-- Migrate the primary key to BIGINT.
ALTER TABLE item_identifier
    ALTER COLUMN pk TYPE BIGINT;

CREATE SEQUENCE item_identifier_pk_seq_big as bigint OWNED BY item_identifier.pk;
SELECT SETVAL('item_identifier_pk_seq_big', COALESCE((SELECT MAX(pk)+1 FROM item_identifier), 1), FALSE);

ALTER TABLE item_identifier ALTER COLUMN pk SET DEFAULT nextval('item_identifier_pk_seq_big');

-- These are needed for retrieving assertions based on an envelope batch.
-- Given the size of the real corpus, indexes are needed.
CREATE INDEX property_envelope_idx ON property_assertion(envelope_pk);
CREATE INDEX ingestion_message_envelope_batch_idx ON ingestion_message(envelope_batch_pk);
CREATE INDEX ingestion_message_idx ON ingestion_message(envelope_pk);

CREATE INDEX assertion_envelope_idx ON assertion (envelope_pk);

-- These are used to lookup all assertions in a variety of common queries.
-- Order of fields is significant.
-- Also used for fix-up of assertions, i.e. resetting the 'state' field.
-- These are both dropped and re-added during bulk ingestion.
CREATE INDEX assertion_lookup ON assertion (from_item_pk, relationship_type_pk, to_item_pk, party_pk);
CREATE INDEX property_lookup ON property_assertion (item_pk, party_pk);
