-- A vocabulary of relationship types.
CREATE TABLE identifier_type (
    -- This is a smallint (max value 32,767) because the values in this table will have low cardinality:
    -- they are driven by a curated schema file. The Identifiers table has a PK to this, and the repeated extra 2 bytes
    -- adds up (half a gigabyte of difference)
    pk    SMALLSERIAL PRIMARY KEY NOT NULL,
    prefix VARCHAR(190) NOT NULL,
    blank BOOLEAN NOT NULL,
    CONSTRAINT identifier_type_prefix_key UNIQUE (prefix)
);
