-- Enable the assertion table to store which is the current value for each one.
-- This removes the need for the assertions_summary table.

ALTER TABLE assertion ADD current BOOLEAN NOT NULL DEFAULT FALSE;

CREATE UNIQUE INDEX ON assertion
(party_pk, from_item_pk, to_item_pk, relationship_type_pk, current)
WHERE current = true;

CREATE INDEX ON assertion (current)
WHERE current = true;

-- Update any other assertions made by this party for this subject, relationship, object.
CREATE OR REPLACE FUNCTION current_assertion_update ()
RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
  UPDATE assertion
     SET current = FALSE
   WHERE party_pk = NEW.party_pk
   AND from_item_pk = NEW.from_item_pk
   AND to_item_pk = NEW.to_item_pk
   AND relationship_type_pk = NEW.relationship_type_pk
   AND current = TRUE;
  RETURN NEW ;
END ;
$$ ;

CREATE TRIGGER current_assertion BEFORE INSERT ON assertion
FOR EACH ROW EXECUTE FUNCTION current_assertion_update () ;


-- Same for the properties.

ALTER TABLE property_assertion ADD current BOOLEAN NOT NULL DEFAULT FALSE;

CREATE UNIQUE INDEX ON property_assertion
(party_pk, item_pk, current)
WHERE current = true;

CREATE INDEX ON property_assertion (current)
WHERE current = true;

-- Update any prior assertions made by the party for this item.
CREATE OR REPLACE FUNCTION current_property_update ()
RETURNS trigger LANGUAGE plpgsql AS
$$
BEGIN
  UPDATE property_assertion
     SET current = FALSE
   WHERE party_pk = NEW.party_pk
   AND item_pk = NEW.item_pk
   AND current = TRUE;
  RETURN NEW ;
END ;
$$ ;

CREATE TRIGGER current_property BEFORE INSERT ON property_assertion
FOR EACH ROW EXECUTE FUNCTION current_property_update () ;

-- Don't need the tables any more.

DROP TABLE assertion_summary;
DROP TABLE property_assertion_summary;
