
create table if not exists property_assertion (
    pk                   serial primary key,
    asserted_at          timestamp with time zone not null,
    party_pk             bigint    not null,
    item_pk              bigint    not null,
    values               jsonb     not null,
    envelope_pk          bigint    not null,

    foreign key (party_pk) references item (pk),
    foreign key (item_pk) references item (pk),
    foreign key (envelope_pk) references envelope (pk)
);

create index if not exists property_assertion_party_pk_idx on property_assertion (party_pk);

create index if not exists property_assertion_item_pk_idx on property_assertion (item_pk);

create index if not exists property_assertion_envelope_pk_idx on property_assertion (envelope_pk);

create table if not exists property_assertion_summary (
    pk                   serial primary key,
    asserted_at          timestamp with time zone not null,
    party_pk             bigint    not null,
    item_pk              bigint    not null,
    values               jsonb     not null,
    envelope_pk          bigint    not null,

    foreign key (party_pk) references item (pk),
    foreign key (item_pk) references item (pk),
    foreign key (envelope_pk) references envelope (pk),

    -- only store the latest property assertion of each type asserted by each party
    constraint property_assertion_summary_party_properties
        unique (party_pk, item_pk)
);

create index if not exists property_assertion_summary_party_pk_idx on property_assertion_summary (party_pk);

create index if not exists property_assertion_summary_item_pk_idx on property_assertion_summary (item_pk);

create index if not exists property_assertion_summary_envelope_pk_idx on property_assertion_summary (envelope_pk);
