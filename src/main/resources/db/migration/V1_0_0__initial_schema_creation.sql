-- A batch of envelopes sent in response to user input.
create table if not exists envelope_batch (
    pk         bigserial primary key not null,
    ext_trace  varchar               null,
    user_agent varchar               null,
    created_at timestamp with time zone             not null
);

create index if not exists ext_trace_idx on envelope_batch (ext_trace);

create table if not exists envelope (
    pk                bigserial primary key not null,
    envelope_batch_pk bigint                not null,
    created_at        timestamp with time zone             not null,
    foreign key (envelope_batch_pk) references envelope_batch (pk)
);

create index if not exists envelope_pk_idx on envelope (envelope_batch_pk);

create table if not exists ingestion_message (
    pk                bigserial primary key not null,
    envelope_batch_pk bigint                not null,
    envelope_pk       bigint                null,
    stage             int                   not null,
    status            boolean               not null,
    message           text                  null,
    updated_at        timestamp with time zone             not null,
    foreign key (envelope_batch_pk) references envelope_batch (pk),
    foreign key (envelope_pk) references envelope (pk)
);

create index if not exists ingestion_message_batch_trace_idx on ingestion_message (envelope_batch_pk, envelope_pk);

-- An Item is any 'thing' that exists, including Research Objects.
create table if not exists item (
    pk         bigserial primary key,
    created_at timestamp with time zone not null
);

-- An Item Identifier is a string, which is associated with exactly one Item.
create table if not exists item_identifier (
    pk      serial primary key,
    prefix  varchar(16)  not null,
    suffix  varchar(256) not null,
    item_pk bigint       not null,
    foreign key (item_pk) references item (pk),
    constraint item_identifier_value_key unique (prefix, suffix)
);

create index if not exists item_identifier_item_pk_idx on item_identifier (item_pk);

-- A vocabulary of relationship types.
create table if not exists relationship_type (
    pk    serial primary key,
    value varchar(190) not null,
    constraint relationship_type_value_key unique (value)
);

/**
 * An itemgraph of a relationship by a Party They say that a given Relationship is either true or false.
 * These can be made repeatedly, and form a history of repeated assertions or retractions.
 * This data can be treated as disposable or archive-suitable, as the assertion_summary is the one that's used day to day.
 */
create table if not exists assertion (
    pk                   serial primary key,
    asserted_at          timestamp with time zone not null,
    state                boolean   not null,
    party_pk             bigint    not null,
    from_item_pk         bigint    not null,
    to_item_pk           bigint    not null,
    relationship_type_pk int       not null,
    envelope_pk          bigint    not null,
    foreign key (party_pk) references item (pk),
    foreign key (from_item_pk) references item (pk),
    foreign key (to_item_pk) references item (pk),
    foreign key (envelope_pk) references envelope (pk),
    foreign key (relationship_type_pk) references relationship_type (pk)
);

create index if not exists assertion_party_pk_idx on assertion (party_pk);

create index if not exists assertion_from_item_pk_idx on assertion (from_item_pk);

create index if not exists assertion_to_item_pk_idx on assertion (to_item_pk);

create index if not exists assertion_relationship_type_pk_idx on assertion (relationship_type_pk);

create table if not exists assertion_summary (
    pk                   serial primary key,
    asserted_at          timestamp with time zone not null,
    party_pk             bigint    not null,
    from_item_pk         bigint    not null,
    to_item_pk           bigint    not null,
    relationship_type_pk int       not null,
    state                boolean   not null,
    envelope_pk          bigint    not null,
    foreign key (party_pk) references item (pk),
    foreign key (from_item_pk) references item (pk),
    foreign key (to_item_pk) references item (pk),
    foreign key (relationship_type_pk) references relationship_type (pk),
    foreign key (envelope_pk) references envelope (pk),
    constraint assertion_summary_party_pk_from_item_pk_to_item_pk_relation_key
        unique (party_pk, from_item_pk, to_item_pk, relationship_type_pk, state)
);

create index if not exists assertion_summary_party_pk_idx on assertion_summary (party_pk);

create index if not exists assertion_summary_from_item_pk_idx on assertion_summary (from_item_pk);

create index if not exists assertion_summary_to_item_pk_idx on assertion_summary (to_item_pk);

create index if not exists assertion_summary_relationship_type_pk_idx on assertion_summary (relationship_type_pk);

create index if not exists assertion_summary_state_idx on assertion_summary (state);
