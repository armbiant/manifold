package org.crossref.messaging.aws.sqs

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.crossref.messaging.core.DeletableChannel
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.PollableChannel
import org.springframework.messaging.support.GenericMessage
import software.amazon.awssdk.core.exception.SdkException
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import software.amazon.awssdk.services.sqs.SqsClient
import software.amazon.awssdk.services.sqs.model.DeleteMessageRequest
import software.amazon.awssdk.services.sqs.model.ReceiveMessageRequest
import software.amazon.awssdk.services.sqs.model.SendMessageRequest
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * A [SqsClient] based [MessageChannel] implementation for sending and receiving messages to / from an AWS SQS queue.
 *
 * It does not support long polling, only short polling and only receives a single message at a time, even if more are
 * available on the queue.
 *
 * It also does not automatically delete received messages from the queue, meaning repeated calls could return the same
 * message, so consumers should be idempotent.
 *
 * @param sqsAsyncClient a configured client used to communicate with the AWS SQS service.
 * @param queueName the name of the SQS queue this channel represents.
 * @param queueUrl the URL of the queue as provided by the SQS service.
 */
class SqsQueue(
    private val sqsAsyncClient: SqsAsyncClient,
    private val queueName: String,
    private val queueUrl: String
) : PollableChannel, DeletableChannel {
    companion object {
        const val QUEUE_NAME = "queueName"
        const val RECEIPT_HANDLE = "receiptHandle"
    }

    override fun send(message: Message<*>, timeout: Long): Boolean {
        return try {
            sendMessage(message, timeout)
            true
        } catch (e: Exception) {
            when (e) {
                is TimeoutException, is SdkException -> false
                else -> throw e
            }
        }
    }

    override fun receive(): Message<*>? = receive(MessageChannel.INDEFINITE_TIMEOUT)

    override fun receive(timeout: Long): Message<*>? =
        runBlocking(Dispatchers.IO) {
            if (timeout != MessageChannel.INDEFINITE_TIMEOUT) {
                withTimeout(timeout) {
                    receiveMessage()
                }
            } else {
                receiveMessage()
            }
        }

    override fun delete(message: Message<*>, timeout: Long) {
        runBlocking(Dispatchers.IO) {
            if (timeout != MessageChannel.INDEFINITE_TIMEOUT) {
                withTimeout(timeout) {
                    deleteMessage(message)
                }
            } else {
                deleteMessage(message)
            }
        }

    }

    private fun sendMessage(message: Message<*>, timeout: Long) =
        sqsAsyncClient.sendMessage(
            SendMessageRequest.builder()
                .queueUrl(queueUrl)
                .messageBody(message.payload.toString())
                .build()
        ).get(timeout, TimeUnit.MILLISECONDS)

    private fun receiveMessage(): Message<*>? {
        val messages =
            sqsAsyncClient.receiveMessage(
                ReceiveMessageRequest.builder()
                    .queueUrl(queueUrl)
                    .maxNumberOfMessages(1)
                    .build()
            ).get().messages()
        if (messages.isEmpty()) {
            return null
        }
        val sqsMessage = messages.first()
        return GenericMessage(
            sqsMessage.body(),
            mapOf(QUEUE_NAME to queueName, RECEIPT_HANDLE to sqsMessage.receiptHandle())
        )
    }

    private fun deleteMessage(message: Message<*>) = sqsAsyncClient.deleteMessage(
        DeleteMessageRequest.builder()
            .queueUrl(queueUrl)
            .receiptHandle(message.headers[RECEIPT_HANDLE].toString())
            .build()
    ).get()
}
