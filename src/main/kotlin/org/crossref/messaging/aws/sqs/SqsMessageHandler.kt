package org.crossref.messaging.aws.sqs

import org.crossref.messaging.aws.s3.S3EventResolver
import org.springframework.core.annotation.AnnotationUtils
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.messaging.Message
import org.springframework.messaging.converter.MessageConverter
import org.springframework.messaging.handler.annotation.support.AnnotationExceptionHandlerMethodResolver
import org.springframework.messaging.handler.annotation.support.PayloadMethodArgumentResolver
import org.springframework.messaging.handler.invocation.AbstractExceptionHandlerMethodResolver
import org.springframework.messaging.handler.invocation.AbstractMethodMessageHandler
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver
import org.springframework.messaging.handler.invocation.HandlerMethodReturnValueHandler
import org.springframework.util.comparator.ComparableComparator
import java.lang.reflect.Method

/**
 * [org.springframework.messaging.MessageHandler] implementation for handling SQS messages. Provides
 * [org.springframework.messaging.handler.HandlerMethod] based message handling which:
 *
 * - discovers [SqsListener] annotated handler functions at startup
 * - matches a message to a handler function at runtime, based on the source queue name of the message
 * - invokes the handler function for the message
 *
 * @param messageConverter used for deserializing message payloads to the argument type of handler functions.
 */
class SqsMessageHandler(
    private val messageConverter: MessageConverter,
    private val environment: Environment) :
    AbstractMethodMessageHandler<SqsMessageHandler.MappingInformation>() {
    override fun initArgumentResolvers(): List<HandlerMethodArgumentResolver> =
        customArgumentResolvers.plus(
            listOf(
                S3EventResolver(messageConverter),
                PayloadMethodArgumentResolver(messageConverter)
            )
        )

    override fun initReturnValueHandlers(): List<HandlerMethodReturnValueHandler> =
        customReturnValueHandlers.toList()

    override fun isHandler(beanType: Class<*>): Boolean = true

    override fun getMappingForMethod(method: Method, handlerType: Class<*>): MappingInformation? {
        val sqsListenerAnnotation = AnnotationUtils.findAnnotation(
            method,
            SqsListener::class.java
        )
        return if (sqsListenerAnnotation != null) {
            val queueName = environment[sqsListenerAnnotation.queueNameEnvVar]
            if (queueName != null) {
                MappingInformation(
                    queueName,
                    sqsListenerAnnotation.deletePolicy
                )
            } else null
        } else null
    }

    override fun getDestination(message: Message<*>): String = message.headers[SqsQueue.QUEUE_NAME].toString()

    override fun getMappingComparator(message: Message<*>): Comparator<MappingInformation> =
        ComparableComparator()

    override fun createExceptionHandlerMethodResolverFor(beanType: Class<*>): AbstractExceptionHandlerMethodResolver =
        AnnotationExceptionHandlerMethodResolver(beanType)

    override fun getMatchingMapping(
        mapping: MappingInformation,
        message: Message<*>
    ): MappingInformation? =
        if (mapping.queueName == getDestination(message)) {
            mapping
        } else null

    override fun getDirectLookupDestinations(mapping: MappingInformation): Set<String> =
        setOf(mapping.queueName)

    data class MappingInformation(
        val queueName: String,
        val deletePolicy: SqsDeletePolicy
    ) : Comparable<MappingInformation> {
        override fun compareTo(other: MappingInformation): Int {
            val queueNameComparison = queueName.compareTo(other.queueName)
            return if (queueNameComparison != 0) {
                queueNameComparison
            } else {
                deletePolicy.compareTo(other.deletePolicy)
            }
        }
    }
}
