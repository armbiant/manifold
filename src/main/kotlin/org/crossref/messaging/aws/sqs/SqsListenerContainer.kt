package org.crossref.messaging.aws.sqs

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.ApplicationListener
import org.springframework.messaging.Message
import org.springframework.messaging.MessagingException
import org.springframework.scheduling.annotation.Async

/**
 * Container class for SQS message listeners that implements the lifecycle interfaces used by the Spring container to
 * create, initialise and start this container.
 *
 * Continually and asynchronously polls the SQS queues named in the [SqsListener] annotated functions registered
 * by Spring, handing off received messages to the SQS message handler and deleting processed messages as per the
 * configured [SqsDeletePolicy] for each queue.
 *
 * @param sqsTemplate used for performing messaging operations on the SQS queues.
 * @param sqsMessageHandler handles the messages received from SQS queues.
 */
class SqsListenerContainer(
    private val sqsTemplate: SqsTemplate,
    private val sqsMessageHandler: SqsMessageHandler
) : ApplicationListener<ApplicationStartedEvent>, InitializingBean, DisposableBean {
    private var runState: Boolean = false
    private lateinit var queueDeletePolicies: Map<String, SqsDeletePolicy>

    override fun afterPropertiesSet() {
        queueDeletePolicies =
            sqsMessageHandler.handlerMethods.keys.associate { it.queueName to it.deletePolicy }
    }

    @Async
    override fun onApplicationEvent(event: ApplicationStartedEvent) {
        runState = true
        runBlocking {
            queueDeletePolicies.keys.forEach {
                launch { pollQueue(it) }
            }
        }
    }

    override fun destroy() {
        runState = false
    }

    private fun pollQueue(queueName: String) {
        while (runState) {
            val message = sqsTemplate.receiveNoDelete(queueName)
            if (message != null) {
                val successful = try {
                    sqsMessageHandler.handleMessage(message)
                    true
                } catch (me: MessagingException) {
                    false
                }
                applyDeletePolicy(queueName, message, successful)
            }
        }
    }

    private fun applyDeletePolicy(queueName: String, message: Message<*>, successful: Boolean) {
        val deletePolicy = queueDeletePolicies[queueName]
        if (
            deletePolicy != SqsDeletePolicy.NEVER
            && (deletePolicy == SqsDeletePolicy.ALWAYS
                    || (deletePolicy == SqsDeletePolicy.SUCCESS && successful)
                    || (deletePolicy == SqsDeletePolicy.FAIL && !successful))
        ) {
            sqsTemplate.delete(queueName, message)
        }
    }
}
