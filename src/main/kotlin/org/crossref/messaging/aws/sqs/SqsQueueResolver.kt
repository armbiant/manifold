package org.crossref.messaging.aws.sqs

import org.springframework.messaging.core.DestinationResolver
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import software.amazon.awssdk.services.sqs.SqsClient
import software.amazon.awssdk.services.sqs.model.GetQueueUrlRequest

/**
 * A [SqsClient] based [DestinationResolver] implementation that resolves an SQS queue name to queue URL and creates a
 * [SqsQueue] representing the queue.
 *
 * @param sqsAsyncClient a configured client used to communicate with the AWS SQS service.
 */
class SqsQueueResolver(
    private val sqsAsyncClient: SqsAsyncClient
) : DestinationResolver<SqsQueue> {
    override fun resolveDestination(queueName: String): SqsQueue = SqsQueue(
        sqsAsyncClient,
        queueName,
        sqsAsyncClient.getQueueUrl(GetQueueUrlRequest.builder().queueName(queueName).build()).get().queueUrl()
    )
}
