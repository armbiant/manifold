package org.crossref.messaging.aws.sqs

import org.crossref.messaging.core.DestinationResolvingMessageDeletingOperations
import org.springframework.messaging.Message
import org.springframework.messaging.converter.MessageConverter
import org.springframework.messaging.core.AbstractDestinationResolvingMessagingTemplate
import software.amazon.awssdk.services.sqs.SqsAsyncClient

/**
 * Provides destination resolvable operations for sending, receiving and deleting messages via SQS queues, as defined by
 * the following interfaces:
 *
 * - [org.springframework.messaging.core.DestinationResolvingMessageSendingOperations]
 * - [org.springframework.messaging.core.DestinationResolvingMessageReceivingOperations]
 * - [org.springframework.messaging.core.DestinationResolvingMessageRequestReplyOperations]
 * - [org.crossref.messaging.core.DestinationResolvingMessageDeletingOperations]
 *
 * @param sqsAsyncClient a configured client used to communicate with the AWS SQS service.
 * @param sqsMessageConverter used for serializing / deserializing message payloads.
 */
class SqsTemplate(
    sqsAsyncClient: SqsAsyncClient,
    sqsMessageConverter: MessageConverter
) : DestinationResolvingMessageDeletingOperations<SqsQueue>,
    AbstractDestinationResolvingMessagingTemplate<SqsQueue>() {
    init {
        destinationResolver = SqsQueueResolver(sqsAsyncClient)
        messageConverter = sqsMessageConverter
    }

    override fun doSend(destination: SqsQueue, message: Message<*>) {
        destination.send(message)
    }

    override fun doReceive(destination: SqsQueue): Message<*>? {
        val message = receiveNoDelete(destination)
        if (message != null) {
            delete(destination, message)
        }
        return message
    }

    override fun doSendAndReceive(destination: SqsQueue, requestMessage: Message<*>): Message<*>? {
        doSend(destination, requestMessage)
        return doReceive(destination)
    }

    override fun delete(destinationName: String, message: Message<*>) =
        delete(resolveDestination(destinationName), message)

    override fun delete(destination: SqsQueue, message: Message<*>) = destination.delete(message)

    override fun receiveNoDelete(destinationName: String): Message<*>? = receive(resolveDestination(destinationName))

    override fun receiveNoDelete(destination: SqsQueue): Message<*>? = destination.receive()
}
