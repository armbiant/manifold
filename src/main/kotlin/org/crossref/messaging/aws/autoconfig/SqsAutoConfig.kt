package org.crossref.messaging.aws.autoconfig

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.crossref.messaging.aws.autoconfig.AwsAutoConfig.AWS_ENDPOINT_OVERRIDE
import org.crossref.messaging.aws.sqs.SqsListenerContainer
import org.crossref.messaging.aws.sqs.SqsMessageHandler
import org.crossref.messaging.aws.sqs.SqsTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.messaging.converter.MappingJackson2MessageConverter
import org.springframework.messaging.converter.MessageConverter
import software.amazon.awssdk.awscore.defaultsmode.DefaultsMode
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import java.net.URI

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(name = [SqsAutoConfig.SQS_ENABLED], havingValue = "true")
class SqsAutoConfig {
    companion object {
        const val SQS_ENABLED = "sqs-enabled"
    }

    @Bean
    fun messageConverter(): MessageConverter =
        MappingJackson2MessageConverter().apply {
            objectMapper.registerModule(JavaTimeModule())
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .registerModule(KotlinModule())
        }

    @Bean
    fun objectMapper(messageConverter: MessageConverter): ObjectMapper {
        return if (messageConverter is MappingJackson2MessageConverter) {
            messageConverter.objectMapper
        } else {
            ObjectMapper()
        }
    }

    @Bean
    fun sqsMessageHandler(messageConverter: MessageConverter, environment: Environment): SqsMessageHandler =
        SqsMessageHandler(messageConverter, environment)

    @Bean
    fun sqsAsyncClient(
        @Value("\${$AWS_ENDPOINT_OVERRIDE:}") awsEndpointOverride: String?
    ): SqsAsyncClient =
        SqsAsyncClient.builder()
            .defaultsMode(DefaultsMode.AUTO)
            .apply {
                if (!awsEndpointOverride.isNullOrBlank()) {
                    endpointOverride(URI(awsEndpointOverride))
                }
            }
            .build()

    @Bean
    fun sqsTemplate(
        sqsAsyncClient: SqsAsyncClient,
        messageConverter: MessageConverter
    ): SqsTemplate = SqsTemplate(sqsAsyncClient, messageConverter)

    @Bean
    fun sqsListenerContainer(
        sqsTemplate: SqsTemplate,
        sqsMessageHandler: SqsMessageHandler
    ): SqsListenerContainer = SqsListenerContainer(sqsTemplate, sqsMessageHandler)
}
