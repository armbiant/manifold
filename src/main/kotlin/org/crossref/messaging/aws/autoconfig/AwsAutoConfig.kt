package org.crossref.messaging.aws.autoconfig

object AwsAutoConfig {
    const val AWS_ENDPOINT_OVERRIDE = "aws-endpoint-override"
}
