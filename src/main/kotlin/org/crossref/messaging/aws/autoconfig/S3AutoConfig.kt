package org.crossref.messaging.aws.autoconfig

import org.crossref.messaging.aws.autoconfig.AwsAutoConfig.AWS_ENDPOINT_OVERRIDE
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.awscore.defaultsmode.DefaultsMode
import software.amazon.awssdk.services.s3.S3Client
import java.net.URI

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(name = [S3AutoConfig.S3_ENABLED], havingValue = "true")
class S3AutoConfig {
    companion object {
        const val S3_ENABLED = "s3-enabled"
        const val S3_NOTIFICATION_QUEUE = "s3-notification-queue"
    }

    @Bean
    fun s3Client(
        @Value("\${$AWS_ENDPOINT_OVERRIDE:}") awsEndpointOverride: String?
    ): S3Client = S3Client.builder()
        .defaultsMode(DefaultsMode.AUTO)
        .apply {
            if (!awsEndpointOverride.isNullOrBlank()) {
                endpointOverride(URI(awsEndpointOverride))
            }
        }
        .build()
}
