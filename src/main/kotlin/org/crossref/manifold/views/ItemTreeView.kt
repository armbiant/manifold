package org.crossref.manifold.retrieval.view

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.views.ItemizedIdentifiers


/** View of a Relationship.
 */
data class RelationshipView(
    /**
     * Multiple parties making this assertion.
     */
    @JsonProperty("asserted-by")
    val assertedBy: List<ItemizedIdentifiers>,

    /**
     * Type of this Relationship.
     */
    @JsonProperty("relationship-type")
    val relTyp: String,

    /**
     * The Item object that is the object of this Relationship.
     * This is the point of recursion in the tree.
     */
    @JsonProperty("object")
    val itemView: ItemView
)

/** View of a Property.
 */
data class PropertyView(
    @JsonProperty("asserted-by")
    val assertedBy: List<ItemizedIdentifiers>,

    @JsonProperty("property-type")
    val propertyType: String,

    @JsonProperty("value")
    val value: JsonNode,
)

/** View of an Item Tree for serialization to JSON.
 */
data class ItemView(
    /**
     * Identifiers by which this Item is known. There can be zero, one or more Item Identifiers. Ordered.
     * In some cases the order may be useful, e.g. sorting Identifiers by preference.
     */
    @JsonProperty("identifiers")
    val itemIdentifiers: ItemizedIdentifiers,

    @JsonProperty("properties")
    val properties: List<PropertyView> = emptyList(),

    /**
     * Relationships to other Items. This is ordered, giving users the ability to consider order meaningful or not.
     * In some cases the order may be useful.
     */
    @JsonProperty("relationships")
    val relationships: List<RelationshipView> = emptyList(),
) {
}