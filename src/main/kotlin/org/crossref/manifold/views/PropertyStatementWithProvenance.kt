package org.crossref.manifold.views

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.node.ObjectNode
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.PropertyStatement
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.retrieval.IdentifierRetriever

data class PropertyStatementWithProvenance(
    @JsonProperty("property-statement-id")
    val pk: Long?,

    @JsonProperty("subject")
    val subjectIds: Map<String, List<String?>>,

    @JsonProperty("values")
    val values: ObjectNode,

    @JsonProperty("asserted-by")
    val assertedBy: Map<String, List<String?>>,

    @JsonProperty("asserted-at")
    val assertedAt: String,

    @JsonProperty("asserting-agent")
    val assertingAgent: String?,

    @JsonProperty("asserted-in-batch")
    val assertedInBatch: Long?
) {

    companion object {
        fun from(
            statement: PropertyStatement,
            provenance: EnvelopeBatchProvenance,
            identifierRetriever: IdentifierRetriever,
            identifierTypeRegistry: IdentifierTypeRegistry
        ) =

            PropertyStatementWithProvenance(
                pk = statement.pk,
                subjectIds = buildItemizedIdentifiers(statement.itemPk, identifierRetriever, identifierTypeRegistry),
                values = statement.values,
                assertedBy = buildItemizedIdentifiers(
                    statement.assertingPartyPk,
                    identifierRetriever,
                    identifierTypeRegistry
                ),
                assertedAt = statement.assertedAt.toString(),
                assertingAgent = provenance.userAgent,
                assertedInBatch = provenance.envelopeBatchPk
            )
    }
}
