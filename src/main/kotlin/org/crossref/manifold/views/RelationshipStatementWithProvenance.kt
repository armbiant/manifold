package org.crossref.manifold.views

import com.fasterxml.jackson.annotation.JsonProperty
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.RelationshipStatement
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.retrieval.IdentifierRetriever

data class RelationshipStatementWithProvenance(
    @JsonProperty("property-statement-id")
    val pk: Long?,

    @JsonProperty("subject")
    val subjectIds: Map<String, List<String?>>,

    @JsonProperty("relationship-type")
    val relationshipType: String?,

    @JsonProperty("object")
    val objectIds: Map<String, List<String?>>,

    @JsonProperty("status")
    val status: Boolean,

    @JsonProperty("asserted-by")
    val assertedBy: Map<String, List<String?>>,

    @JsonProperty("asserted-at")
    val assertedAt: String,

    @JsonProperty("asserting-agent")
    val assertingAgent: String?,

    @JsonProperty("asserted-in-batch")
    val assertedInBatch: Long?
) {

    companion object {
        fun from(
            statement: RelationshipStatement,
            provenance: EnvelopeBatchProvenance,
            identifierRetriever: IdentifierRetriever,
            identifierTypeRegistry: IdentifierTypeRegistry
        ) =

            RelationshipStatementWithProvenance(
                pk = statement.pk,
                subjectIds = buildItemizedIdentifiers(
                    statement.fromItemPk,
                    identifierRetriever,
                    identifierTypeRegistry
                ),
                relationshipType = statement.relationshipType,
                objectIds = buildItemizedIdentifiers(statement.toItemPk, identifierRetriever, identifierTypeRegistry),
                status = statement.status,
                assertedBy = buildItemizedIdentifiers(
                    statement.assertingPartyPk,
                    identifierRetriever,
                    identifierTypeRegistry
                ),
                assertedAt = statement.assertedAt.toString(),
                assertingAgent = provenance.userAgent,
                assertedInBatch = provenance.envelopeBatchPk
            )

    }
}
