package org.crossref.manifold.ingestion



/**
 * A batch of [Envelope]s which corresponds to one chunk of information arriving from outside, such as a metadata
 * deposit, social media event, reference match etc. These are batched together for traceability, because one piece of
 * information in the outside world may map to more than one [Envelope], i.e. represent assertions with multiple
 * parties. By keeping these together we can track them all the work that we did in response.
 */
data class EnvelopeBatch(
    /**
     * List of [Envelope]s, each of which asserts an Item Tree.
     */
    val envelopes: List<Envelope>,

    /** Which service inserted this Envelope.
     */
    val provenance: EnvelopeBatchProvenance,

    /** Primary Key in the database
     */
    val pk: Long? = null
) {
    /**
     * Add and replace Envelopes.
     */
    fun withEnvelopes(newEnvelopes: List<Envelope>) = EnvelopeBatch(newEnvelopes, provenance)

    /** Add and replace the PK.
     */
    fun withPk(newPk: Long) = EnvelopeBatch(envelopes, provenance, newPk)
}