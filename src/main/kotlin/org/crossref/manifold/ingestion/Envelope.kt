package org.crossref.manifold.ingestion

import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.*

/**
 * One or more [Item] trees packaged up with the [ItemTreeAssertion] that describes what to do with them.
 */
data class Envelope(
    val itemTrees: List<Item>,
    val assertion: ItemTreeAssertion,
    val pk: Long? = null
) {
    fun withAssertion(newAssertion: ItemTreeAssertion): Envelope = Envelope(itemTrees, newAssertion, pk)
    fun withItemTrees(newItemTrees: List<Item>) =
        Envelope(newItemTrees, assertion, pk)

    fun withPk(newPk: Long) = Envelope(itemTrees, assertion, newPk)
}

/**
 *  Is there any use of ambiguous identifiers in this EnvelopeBatch?
 */
fun hasAmbiguousIdentifiers(batch: EnvelopeBatch) = batch.envelopes.any { env ->
    env.itemTrees.any { item ->
        hasAmbiguousIdentifiers(item)
    }
}

/** From an EnvelopeBatch retrieve all Identifiers that are used in an unambiguous way.
 * From the Content and the Envelope itself.
 */
fun getUnambiguousUnresolvedIdentifiers(batch: EnvelopeBatch): Set<Identifier> =
    batch.envelopes.flatMap { envelope ->
        envelope.itemTrees.flatMap { getUnambiguousUnresolvedIdentifiers(it) } +
                getUnambiguousUnresolvedIdentifiers(
                    envelope.assertion.assertingParty
                )
    }.toSet()

/**
 * Remove ambiguously identified Items form the [EnvelopeBatch] and return.
 */
fun removeAmbiguousItems(batch: EnvelopeBatch): EnvelopeBatch =
    batch.withEnvelopes(batch.envelopes.map(::removeAmbiguousItems))

/**
 * Remove ambiguously identified Items form the [EnvelopeBatch] and return.
 */
fun removeAmbiguousItems(envelope: Envelope): Envelope =
    envelope.withItemTrees(
        envelope.itemTrees.mapNotNull(::removeAmbiguousItems)
    )