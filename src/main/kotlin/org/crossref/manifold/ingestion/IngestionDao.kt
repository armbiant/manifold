package org.crossref.manifold.ingestion

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.sql.Types
import java.time.OffsetDateTime


/**
 * Data access for book-keeping around ingestion processes.
 */
@Repository
class IngestionDao(
    val jdbcTemplate: JdbcTemplate,

    @Autowired
    val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Create a new EnvelopeBatch and return its PK.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun createEnvelopeBatch(batch: EnvelopeBatch): Long {
        val keyHolder = GeneratedKeyHolder()

        val paramSource = MapSqlParameterSource(
            mapOf(
                "ext_trace" to batch.provenance.externalTrace,
                "user_agent" to batch.provenance.userAgent
            )
        )
            .addValue(
                "created_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            )

        npTemplate.update(
            "INSERT INTO envelope_batch (created_at, ext_trace, user_agent) VALUES (:created_at, :ext_trace, :user_agent)",
            paramSource,
            keyHolder,
            arrayOf("pk")
        )

        // If this returns a null there was a problem creating the row, which is a showstopper.
        return keyHolder.key!!.toLong()
    }


    /**
     * Create a new EnvelopeBatch and return its PK.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun createEnvelope(batchPk: Long): Long {
        val keyHolder = GeneratedKeyHolder()

        val paramSource = MapSqlParameterSource().apply {
            addValue(
                "created_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            ).addValue("envelope_batch_pk", batchPk)
        }

        npTemplate.update(
            "INSERT INTO envelope (created_at, envelope_batch_pk) VALUES (:created_at, :envelope_batch_pk)",
            paramSource,
            keyHolder,
            arrayOf("pk")
        )

        // If this returns a null there was a problem creating the row, which is a showstopper.
        return keyHolder.key!!.toLong()
    }

    /**
     * Log an operation in relation to an EnvelopeBatch and optionally the Envelope within it.
     * There's always a batch, but for operations relating to the envelope itself, the Envelope PK can be null.
     */
    fun update(batchPk: Long, envelopePk: Long?, stage: Stage, status: Boolean, message: String? = null) {
        val paramSource = MapSqlParameterSource().apply {
            addValue(
                "updated_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            ).addValues(
                mapOf(
                    "envelope_batch_pk" to batchPk,
                    "envelope_pk" to envelopePk,
                    "stage" to stage.value,
                    "status" to status,
                    "message" to message
                )
            )
        }
        npTemplate.update(
            "INSERT INTO ingestion_message (" +
                    "updated_at, envelope_batch_pk, envelope_pk, stage, status, message" +
                    ") VALUES (" +
                    ":updated_at, :envelope_batch_pk, :envelope_pk, :stage, :status, :message" +
                    ")",
            paramSource
        )
    }
}


