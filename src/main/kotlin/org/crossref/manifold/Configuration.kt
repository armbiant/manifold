package org.crossref.manifold

import org.crossref.manifold.modules.itemTree.ItemTreeInterceptor
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.servlet.server.ServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.retry.annotation.EnableRetry
import org.springframework.web.servlet.config.annotation.*
import org.springframework.web.util.pattern.PathPatternParser

/**
 * Spring Boot configuration.
 */
@Configuration
@EnableRetry
@EnableWebMvc
class AppConfiguration : WebMvcConfigurer {

    /**
     * Servlet web factory, required to smooth interop with transitive dependencies of Cayenne.
     */
    @Bean
    fun servletWebServerFactory(): ServletWebServerFactory? {
        return TomcatServletWebServerFactory()
    }

    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        configurer.favorParameter(true)
            .parameterName("format")
            .ignoreAcceptHeader(false)
            .defaultContentType(MediaType.APPLICATION_JSON)
            .mediaType("application/json", MediaType.APPLICATION_JSON)
            .mediaType("application/vnd.scholix+json", MediaType("application", "vnd.scholix+json"))
            .mediaType("application/rdf+xml", MediaType("application", "rdf+xml"))
            .mediaType("text/turtle", MediaType("text", "turtle"))
            .mediaType("application/ld+json", MediaType("application", "ld+json"))

            .mediaType("json", MediaType.APPLICATION_JSON)
            .mediaType("scholix", MediaType("application", "vnd.scholix+json"))
            .mediaType("rdf-xml", MediaType("application", "rdf+xml"))
            .mediaType("rdf-turtle", MediaType("text", "turtle"))
            .mediaType("json-ld", MediaType("application", "ld+json"))
    }

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.setPatternParser(PathPatternParser())
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(ItemTreeInterceptor())
    }
}
