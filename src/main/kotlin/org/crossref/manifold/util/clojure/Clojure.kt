package org.crossref.manifold.util.clojure

import clojure.lang.IPersistentMap
import clojure.lang.Keyword
import clojure.lang.PersistentHashMap
import clojure.lang.RT
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.*
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.unixml.support.logger


/**
 * Build a collection of IDs from a Clojure Item Tree's list of IDs.
 */
fun Identifier.Companion.fromCljColl(inp: Any?): List<Identifier> = if (inp is Iterable<*>) {
    inp.filterIsInstance(String::class.java).map { x -> Identifier(x) }
} else emptyList()

/**
 * Build a RelationType object from a Clojure Item Tree's Relation Type keyword.
 */

/** Clojure keyword to String with safe default. For relationship types.
 */
fun cljValToString(inp: Any?): String = if (inp is Keyword) {
    inp.name
} else "UNKNOWN"

/**
 * Build a collection of Relationships, and subtrees, from a Clojure Item Tree's list of IDs.
 * This is the point at which the tree branches out.
 */

fun Relationship.Companion.fromCljColl(inp: Any?): List<Relationship> = if (inp is Map<*, *>) {
    inp.flatMap { (typ, values) ->
        // Type erasure prevents checking the type of the iterable for values,
        // but it should be Iterable<Map<*,*>>.
        if (typ is Keyword && values is Iterable<*>) {
            val relationshipType = cljValToString(typ)
            values.mapNotNull { x ->
                if (x is Map<*, *>) {
                    Relationship(relationshipType, Item.fromClj(x))
                } else null
            }
        } else {
            logger.warn("ERROR! Didn't recognise k: $typ v: $values")
            emptyList()
        }
    }
} else emptyList()


val kwId = Keyword.intern("id")
val kwRel = Keyword.intern("rel")
val reservedKws = setOf<Keyword>(kwId, kwRel)

/**
 * Convert a property value, as a Clojure object, into a JSON node.
 */
fun makeProperty(input: Any?): JsonNode {
    val mapper = ObjectMapper()
    return when (input) {
        null -> {
            mapper.nullNode()
        }
        is Keyword -> {
            TextNode(input.name)
        }
        is Iterable<*> -> {
            mapper.valueToTree(input.map { makeProperty(it) })
        }
        is Int -> {
            IntNode(input)
        }
        is Long -> {
            LongNode(input)
        }
        is Float -> {
            FloatNode(input)
        }
        is String -> {
            TextNode(input)
        }
        is Map<*, *> -> {

            val node = mapper.createObjectNode()
            for ((k, v) in input) {
                val ks: String = if (k is Keyword) {
                    k.name
                } else {
                    k.toString()
                }
                node.replace(ks, makeProperty(v))
            }
            node
        }
        is Boolean -> {
            if (input) {
                BooleanNode.TRUE
            } else {
                BooleanNode.FALSE
            }
        }
        else -> {
            logger.error("Didn't recognise property type: $input")
            mapper.nullNode()

        }
    }
}

fun makeProperties(inp: Map<*, *>): Properties {
    val properties = makeProperty(inp.filterKeys { x -> !reservedKws.contains(x) }).deepCopy<ObjectNode>()
    return Properties(properties)
}

/**
 * Construct a Manifold Kotlin Item Tree from a Clojure Item Tree.
 */
fun Item.Companion.fromClj(inp: Map<*, *>): Item {

    // The following keys are reserved and each need special treatment.
    val ids = Identifier.fromCljColl(inp[kwId])
    val relationships = Relationship.fromCljColl(inp[kwRel])

    val properties = makeProperties(inp)

    return Item(ids, relationships, listOf(properties))
}

/**
 * Convert a JSON node into a Clojure Object, recursively.
 */
fun JsonNode.toClj(): Any? =
    if (this.isTextual) {
        this.asText()
    } else if (this.isBigDecimal) {
        this.asLong()
    } else if (this.isObject) {
        PersistentHashMap.create(RT.seq(this.fields().asSequence().toList().flatMap {
            listOf(Keyword.intern(it.key), it.value.toClj())
        }))
    } else if (this.isBigInteger) {
        this.asLong()
    } else if (this.isBoolean) {
        this.asBoolean()
    } else if (this.isDouble) {
        this.asDouble()
    } else if (this.isFloat) {
        this.asDouble()
    } else if (this.isInt) {
        this.asInt()
    } else if (this.isLong) {
        this.asLong()
    } else if (this.isNull) {
        null
    } else if (this.isShort) {
        this.asInt()
    } else if (this.isArray) {
        this.toList().map { it.toClj() }
    } else if (this.isBigInteger) {
        this.asLong()
    } else {
        null
    }

/**
 * Transform an Item tree to the Clojure representation that Cayenne produces.
 * Some values are part of controlled vocabulary (such as e.g. {"type": "work"}) and naturally
 * render as strings. But need to be converted to keywords. Take a set of keywords (e.g. "type") for which
 * the value (e.g. "work") should be converted to a keyword.
 */
fun Item.toClj(fieldsToConvertToKeywords: Set<String>): IPersistentMap? {
    val identifiers = this.identifiers.map { it.uri }

    // Manifold has relationships as lists of objects.
    // Cayenne has a map of rel type to object. So group by rel type.
    val relationships = this
        .rels
        // The keys should be Clojure Keywords. The values are Items, which should be transformed into Clojure objects
        // recursively.
        .groupBy(
            keySelector = { Keyword.intern(it.relTyp) },
            valueTransform = { it.obj.toClj(fieldsToConvertToKeywords) })
        // Turn into a list of [k1 v1 k2 v2 ...] as the PersistentHashMap constructor expects.
        .flatMap { (k, v) -> listOf(k, RT.seq(v)) }
    val relationshipsAsHashmap = PersistentHashMap.create(RT.seq(relationships))

    // Properties are lists of JSON assertions as made by various parties.
    // So we need to merge these.

    // Take the list of properties objects.
    val p = this.properties
        // Flatten into a big list of pairs.
        .flatMap { it.values.fields().asSequence().toList() }
        // Group values by key so we have one of each key.
        // The keys should be Clojure Keywords. The values are JSON objects which should be transformed into Clojure
        // objects recursively.
        .groupBy(keySelector = { it.key }, valueTransform = { it.value.toClj() })
        // Simply take the first value we find and discard the others.
        .map { (k, vs) -> k to vs.first() }
        // Values for some keywords must be keywords because Cayenne expects it. Use the supplied set of keys to decide.
        // Expect that these are always Strings, or in the worst case, can be toStringed.
        .map { (k, v) ->
            k to
                    if (k in fieldsToConvertToKeywords) {
                        Keyword.intern(v.toString())
                    } else {
                        v
                    }
        }
        // Finally, turn into a list of [k1 v1 k2 v2 ... ] as the PersistentHashMap constructor expects.
        .flatMap { it -> listOf(Keyword.intern(it.first), it.second) }

    val asSeq = RT.seq(p)
    return PersistentHashMap.create(asSeq).assoc(kwId, identifiers).assoc(kwRel, relationshipsAsHashmap)
}
