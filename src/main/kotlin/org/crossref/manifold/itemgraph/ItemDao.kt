package org.crossref.manifold.itemgraph

import org.crossref.manifold.itemtree.Tokenized
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.sql.ResultSet
import java.sql.Types
import java.time.OffsetDateTime

/** DAO for Items and their Identifiers. Because Items are retrieved by their Identifiers these are grouped into the
 * same DAO.
 */
@Repository
class ItemDao(
    val jdbcTemplate: JdbcTemplate,
    val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate),
    val identifierTypeRegistry: IdentifierTypeRegistry
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Create a new Item and return its PK.
     * No other information stored (yet).
     * @return The new Item PK.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun insertNewItem(): Long {
        val keyHolder = GeneratedKeyHolder()

        val paramSource = MapSqlParameterSource().apply {
            addValue(
                "created_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            )
        }

        npTemplate.update(
            "INSERT INTO item (created_at) VALUES (:created_at)",
            paramSource,
            keyHolder,
            arrayOf("pk")
        )

        // If this returns a null there was a problem creating the row, which is a showstopper.
        return keyHolder.key!!.toLong()
    }

    /**
     * If the ItemIdentifier exists, add its PK and return, along with its Item's PK.
     * If not, return the ItemIdentifier unchanged.
     * @return The Identifier and Item PKs, if found.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun findItemAndIdentifierPk(tokenized: Tokenized): IdentifierPkItemPk? =
        npTemplate.query(
            "select pk, item_pk from item_identifier where prefix = :prefix and suffix = :suffix",

            MapSqlParameterSource().apply {
                addValue(
                    "prefix",
                    identifierTypeRegistry.resolvePrefix(tokenized.prefix),
                    Types.SMALLINT
                )
                addValue(
                    "suffix",
                    tokenized.suffix,
                    Types.VARCHAR
                )
            }

        ) { rs: ResultSet, _: Int ->
            IdentifierPkItemPk(identifierPk = rs.getLong("pk"), itemPk = rs.getLong("item_pk"))
        }.firstOrNull()

    /**
     * Create a new ItemIdentifier connected with the Item PK, and return the updated ItemIdentifier.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun createItemIdentifier(itemPk: Long, tokenized: Tokenized): Long {

        val keyHolder = GeneratedKeyHolder()

        val paramSource = MapSqlParameterSource(
            mapOf(
                "prefix" to identifierTypeRegistry.resolvePrefix(tokenized.prefix),
                "suffix" to tokenized.suffix,
                "item_pk" to itemPk
            )
        )

        npTemplate.update(
            "INSERT INTO item_identifier (prefix, suffix, item_pk) VALUES (:prefix, :suffix, :item_pk)",
            paramSource,
            keyHolder,
            arrayOf("pk")
        )

        // If this returns a null there was a problem creating the row, which is a showstopper.
        return keyHolder.key!!.toLong()
    }


    /**
     * For a collection of Identifier values, return a map that can be used to resolve them.
     * Read only, so will retrieve only those Identifiers already known. Those not yet known are not returned.
     * @return map of pairs of tokenized Identifier PK and corresponding Item PK.
     */
    fun getIdentifierMappingsRO(
        identifiers: Collection<Tokenized>
    ): Map<Tokenized, IdentifierPkItemPk> = if (identifiers.isEmpty()) {
        // Special case, the SQL templating doesn't like an empty list.
        emptyMap()
    } else {
        // Start with a with the all keys present but unresolved ItemIdentifiers.
        val blank: MutableMap<Tokenized, IdentifierPkItemPk> = mutableMapOf()


        // The number of identifiers can vary, which means that the SQL 'IN' clause would be templated to a sequence
        // of random lengths. Very bad for query cache. Also, given the input length is arbitrary, a potential vector
        // for a pathologically large SQL query.
        // So batching them up into batches of size N means that there is only a maximum of N permutations of the query.
        // And for large batch sizes, we still get the benefit of querying a large number of ItemIdentifiers in a
        // reduced number of SQL queries.
        val batches = identifiers.chunked(10)
        for (batch in batches) {
            // Look up those we could find.
            // Note that :values is a list of tuples.
            val found =
                npTemplate.query(
                    "select pk, prefix, suffix, item_pk from item_identifier where (prefix, suffix) in (:values)",
                    mapOf("values" to batch.map {
                        arrayOf(identifierTypeRegistry.resolvePrefix(it.prefix), it.suffix)
                    })
                ) { rs, _ ->
                    val prefixVal = identifierTypeRegistry.reversePrefix(rs.getInt("prefix"))
                    val tokenized = Tokenized(prefixVal, rs.getString("suffix"))
                    val itemIdentifierPk = rs.getLong("pk")
                    val itemPk = rs.getLong("item_pk")

                    tokenized to IdentifierPkItemPk(itemIdentifierPk, itemPk)

                }.toMap()

            // And replace them.
            blank.putAll(found)
        }

        blank
    }

    /**
     * For a set of ItemPks, retrieve a mapping of each Item PK to its Item PK and tokenized pair.
     */
    fun fetchItemIdentifierPairsForItemPks(itemPks: Set<Long>): Collection<Pair<Long, Tokenized>> =
        // Need to guard, Postgres doesn't like an empty list.
        if (itemPks.isEmpty()) {
            emptyList()
        } else {
            npTemplate.query(
                "select pk, prefix, suffix, item_pk from item_identifier where item_pk in (:item_pks)",
                mapOf("item_pks" to itemPks)
            ) { rs, _ ->
                Pair(
                    rs.getLong("item_pk"),
                    Tokenized(identifierTypeRegistry.reversePrefix(rs.getInt("prefix")), rs.getString("suffix"))
                )
            }
        }

    private fun getItemCount(): Long =
        jdbcTemplate.queryForObject("SELECT COUNT(pk) FROM item", Long::class.java) as Long

    private fun getIdentifierCount(): Long =
        jdbcTemplate.queryForObject("SELECT COUNT(pk) FROM item_identifier", Long::class.java) as Long

    /**
     * Insert a batch of Item Identifiers.
     * This is used both for both of day-to-day insertion of Identifiers, but also in batch ingestion mode.
     */
    fun insertIdentifiersBulk(
        identifiers: Collection<Tokenized>
    ) {

        // SQL params don't like empty lists.
        if (identifiers.isEmpty()) {
            return
        }

        val totalCount = identifiers.count()

        logger.info("Buffer $totalCount identifiers...")

        // Each batch makes one SQL statement.
        // At average 40 bytes per identifier suffix, a chunk of 1,000 adds 400 KB to the SQL statement.
        // In a test of 1 million identifiers, a chunk size of 10,000 results in ingestion at 16,000 identifiers per
        // second. Size 1000 gives the same figure. Batch size 100 gives 15,000 per second.

        val itemParamSource = MapSqlParameterSource().apply {
            addValue(
                "created_at",
                OffsetDateTime.now(),
                Types.TIMESTAMP_WITH_TIMEZONE
            )
            addValue("num", totalCount, Types.INTEGER)
        }


        val newItemPks =
            npTemplate.query(
                "INSERT INTO item(created_at) \n" +
                        "SELECT created_at FROM GENERATE_SERIES(0, :num), (SELECT :created_at ::date AS created_at) b\n" +
                        "RETURNING pk\n",
                itemParamSource
            ) { rs, _ ->
                rs.getLong("pk")
            }

        // Each new Identifier combined with its Item.
        val combined = identifiers.zip(newItemPks)

        // Each chunk maps to a SQL insert statement. We want them to be large for performance, but not too large.
        // For UniXML data, at 10,000 the average size is around 400,000
        // At 1,000 the size is 40,000 which is more reasonable.
        val combinedChunks = combined.chunked(1000)

        // Avoid divide by zero.
        val numChunks = combinedChunks.count()
        if (numChunks > 0) {
            val averageSize = combinedChunks.sumOf { batch ->
                batch.sumOf {
                    // Account 4 bytes for Long to store the prefix.
                    // This figure is only used for logging, but for large batches, this could be a
                    // significant chunk of memory.
                    it.first.suffix.length + 4
                }
            } / numChunks
            logger.info("Insert $totalCount identifiers in $numChunks chunks. Average size of chunk: $averageSize characters")
        }

        // Splits large-ish batches, creating a corresponding Item for each, then zips the new Item PKs with Identifiers
        // in code, then inserts ignoring duplicates.
        // This can achieve 10,000 to 20,000 insertions per second. More exotic appraoches, such as a single Common
        // Table Expression or a temporary unlogged table, don't improve on that.
        for (chunk in combinedChunks) {

            val identifierParamSource = MapSqlParameterSource().apply {
                addValue("values", chunk.map { (identifier, itemPk) ->
                    arrayOf(identifierTypeRegistry.resolvePrefix(identifier.prefix), identifier.suffix, itemPk)
                })
            }

            // Note that :values is a list of tuples.
            // When we insert the unique constraint has temporarily gone so no conflict handling needed.
            npTemplate.update(
                "INSERT INTO item_identifier(prefix, suffix, item_pk) \n" +
                        "   VALUES :values",
                identifierParamSource
            )

        }
    }


    fun startOfflineWarm() {

        // During offline warming, drop this constraint.
        // We will temporarily break it by writing duplicate identifiers. We will then deduplicate and put the constraint back.
        logDuration(logger, "drop index for (prefix, suffix)") {
            npTemplate.update(
                "alter table item_identifier drop constraint IF EXISTS item_identifier_value_key",
                MapSqlParameterSource()
            )
        }

        // This index affects the deduplication stage, as it needs to check the item table for every row.
        // We will ensure that there are no dangling items before putting this index back at the end.
        logDuration(logger, "drop foreign key index for item pk") {
            npTemplate.update(
                "alter table item_identifier drop constraint IF EXISTS item_identifier_item_pk_fkey",
                MapSqlParameterSource()
            )
        }

        logDuration(logger, "drop index for item pk") {
            jdbcTemplate.update("DROP INDEX IF EXISTS item_identifier_item_pk_idx")
        }

    }

    /**
     * This is not transactional in order to allow other threads to observe progress.
     * For the typical entire XML corpus there was a 30% duplicate rate.
     * If this crashes it can be restarted, so there's no reason to isolate it.
     */
    fun finishOfflineWarm() {
        logDuration(logger, "complete offline warming") {
            val preItemCount = getItemCount()
            val preIdentifierCount = getIdentifierCount()

            logger.info("De-duplicating $preItemCount Item Identifiers...")

            // Create an index on the values in order to remove the duplicates.
            // This is a non-unique index, which means it's not useful beyond this operation.
            logDuration(logger, "create temporary index for (prefix, suffix, pk)") {
                npTemplate.update(
                    "create index temp_prefix_suffix_pk_idx on item_identifier (prefix, suffix, pk)",
                    MapSqlParameterSource()
                )
                npTemplate.update("vacuum item_identifier", MapSqlParameterSource())
                npTemplate.update("analyze item_identifier", MapSqlParameterSource())
            }

            // Delete every duplicate item identifier. Always keep the lowest PK, which means that we remove only newly introduced duplicates.
            // This will mean that any prior assertions and items that were in the database before this offline warming should be unaffected.
            // Build whole new table rather than try to delete, as the resulting table would have 30% holes, needing a VACUUM FULL anyway.
            logDuration(logger, "Rebuilding Identifiers without duplicates") {

                npTemplate.update("CREATE TABLE new_item_identifier (LIKE item_identifier)", MapSqlParameterSource())

                logger.info("This is a long operation, can take of the order of 2 days for the whole corpus.")
                npTemplate.update(
                    "INSERT INTO new_item_identifier (pk, prefix, suffix, item_pk) " +
                            "SELECT pk, prefix, suffix, item_pk " +
                            "FROM (SELECT pk, prefix, suffix, item_pk, row_number() OVER w as r FROM item_identifier WINDOW w AS (PARTITION BY prefix, suffix ORDER BY pk)) t " +
                            "WHERE t.r = 1;", MapSqlParameterSource()
                )

                npTemplate.update("DROP TABLE item_identifier", MapSqlParameterSource())
                npTemplate.update("ALTER TABLE new_item_identifier RENAME TO item_identifier", MapSqlParameterSource())

                // Run these again. They shouldn't have any effect as this is a brand new table, but this ensure that any loose ends are tied up.
                npTemplate.update("VACUUM item_identifier", MapSqlParameterSource())
                npTemplate.update("ANALYZE item_identifier", MapSqlParameterSource())
            }

            // Now our uniqueness should be back, re-instate item_identifier_value_keythat constraint.
            logDuration(logger, "rebuild unique index for item_identifier") {
                npTemplate.update(
                    "ALTER TABLE item_identifier ADD CONSTRAINT item_identifier_value_key UNIQUE (prefix, suffix)",
                    MapSqlParameterSource()
                )
            }

            logDuration(logger, "rebuild item pk index for item_identifier") {
                npTemplate.update("ALTER TABLE item_identifier ADD PRIMARY KEY (pk)", MapSqlParameterSource())

                // This sequence name follows the Postgres convention for BIGSERIAL.
                // Start at the inherited maximum value.

                npTemplate.update("CREATE SEQUENCE item_identifier_pk_seq AS BIGINT", MapSqlParameterSource())
                npTemplate.query("SELECT setval('item_identifier_pk_seq', max(pk)) FROM item_identifier") { it ->
                    logger.info(
                        "Set max PK to ${it.getInt(1)}"
                    )
                }
                npTemplate.update("ALTER TABLE item_identifier ALTER COLUMN pk SET NOT NULL", MapSqlParameterSource())
                npTemplate.update(
                    "ALTER TABLE item_identifier ALTER COLUMN pk SET DEFAULT nextval('item_identifier_pk_seq')",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER SEQUENCE item_identifier_pk_seq OWNED BY item_identifier.pk",
                    MapSqlParameterSource()
                )
                jdbcTemplate.update(
                    "CREATE INDEX IF NOT EXISTS item_identifier_item_pk_idx ON item_identifier (item_pk)")
            }

            // Now we can rebuild items. There's no useful info in there beyond the presence of the Item PK, but it is a valuable table.
            logDuration(logger, "rebuild item table") {
                npTemplate.update(
                    "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_from_item_pk_fkey",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_party_pk_fkey",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_to_item_pk_fkey",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE property_assertion DROP CONSTRAINT IF EXISTS property_assertion_item_pk_fkey",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE property_assertion DROP CONSTRAINT IF EXISTS property_assertion_party_pk_fkey",
                    MapSqlParameterSource()
                )

                npTemplate.update("TRUNCATE item", MapSqlParameterSource())
                npTemplate.update(
                    "INSERT INTO item (pk, created_at) SELECT item_pk, now() FROM item_identifier",
                    MapSqlParameterSource()
                )
            }

            logDuration(logger, "rebuild item index and foreign key integrity") {
                npTemplate.update(
                    "ALTER TABLE item_identifier ADD CONSTRAINT item_identifier_item_pk_fkey FOREIGN KEY (item_pk) REFERENCES item(pk)",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE assertion ADD CONSTRAINT assertion_from_item_pk_fkey FOREIGN KEY (from_item_pk) REFERENCES item(pk)",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE assertion ADD CONSTRAINT assertion_party_pk_fkey FOREIGN KEY (party_pk) REFERENCES item(pk)",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE assertion ADD CONSTRAINT assertion_to_item_pk_fkey FOREIGN KEY (to_item_pk) REFERENCES item(pk)",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE property_assertion ADD CONSTRAINT property_assertion_item_pk_fkey FOREIGN KEY (item_pk) REFERENCES item(pk)",
                    MapSqlParameterSource()
                )
                npTemplate.update(
                    "ALTER TABLE property_assertion ADD CONSTRAINT property_assertion_party_pk_fkey FOREIGN KEY (party_pk) REFERENCES item(pk)",
                    MapSqlParameterSource()
                )
            }

            val postItemCount = getItemCount()
            val postIdentifierCount = getIdentifierCount()

            logger.info("Reduced $preIdentifierCount identifiers to $postIdentifierCount, a reduction of ${preIdentifierCount - postIdentifierCount}")
            logger.info("Reduced $preItemCount items to $postItemCount, a reduction of ${preItemCount - postItemCount}")
        }
    }
}
