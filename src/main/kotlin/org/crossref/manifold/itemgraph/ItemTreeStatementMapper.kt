package org.crossref.manifold.itemgraph

import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.toPropertyStatements
import org.crossref.manifold.itemtree.toRelationshipStatements
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

/** Projection of [RelationshipStatement] that contains the fields considered important for identity in the context of
 * set algebra for statement merging strategies. Fields that should not be considered for identity are not included
 * (i.e. assertedAt, pk).
 */
data class UniqueRelationshipStatement(
    val fromItemPk: Long,
    val relationshipType: String,
    val toItemPk: Long,
    val assertingPartyPk: Long
) {

    constructor(input: RelationshipStatement) : this(
        input.fromItemPk,
        input.relationshipType,
        input.toItemPk,
        input.assertingPartyPk
    )
}

/** Projection of [PropertyStatement] that contains the fields considered important for identity in the context of set
 * algebra for statement merging strategies. Fields that should not be considered for identity are not included (i.e.
 * assertedAt).
 */
data class UniquePropertyStatement(
    val itemPk: Long,
    val values: JsonNode,
    val assertingPartyPk: Long
) {

    constructor(input: PropertyStatement) : this(input.itemPk, input.values, input.assertingPartyPk)
}

/**
 * From this iterable, subtract all items from this set that are present in the other set, produced by the selector
 * function applied ot each entry.
 */
fun <T, K> Iterable<T>.subtractBy(other: Set<T>, selector: (T) -> K): Set<T> {
    val visited = other.mapTo(HashSet(), selector)
    val result = mutableSetOf<T>()
    for (e in this) {
        val key = selector(e)
        if (visited.add(key)) {
            result.add(e)
        }
    }

    return result
}

/**
 * Accepts an envelope of Item Trees, compares to the current Item Graph, and produces a set of Statements.
 * Relationship Statements and Property Statements are different types, but are generated using the same strategy.
 *
 */
@Service
class ItemTreeStatementMapper(
    val relationshipDao: RelationshipDao,
    val propertyDao: PropertyDao,
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /** Given an Item Tree and an Envelope, produce the set of Relationship Statements according to the Statement
     * Operation. See [MergeStrategy] for the various options available.
     */
    fun toRelationshipStatementsByStrategy(
        envelope: Envelope,
        resolvedItemTree: Item,
    ): Collection<RelationshipStatement> {

        val assertingParty = envelope.assertion.assertingParty
        check(assertingParty.pk != null) { "Asserting Party supplied wasn't resolved." }

        // Get the relation triples being asserted.
        val assertedRelationships =
            toRelationshipStatements(resolvedItemTree, assertingParty.pk, envelope.assertion.assertedAt)

        // Get the set of subject PKs so we can retrieve any existing statements attached to them.
        val fromItemPks = assertedRelationships.map(RelationshipStatement::fromItemPk).toSet()

        // Take a strategy.
        return when (envelope.assertion.mergeStrategy) {
            MergeStrategy.NONE -> emptyList()

            // For naive, simply assert the new Relationships regardless of what was there before.
            // This means that anything omitted won't be removed.
            MergeStrategy.NAIVE -> assertedRelationships

            // This means any pre-existing Relationship that was omitted this time will remain in the Item Graph.
            MergeStrategy.UNION_CLOSED -> {

                // Figure out which ones were there before and don't re-add them.
                val preAssertedRelationships =
                    relationshipDao.getCurrentSubjRelationshipStatements(listOf(assertingParty.pk), fromItemPks)

                // Relationships that were previously un-asserted.
                assertedRelationships.subtractBy(preAssertedRelationships) { UniqueRelationshipStatement(it) }
            }

            // This means any pre-existing Relationship that was omitted this time be removed.
            MergeStrategy.CLOSED -> {

                // Figure out which ones were there before and don't re-add them.
                val preAssertedRelationships =
                    relationshipDao.getCurrentSubjRelationshipStatements(listOf(assertingParty.pk), fromItemPks)

                val toRemove = preAssertedRelationships.subtractBy(assertedRelationships) { UniqueRelationshipStatement(it) }
                val removal = toRemove.map { it.copy(status=false) }

                // Relationships that were previously un-asserted.
                val all = assertedRelationships.subtractBy(preAssertedRelationships) { UniqueRelationshipStatement(it) } + removal

                all
            }
        }
    }

    /** Given an Item Tree and an Envelope, produce the set of Property Statements according to the Statement
     * Operation. See [MergeStrategy] for the various options available.
     */
    fun toPropertyStatementsByStrategy(
        envelope: Envelope,
        resolvedItemTree: Item,
    ): Collection<PropertyStatement> {

        val assertingParty = envelope.assertion.assertingParty
        check(assertingParty.pk != null) { "Asserting Party supplied wasn't resolved." }

        // Convert the tree to a set of Properties, one for each Item in the tree.
        val assertedProperties =
            toPropertyStatements(resolvedItemTree, assertingParty.pk, envelope.assertion.assertedAt)

        // Get the set of subject PKs, so we can retrieve any existing statements attached to them.
        val fromItemPks = assertedProperties.map(PropertyStatement::itemPk).toSet()

        // Take a strategy.
        return when (envelope.assertion.mergeStrategy) {
            MergeStrategy.NONE -> emptyList()

            // For naive, simply assert the new Relationships regardless of what was there before.
            // Because each Property object represents all properties on an Item, the whole item's properties will be
            // replaced by the new Property object.
            MergeStrategy.NAIVE -> assertedProperties

            MergeStrategy.UNION_CLOSED -> {
                // Retrieve all prior Property statements for these Items by this asserting party.
                // The database ensures that (asserting party, item) tuple is unique, so we can safely assume that.
                val preAssertedProperties =
                    propertyDao.getCurrentSubjPropertyStatements(listOf(assertingParty.pk), fromItemPks)

                // Group them by the Item PK they are associated with.
                val preByItem = preAssertedProperties.map { Pair(it.itemPk, it) }.toMap()
                val newByItem = assertedProperties.map { Pair(it.itemPk, it) }.toMap()

                val merged = newByItem.mapNotNull { (newPk, newProperties) ->
                    val pre = preByItem[newPk]
                    when (pre) {
                        null -> {
                            // If it didn't exist, simply assert it.
                            newProperties
                        }
                        newProperties -> {
                            // If there is no change do not re-assert, leave the old one in-place.
                            // Whilst it would be fine to re-assert, it would be huge waste of database!
                            null
                        }
                        else -> {
                            pre.replace(newProperties)
                        }
                    }
                }
                merged
            }

            // Closed strategy simply replaces the whole set at once.
            MergeStrategy.CLOSED -> assertedProperties
        }
    }
}
