package org.crossref.manifold.itemgraph

import org.crossref.manifold.bulk.EnvelopeBatchArchiveIngester
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.springframework.stereotype.Service

/**
 * An Item Graph wrapper that enables bulk offline ingestion.
 * This is used for ingestion of whole corpora, e.g. hundreds of millions of assertions.
 *
 * This class is split into two halves: warming identifiers in bulk, and then ingesting in bulk. See
 * [EnvelopeBatchArchiveIngester] for a discussion of the stages.
 *
 * Not for use on a running system as it temporarily removes indexes.
 */
@Service
class ItemGraphBulk(
    val itemDao: ItemDao,
    val propertyDao: PropertyDao,
    val relationshipDao: RelationshipDao,
    val identifierTypeRegistry: IdentifierTypeRegistry,
    val itemGraph: ItemGraph) {

    /** Start an Identifier warming session.
     * This will drop indexes for speed of insertion.
     */
    fun startIdentifiers() {
        itemDao.startOfflineWarm()
    }

    /** Insert Identifiers within an Identifier warming session.
     */
    fun insertIdentifiers(identifiers: Set<Identifier>) {
        val tokenized = identifiers.mapNotNull {
            identifierTypeRegistry.tokenize(it).tokenized
        }

        return itemDao.insertIdentifiersBulk(tokenized)
    }

    /** Finish an Identifier warming session.
     * This will rebuild indexes and unique constraints to restore the database to the online state.
     */
    fun finishIdentifiers() {
        itemDao.finishOfflineWarm()
    }

    /** Start a Statement ingestion session.
     * This will drop indexes for speed of insertion.
     */
    fun startStatements() {
        propertyDao.startOfflineIngest()
        relationshipDao.startOfflineIngest()
    }

    /**
     * Insert assertions from a list of EnvelopeBatches within a Statement ingestion session. Statements are inserted
     * with the 'current' flag false for speed of insertion. Those 'current' flags are fixed up [finishStatements].
     */
    fun assertStatements(batches: List<EnvelopeBatch>) {
        for (batch in batches) {
            // Call out to ItemGraph as it's optimised for this use case too. It handles resolution.
            itemGraph.assert(batch, true)
        }
    }

    /** Finish a Statement ingestion session.
     * This will rebuild indexes and unique constraints to restore the database to the online state.
     * It will then set 'current' flags to the latest statement per asserting party.
     */
    fun finishStatements() {
        propertyDao.finishOfflineIngest()
        relationshipDao.finishOfflineIngest()

        propertyDao.fixupCurrent()
        relationshipDao.fixupCurrent()
    }
}