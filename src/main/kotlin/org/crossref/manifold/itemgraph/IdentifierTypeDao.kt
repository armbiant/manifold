package org.crossref.manifold.itemgraph;

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Repository

/**
 * DAO For the registry of Identifier Types.
 */
@Repository
class IdentifierTypeDao(
    val jdbcTemplate: JdbcTemplate,
    val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Resolve an Identifier Type Prefix to an Identifier Type PK, creating if necessary.
     */
    fun resolve(prefix: String, blank: Boolean): Int {
        val params = mapOf("prefix" to prefix, "blank" to blank)

        return npTemplate.queryForList(
            "select pk from identifier_type where prefix = :prefix",
            params,
            Int::class.java
        ).firstOrNull() ?: SimpleJdbcInsert(jdbcTemplate).withTableName("identifier_type")
            .apply { usingGeneratedKeyColumns("pk") }
            .executeAndReturnKey(
                params
            ).toInt()
    }
}


