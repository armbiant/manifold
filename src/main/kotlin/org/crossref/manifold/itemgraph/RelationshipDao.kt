package org.crossref.manifold.itemgraph

import java.sql.ResultSet
import java.sql.Types
import java.time.OffsetDateTime
import org.crossref.manifold.api.RelationshipTypesView
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemtree.Tokenized
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.retrieval.statements.RelationshipStatementFilter
import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Repository

/** Pair of Identifier PK and corresponding Item PK.
 * A [Pair<Long, Long>] would be confusing and error-prone.
 */
data class IdentifierPkItemPk(val identifierPk: Long, val itemPk: Long)

/**
 * DAO for Property statements within the Item Graph.
 * Contains a cache of Relationship Types to map them in and out of the database because the vocabulary is small and
 * well known, and this is a very hot data path.
 */
@Repository
class RelationshipDao(
    val jdbcTemplate: JdbcTemplate,
    val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
) {
    private val relStmtMapper: RowMapper<RelationshipStatement> = RowMapper { rs, _ ->
        RelationshipStatement(
            fromItemPk = rs.getLong("from_item_pk"),
            relationshipType = rs.getString("relationship_type_value"),
            toItemPk = rs.getLong("to_item_pk"),
            assertingPartyPk = rs.getLong("party_pk"),
            status = true,
            assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
            pk = rs.getLong("assertion_pk")
        )
    }

    var logger: Logger = LoggerFactory.getLogger(this::class.java)


    /**
     * Retrieve the pk for this RelType, if known.
     */
    fun resolveRelationshipTypeRO(relationshipType: String): Long? {
        val params = mapOf("value" to relationshipType)

        return npTemplate.queryForList(
            "select pk from relationship_type where value = :value",
            params,
            Long::class.java
        ).firstOrNull()?.toLong()
    }


    /**
     * Retrieve the pk for this RelType.
     * Create if necessary.
     */
    fun resolveRelationshipType(relationshipType: String): Long {
        val params = mapOf("value" to relationshipType)

        return resolveRelationshipTypeRO(relationshipType)
            ?: SimpleJdbcInsert(jdbcTemplate).withTableName("relationship_type")
                .apply { usingGeneratedKeyColumns("pk") }
                .executeAndReturnKey(
                    params
                ).toLong()
    }

    /** Insert the given [RelationshipStatement]s into the Item Graph. These can be from more than one Envelope.
     * @param[relationshipStatements] collection of Pairs of Relationship Statement and their corresponding Envelope PK.
     */
    fun assertRelationships(
        relationshipStatements: Collection<Pair<RelationshipStatement, Long>>,
        relationshipTypeRegistry: RelationshipTypeRegistry,
        skipCurrent: Boolean = false
    ) {

        // Resolve the relationship types into known non-null relationship type PKs.
        // If there are any relationship types that aren't recognised then they are discarded at this point.
        // Carry the Envelope Batches through.
        val withRelTypePks: List<Triple<RelationshipStatement, Long?, Long>> =
            relationshipStatements.mapNotNull { (statement, envelopePk) ->

                val resolved = relationshipTypeRegistry.resolve(statement.relationshipType)
                if (resolved == null) {
                    logger.error("Didn't recognise relationship type: ${statement.relationshipType}")
                    null
                } else {
                    Triple(statement, envelopePk, resolved)
                }
            }



        val props = withRelTypePks.map { (statement, envelopePk, relationshipTypePk) ->
            MapSqlParameterSource(
                mapOf(
                    "asserted_at" to statement.assertedAt,
                    "party_pk" to statement.assertingPartyPk,
                    "from_item_pk" to statement.fromItemPk,
                    "to_item_pk" to statement.toItemPk,
                    "relationship_type_pk" to relationshipTypePk,
                    "envelope_pk" to envelopePk,
                    "state" to statement.status,

                    // Current is always true (i.e. this is the latest) unless it's specifically disabled.
                    "current" to !skipCurrent
                )
            ).apply { registerSqlType("asserted_at", Types.TIMESTAMP_WITH_TIMEZONE) }
        }.toTypedArray()
        npTemplate.batchUpdate(
            "insert into assertion (" +
                    "asserted_at, party_pk, from_item_pk, to_item_pk, relationship_type_pk, state, envelope_pk, current" +
                    ") values (" +
                    ":asserted_at, :party_pk, :from_item_pk, :to_item_pk, :relationship_type_pk, :state, :envelope_pk, :current" +
                    ")",
            props
        )
    }


    fun getRelationshipTypes(): RelationshipTypesView =
        RelationshipTypesView(jdbcTemplate.queryForList("select value from relationship_type", String::class.java))

    /**
     * For a set of Relationship PKs, retrieve those that are currently already asserted by the Party Item PK.
     */
    fun getCurrentSubjRelationshipStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>
    ): Set<RelationshipStatement> =
        // SQL can't cope with empty lists.
        if (partyPks.isEmpty() || subjectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, relationship_type.value AS relationship_type_value, to_item_pk, party_pk " +
                        "FROM assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND from_item_pk IN (:from_item_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state = TRUE",
                mapOf("party_pks" to partyPks, "from_item_pks" to subjectItemPks)
            ) { rs, _ ->
                RelationshipStatement(
                    fromItemPk = rs.getLong("from_item_pk"),
                    relationshipType = rs.getString("relationship_type_value"),
                    toItemPk = rs.getLong("to_item_pk"),
                    assertingPartyPk = rs.getLong("party_pk"),
                    status = true,
                    assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
                )
            }.toSet()
        }

    /**
     * For a set of Relationship PKs, retrieve those that are currently already asserted by the Party Item PK.
     */
    fun getCurrentSubjRelationshipStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>,
        relTypes: Set<Long>
    ): Set<RelationshipStatement> =
        // SQL can't cope with empty lists.
        if (partyPks.isEmpty() || subjectItemPks.isEmpty() || relTypes.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, relationship_type.value AS relationship_type_value, to_item_pk, party_pk " +
                        "FROM assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND from_item_pk in (:from_item_pks) " +
                        "AND relationship_type_pk IN (:relationship_type_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state = TRUE ",
                mapOf(
                    "party_pks" to partyPks,
                    "from_item_pks" to subjectItemPks,
                    "relationship_type_pks" to relTypes
                )
            ) { rs, _ ->
                RelationshipStatement(
                    fromItemPk = rs.getLong("from_item_pk"),
                    relationshipType = rs.getString("relationship_type_value"),
                    toItemPk = rs.getLong("to_item_pk"),
                    assertingPartyPk = rs.getLong("party_pk"),
                    status = true,
                    assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
                )
            }.toSet()
        }

    /**
     * For a set of Relationship PKs, get those asserted by anyone.
     */
    fun getCurrentSubjRelationshipStatements(subjectItemPks: Set<Long>): Set<RelationshipStatement> =
        // SQL can't cope with empty lists.
        if (subjectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT assertion.pk AS assertion_pk, asserted_at, from_item_pk, " +
                        "relationship_type.value AS relationship_type_value, to_item_pk, party_pk " +
                        "FROM assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND from_item_pk in (:from_item_pks) " +
                        "AND state = TRUE ",
                mapOf("from_item_pks" to subjectItemPks), relStmtMapper
            ).toSet()
        }

    /**
     * For a set of Relationship PKs, get those asserted by anyone.
     */
    fun getCurrentObjRelationshipStatements(objectItemPks: Set<Long>): Set<RelationshipStatement> =
        // SQL can't cope with empty lists.
        if (objectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT assertion.pk AS assertion_pk, asserted_at, from_item_pk, " +
                        "relationship_type.value AS relationship_type_value, to_item_pk, party_pk, " +
                        "FROM assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND to_item_pk in (:to_item_pks) " +
                        "AND state = TRUE ",

                mapOf("to_item_pks" to objectItemPks), relStmtMapper
            ).toSet()
        }

    /**
     * For a set of Relationship PKs, retrieve those that are currently already asserted by the Party Item PK.
     */
    fun getCurrentRelObjStatements(
        partyPks: Collection<Long>,
        objectItemPks: Set<Long>
    ): Set<RelationshipStatement> =
        // SQL can't cope with empty lists.
        if (partyPks.isEmpty() || objectItemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "select asserted_at, from_item_pk, relationship_type.value AS relationship_type_value, to_item_pk, party_pk " +
                        "from assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND to_item_pk IN (:to_item_pks) " +
                        "and party_pk IN (:party_pks) " +
                        "and state = TRUE ",
                mapOf("party_pks" to partyPks, "to_item_pks" to objectItemPks)
            ) { rs, _ ->
                RelationshipStatement(
                    fromItemPk = rs.getLong("from_item_pk"),
                    relationshipType = rs.getString("relationship_type_value"),
                    toItemPk = rs.getLong("to_item_pk"),
                    assertingPartyPk = rs.getLong("party_pk"),
                    status = true,
                    assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
                )
            }.toSet()
        }

    /** Get all statements, current or not.
     */
    fun getAllStatements(
        partyPk: Long, fromPk: Long, relationshipTypePk: Long, toPk: Long, state: Boolean = true, current: Boolean = true
    ): Set<RelationshipStatement> =
        npTemplate.query(
            "SELECT asserted_at, from_item_pk, relationship_type.value as relationship_type_value, to_item_pk, party_pk, state " +
                    "FROM assertion " +
                    "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                    "WHERE from_item_pk = :from_item_pk " +
                    "AND to_item_pk = :to_item_pk " +
                    "AND current = :current " +
                    "AND relationship_type_pk = :relationship_type_pk " +
                    "AND party_pk = :party_pk " +
                    "AND state = :state",
            mapOf(
                "party_pk" to partyPk,
                "from_item_pk" to fromPk,
                "relationship_type_pk" to relationshipTypePk,
                "to_item_pk" to toPk,
                "current" to current,
            "state" to state
            )
        ) { rs, _ ->
            RelationshipStatement(
                fromItemPk = rs.getLong("from_item_pk"),
                relationshipType = rs.getString("relationship_type_value"),
                toItemPk = rs.getLong("to_item_pk"),
                assertingPartyPk = rs.getLong("party_pk"),
                status = rs.getBoolean("state"),
                assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
            )
        }.toSet()

    /**
     * Find current Relationship Statements to the Object, asserted by the Party.
     * I.e. Match triples < subjects , relationships , * >
     */
    fun getCurrentSubjRelStatements(
        partyPks: Collection<Long>,
        subjectItemPks: Set<Long>,
        relationshipTypePks: Set<Long>,
    ): Set<RelationshipStatement> {

        // SQL can't cope with empty lists.
        return if (partyPks.isEmpty() || subjectItemPks.isEmpty() || relationshipTypePks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, relationship_type.value as relationship_type_value, to_item_pk, party_pk " +
                        "FROM assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND from_item_pk in (:from_item_pks) " +
                        "AND relationship_type_pk IN (:relationship_type_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state = TRUE ",
                mapOf(
                    "party_pks" to partyPks,
                    "from_item_pks" to subjectItemPks,
                    "relationship_type_pks" to relationshipTypePks
                )
            ) { rs, _ -> buildRelationshipStatement(rs)
            }.toSet()
        }
    }

    /**
     * Find current Relationship Statements from the Subject, asserted by the Party.
     * I.e. Match triples < * , relationships , objects >
     */
    fun getCurrentRelObjStatements(
        partyPks: Collection<Long>,
        objItemPks: Set<Long>,
        relationshipTypePks: Set<Long>,
    ): Set<RelationshipStatement> {

        // SQL can't cope with empty lists.
        return if (partyPks.isEmpty() || objItemPks.isEmpty() || relationshipTypePks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, from_item_pk, relationship_type.value as relationship_type_value, to_item_pk, party_pk " +
                        "FROM assertion " +
                        "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                        "WHERE current = TRUE " +
                        "AND to_item_pk in (:to_item_pks) " +
                        "AND relationship_type_pk IN (:relationship_type_pks) " +
                        "AND party_pk IN (:party_pks) " +
                        "AND state = TRUE ",
                mapOf(
                    "party_pks" to partyPks,
                    "to_item_pks" to objItemPks,
                    "relationship_type_pks" to relationshipTypePks
                )
            ) { rs, _ -> buildRelationshipStatement(rs)
            }.toSet()
        }
    }

    fun buildRelationshipStatement(rs: ResultSet) = RelationshipStatement(
        fromItemPk = rs.getLong("from_item_pk"),
        relationshipType = rs.getString("relationship_type_value"),
        toItemPk = rs.getLong("to_item_pk"),
        assertingPartyPk = rs.getLong("party_pk"),
        status = true,
        assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
    )

    /**
     * Fetch a page of Statement Items starting from the given Statement Pk.
     * Return an optional starting Statement Pk if there are more items.
     */
    fun getRelationshipStatementRange(
        startPk: Long,
        count: Int,
        filter: RelationshipStatementFilter
    ): List<Pair<RelationshipStatement, EnvelopeBatchProvenance>> =
        npTemplate.query(
            "SELECT assertion.pk as assertion_pk, " +
                    "assertion.from_item_pk as assertion_from_item_pk, " +
                    "assertion.to_item_pk as assertion_to_item_pk, " +
                    "assertion.relationship_type_pk as assertion_relationship_type_pk, " +
                    "assertion.party_pk as assertion_party_pk, " +
                    "assertion.state as assertion_state, " +
                    "assertion.envelope_pk as assertion_envelope_pk, " +
                    "assertion.asserted_at as assertion_asserted_at, " +
                    "envelope_batch.pk as envelope_batch_pk, " +
                    "envelope_batch.user_agent as envelope_batch_user_agent, " +
                    "envelope_batch.ext_trace as envelope_batch_ext_trace, " +
                    "relationship_type.value as relationship_type_value " +
                    "FROM assertion " +
                    "JOIN envelope ON assertion.envelope_pk = envelope.pk " +
                    "JOIN envelope_batch ON envelope.envelope_batch_pk = envelope_batch.pk " +
                    "JOIN relationship_type ON relationship_type.pk = assertion.relationship_type_pk " +
                    "WHERE assertion.pk >= :start_pk " +
                    (if (filter.subjectPk != null) {
                        "AND from_item_pk = :from_item_pk "
                    } else "") +
                    (if (filter.objectPk != null) {
                        "AND to_item_pk = :to_item_pk "
                    } else "") +
                    (if (filter.relationshipTypePk != null) {
                        "AND relationship_type_pk = :relationship_type_pk "
                    } else "") +
                    (if (filter.partyPk != null) {
                        "AND party_pk = :party_pk "
                    } else "") +
                    "LIMIT :count",
            mapOf(
                "start_pk" to startPk,
                "count" to count,
                "from_item_pk" to filter.subjectPk,
                "to_item_pk" to filter.objectPk,
                "relationship_type_pk" to filter.relationshipTypePk,
                "party_pk" to filter.partyPk
            )
        ) { rs, _ ->
            Pair(relationshipStatementFromRs(rs), envelopeProvenanceFromRs(rs))
        }

    fun startOfflineIngest() {
        val drops = listOf(
            "assertion_lookup" to "DROP INDEX IF EXISTS assertion_lookup",
            "assertion_envelope_idx" to "DROP INDEX IF EXISTS assertion_envelope_idx",
            "assertion_from_item_pk_idx" to "DROP INDEX IF EXISTS assertion_from_item_pk_idx",
            "assertion_party_pk_idx" to "DROP INDEX IF EXISTS assertion_party_pk_idx",
            "assertion_relationship_type_pk_idx" to "DROP INDEX IF EXISTS assertion_relationship_type_pk_idx",
            "assertion_to_item_pk_idx" to "DROP INDEX IF EXISTS assertion_to_item_pk_idx",
            "assertion_envelope_pk_fkey" to "DROP INDEX IF EXISTS assertion_envelope_pk_fkey",
            "assertion_from_item_pk_fkey" to "DROP INDEX IF EXISTS assertion_from_item_pk_fkey",
            "assertion_party_pk_fkey" to "DROP INDEX IF EXISTS assertion_party_pk_fkey",
            "assertion_relationship_type_pk_fkey" to "DROP INDEX IF EXISTS assertion_relationship_type_pk_fkey",
            "assertion_to_item_pk_fkey" to "DROP INDEX IF EXISTS assertion_to_item_pk_fkey",
            "assertion_envelope_pk_fkey" to "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_envelope_pk_fkey",
            "assertion_from_item_pk_fkey" to "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_from_item_pk_fkey",
            "assertion_party_pk_fkey" to "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_party_pk_fkey",
            "assertion_relationship_type_pk_fkey" to "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_relationship_type_pk_fkey",
            "assertion_to_item_pk_fkey" to "ALTER TABLE assertion DROP CONSTRAINT IF EXISTS assertion_to_item_pk_fkey",
            "current_assertion" to "ALTER TABLE assertion DISABLE TRIGGER current_assertion",
        )

        drops.forEach { (name, command) ->
            logDuration(logger, "drop $name") {
                npTemplate.update(
                    command,
                    MapSqlParameterSource()
                )
            }
        }

    }

    /**
     * Create those indexes that we temporarily dropped in startOfflineIngest.
     * Pay extra attention to keep these in sync with any changes to the flyway migration files.
     */
    fun finishOfflineIngest() {
        val creates = listOf(
            "assertion_lookup" to "CREATE INDEX IF NOT EXISTS assertion_lookup ON assertion (from_item_pk, relationship_type_pk, to_item_pk, party_pk)",
            "assertion_envelope_idx" to "CREATE INDEX IF NOT EXISTS assertion_envelope_idx ON assertion (envelope_pk)",
            "assertion_party_pk_idx" to "CREATE INDEX IF NOT EXISTS assertion_party_pk_idx on assertion (party_pk)",
            "assertion_from_item_pk_idx" to "CREATE INDEX IF NOT EXISTS assertion_from_item_pk_idx on assertion (from_item_pk)",
            "assertion_to_item_pk_idx" to "CREATE INDEX IF NOT EXISTS assertion_to_item_pk_idx on assertion (to_item_pk)",
            "assertion_relationship_type_pk_idx" to "CREATE INDEX IF NOT EXISTS assertion_relationship_type_pk_idx on assertion (relationship_type_pk)",
            "assertion_envelope_pk_fkey" to "ALTER TABLE assertion ADD CONSTRAINT assertion_envelope_pk_fkey FOREIGN KEY (envelope_pk) REFERENCES envelope(pk)",
            "assertion_from_item_pk_fkey" to "ALTER TABLE assertion ADD CONSTRAINT assertion_from_item_pk_fkey FOREIGN KEY (from_item_pk) REFERENCES item(pk)",
            "assertion_party_pk_fkey" to "ALTER TABLE assertion ADD CONSTRAINT assertion_party_pk_fkey FOREIGN KEY (party_pk) REFERENCES item(pk)",
            "assertion_relationship_type_pk_fkey" to "ALTER TABLE assertion ADD CONSTRAINT assertion_relationship_type_pk_fkey FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type(pk)",
            "assertion_to_item_pk_fkey" to "ALTER TABLE assertion ADD CONSTRAINT assertion_to_item_pk_fkey FOREIGN KEY (to_item_pk) REFERENCES item(pk)",
            "current_assertion" to "ALTER TABLE assertion ENABLE TRIGGER current_assertion",
        )

        creates.forEach { (name, command) ->
            logDuration(logger, "create $name") {
                npTemplate.update(
                    command,
                    MapSqlParameterSource()
                )
            }
        }

    }

    /**
     * Update the 'current' flags for all assertions to set the most recent to be current.
     * Uses the `assertion_lookup` index, which is should be re-added with `finishOfflineIngest` before calling this.
     */
    fun fixupCurrent() {
        logger.info("Updating 'latest' flags on all relationship assertions. This may take a while.")
        npTemplate.update("UPDATE assertion SET current = FALSE WHERE current = TRUE", MapSqlParameterSource())

        npTemplate.update(
            "UPDATE assertion a\n" +
                    "SET    current = true\n" +
                    "WHERE  NOT EXISTS (\n" +
                    "   SELECT FROM assertion x\n" +
                    "   WHERE (x.from_item_pk, x.relationship_type_pk, x.to_item_pk, x.party_pk)\n" +
                    "       = (a.from_item_pk, a.relationship_type_pk, a.to_item_pk, a.party_pk)\n" +
                    "   AND   x.pk > a.pk\n" +
                    "   );", MapSqlParameterSource()
        )
        logger.info("Finished 'latest' flags on all relationship assertions!")
    }

    fun getCurrentRelsResolvingReified(
        itemPks: Set<Long>
    ): Set<RelationshipStatement> =
        if (itemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT DISTINCT a.pk AS assertion_pk, a.asserted_at, a.from_item_pk, " +
                        "rt.value AS relationship_type_value, a.to_item_pk, a.party_pk " +
                        "FROM assertion a " +
                        "JOIN item_identifier fii ON a.from_item_pk = fii.item_pk " +
                        "JOIN identifier_type fit ON fii.prefix = fit.pk " +
                        "JOIN relationship_type rt ON a.relationship_type_pk = rt.pk " +
                        "JOIN item_identifier tii ON a.to_item_pk = tii.item_pk " +
                        "JOIN identifier_type tit ON tii.prefix = tit.pk " +
                        "WHERE a.current = TRUE " +
                        "AND a.state = TRUE " +
                        "AND fit.blank = FALSE " +
                        "AND tit.blank = FALSE " +
                        "AND rt.value != 'object' " +
                        "AND (a.from_item_pk IN (:item_pks) OR a.to_item_pk IN (:item_pks)) " +
                        "UNION " +
                        "SELECT DISTINCT fa.pk AS assertion_pk, fa.asserted_at, fa.from_item_pk, " +
                        "frt.value AS relationship_type_value, ta.to_item_pk, fa.party_pk " +
                        "FROM assertion fa " +
                        "JOIN item_identifier ffii ON fa.from_item_pk = ffii.item_pk " +
                        "JOIN identifier_type ffit ON ffii.prefix = ffit.pk " +
                        "JOIN relationship_type frt ON fa.relationship_type_pk = frt.pk " +
                        "JOIN item_identifier ftii ON fa.to_item_pk = ftii.item_pk " +
                        "JOIN identifier_type ftit ON ftii.prefix = ftit.pk " +
                        "JOIN assertion ta ON fa.to_item_pk = ta.from_item_pk " +
                        "JOIN relationship_type trt ON ta.relationship_type_pk = trt.pk " +
                        "JOIN item_identifier ttii ON ta.to_item_pk = ttii.item_pk " +
                        "JOIN identifier_type ttit ON ttii.prefix = ttit.pk " +
                        "WHERE fa.current = TRUE " +
                        "AND fa.state = TRUE " +
                        "AND ta.current = TRUE " +
                        "AND ta.state = TRUE " +
                        "AND ffit.blank = FALSE " +
                        "AND ftit.blank = TRUE " +
                        "AND frt.value != 'object' " +
                        "AND ttit.blank = FALSE " +
                        "AND trt.value = 'object' " +
                        "AND fa.from_item_pk IN (:item_pks) " +
                        "UNION " +
                        "SELECT DISTINCT fa.pk AS assertion_pk, fa.asserted_at, fa.from_item_pk, " +
                        "frt.value AS relationship_type_value, ta.to_item_pk, fa.party_pk " +
                        "FROM assertion fa " +
                        "JOIN item_identifier ffii ON fa.from_item_pk = ffii.item_pk " +
                        "JOIN identifier_type ffit ON ffii.prefix = ffit.pk " +
                        "JOIN relationship_type frt ON fa.relationship_type_pk = frt.pk " +
                        "JOIN item_identifier ftii ON fa.to_item_pk = ftii.item_pk " +
                        "JOIN identifier_type ftit ON ftii.prefix = ftit.pk " +
                        "JOIN assertion ta ON fa.to_item_pk = ta.from_item_pk " +
                        "JOIN relationship_type trt ON ta.relationship_type_pk = trt.pk " +
                        "JOIN item_identifier ttii ON ta.to_item_pk = ttii.item_pk " +
                        "JOIN identifier_type ttit ON ttii.prefix = ttit.pk " +
                        "WHERE fa.current = TRUE " +
                        "AND fa.state = TRUE " +
                        "AND ta.current = TRUE " +
                        "AND ta.state = TRUE " +
                        "AND ffit.blank = FALSE " +
                        "AND ftit.blank = TRUE " +
                        "AND frt.value != 'object' " +
                        "AND ttit.blank = FALSE " +
                        "AND trt.value = 'object' " +
                        "AND ta.to_item_pk IN (:item_pks) " +
                        "ORDER by assertion_pk",
                mapOf("item_pks" to itemPks), relStmtMapper
            ).toSet()
        }
}

fun relationshipStatementFromRs(rs: ResultSet) = RelationshipStatement(
    pk = rs.getLong("assertion_pk"),
    fromItemPk = rs.getLong("assertion_from_item_pk"),
    relationshipType = rs.getString("relationship_type_value"),
    toItemPk = rs.getLong("assertion_to_item_pk"),
    assertingPartyPk = rs.getLong("assertion_party_pk"),
    status = rs.getBoolean("assertion_state"),
    assertedAt = rs.getObject("assertion_asserted_at", OffsetDateTime::class.java)
)

fun envelopeProvenanceFromRs(rs: ResultSet) = EnvelopeBatchProvenance(
    userAgent = rs.getString("envelope_batch_user_agent"),
    externalTrace = rs.getString("envelope_batch_ext_trace"),
    envelopeBatchPk = rs.getLong("envelope_batch_pk"),
)

