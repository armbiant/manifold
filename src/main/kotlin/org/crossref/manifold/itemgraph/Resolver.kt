package org.crossref.manifold.itemgraph


import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.getUnambiguousUnresolvedIdentifiers
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.retrieval.view.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.dao.CannotSerializeTransactionException
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional

/**
 * Resolve Items, and Item Trees against the Item Graph. 'Resolution' means having a known Primary Key (PK) in the
 * database.
 * Read-only resolution will query only for already known Items. It is useful for querying and first-pass on ingestion.
 * Read-write resolution will create Items and Identifiers. It is useful for ingestion.
 */
@Service
class Resolver(
    @Autowired val relationships: RelationshipDao,
    @Autowired val itemDao: ItemDao,
    @Autowired val identifierTypeRegistry: IdentifierTypeRegistry,
    ) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    // This self-referential var will actually be a Spring generated proxy object at runtime.
    // For Spring managed transactions to propagate across function calls, they must be called via this proxy.
    // Functions called directly from within the class bypass the proxy and are therefore not managed by Spring.
    @Lazy
    @Autowired
    lateinit var selfProxy: Resolver

    // Resolve a whole Item Tree in an SQL session.
    // First resolve the Item IDs by linked Identifiers.
    // Shouldn't be needed at resolving time.
    fun resolveTreeRW(inputItem: Item): Item {
        val resolvedItem = selfProxy.resolveRW(inputItem)

        return resolvedItem.withRelationships(resolvedItem.rels.map {
            it.withItem(
                selfProxy.resolveTreeRW(
                    it.obj
                )
            )
        })
    }

    /**
     * Resolve a single Item in the Graph, creating Items and ItemIdentifiers where necessary.
     * Unless it's already resolved, in which case no operation. This is useful as it allows the function to be applied
     * to an entire tree, and resolves only the bits that are not yet resolved.
     */
    @Transactional(isolation = Isolation.SERIALIZABLE)
    @Retryable(
        CannotSerializeTransactionException::class,
        maxAttempts = 5,
        backoff = Backoff(random = true, delay = 1000, maxDelay = 20000)
    )
    fun resolveRW(inputItem: Item): Item {
        check(inputItem.identifiers.all { it.tokenized != null }) { "All ItemIdentifiers must have tokenized component." }

        // If this is already resolved, perform no operation.
        if (inputItem.pk !== null && inputItem.identifiers.all { it.pk != null }) {
            return inputItem
        }

        return if (inputItem.hasNoIdentifiers()) {

            // If it's a blank node then create a new Item PK.
            val newItemPk = itemDao.insertNewItem()

            inputItem.withPk(newItemPk)
        } else {
            // If it's not a blank node then we need to reconcile all the Item Identifiers.
            val resolvedItemIdentifiers = inputItem.identifiers.map {

                val identifierPkItemPk = itemDao.findItemAndIdentifierPk(
                    it.tokenized ?: throw Exception("Untokenized identifier passed for resolution.")
                )

                if (identifierPkItemPk != null) {
                    Pair(it.withPk(identifierPkItemPk.identifierPk), identifierPkItemPk.itemPk)
                } else {
                    Pair(it, null)
                }
            }

            // Split into those ItemIdentifiers for which we did find the ID and those that we didn't.
            val (known: List<Pair<Identifier, Long?>>, unknown) = resolvedItemIdentifiers.partition { (_, itemPk) -> itemPk != null }

            // All known ItemIdentifiers must point to the same Item or there's a conflict.
            // From the above partition we know that none of these is null.
            val distinctItemPks = known.mapNotNull(Pair<Identifier, Long?>::second).distinct()

            if (distinctItemPks.count() > 1) {
                val message =
                    known.map { (itemIdentifier, itemId) -> "Identifier: ${itemIdentifier.tokenized} pk: ${itemIdentifier.pk}, maps to Item $itemId. " }
                        .joinToString("; ")
                throw Exception("Found conflict! Conflicting Item Identifiers: $message")
            }

            if (known.isEmpty()) {
                // If there are no known ones, it's time to create a new Item and add the Identifiers.
                val newItemPk = itemDao.insertNewItem()

                val newIdentifiers = unknown.map {
                    val identifierPk = itemDao.createItemIdentifier(
                        newItemPk,
                        it.first.tokenized ?: throw Exception("Untokenized identifier passed for resolution.")
                    )
                    it.first.withPk(identifierPk)
                }

                inputItem.withPk(newItemPk).withIdentifiers(newIdentifiers)
            } else {

                // If there is at least one known one, and they all map to the same Item, add that id to the item
                // and to any remaining unknown identifiers.

                // The list of distinct ids is one long, so we can take the first.
                val itemPk = distinctItemPks.first()

                val newIdentifiers = unknown.map {
                    val identifierPk = itemDao.createItemIdentifier(
                        itemPk,
                        it.first.tokenized ?: throw Exception("Untokenized identifier passed for resolution.")
                    )
                    it.first.withPk(identifierPk)
                }

                // Combine the previous known ItemIdentifiers and any newly created ones.
                val allIdentifiers = newIdentifiers + known.map(Pair<Identifier, Long?>::first)

                inputItem.withPk(itemPk).withIdentifiers(allIdentifiers)
            }
        }
    }

    /**
     * Resolve Items by their identifiers using pre-existing data.
     * This is an optimisation before full tree resolution, and is both contention-safe and better optimised for SQL.
     */
    fun resolveRO(itemTree: Item): Item {
        check(allIdentifiersTokenized(itemTree)) { "All ItemIdentifiers must be Tokenized prior to Item read-only resolution." }

        // The pair mapping describes how to resolve ItemIdentifiers, using only their prefix and suffix.
        val presentPairs = unresolvedIdentifiers(itemTree).mapNotNull { it.tokenized }
        val pairMapping = itemDao.getIdentifierMappingsRO(presentPairs)

        return resolveItemTreeFromIdentifierMappings(
            itemTree,
            pairMapping
        )
    }

    /**
     * Resolve an un-tokenized Identifier.
     * Useful for serving queries that aren't part of a larger ItemTree.
     */
    fun resolveRO(identifier: Identifier): Item =
        resolveRO(Item().withIdentifier(identifierTypeRegistry.tokenize(identifier)))


    /**
     * Resolve all Items in an Envelope Batch by identifiers using pre-existing data.
     * This is optimised to fetch all identifiers in one batch.
     */
    fun resolveRO(input: EnvelopeBatch): EnvelopeBatch {
        check(
            input.envelopes.all { allIdentifiersTokenized(it.assertion.assertingParty) }) { "All Asserting Party Item Identifiers must be Tokenized prior to Item read-only resolution." }

        check(input.envelopes.all { it.itemTrees.all(::allIdentifiersTokenized) }) { "All Item Tree Item Identifiers must be Tokenized prior to Item read-only resolution." }

        val identifiers = getUnambiguousUnresolvedIdentifiers(input).mapNotNull { it.tokenized }

        val mapping = itemDao.getIdentifierMappingsRO(identifiers)

        return input.withEnvelopes(
            input.envelopes.map { envelope ->
                envelope
                    .withAssertion(
                        envelope.assertion.withAssertingParty(
                            resolveItemTreeFromIdentifierMappings(envelope.assertion.assertingParty, mapping)
                        )
                    )
                    .withItemTrees(envelope.itemTrees.map { resolveItemTreeFromIdentifierMappings(it, mapping) })
            })

    }

    // This inserts in bulk and then retrieves from the database
    fun resolveRW(input: EnvelopeBatch): EnvelopeBatch {
        check(
            input.envelopes.all { allIdentifiersTokenized(it.assertion.assertingParty) }) { "All Asserting Party Item Identifiers must be Tokenized prior to Item read-only resolution." }

        check(input.envelopes.all { it.itemTrees.all(::allIdentifiersTokenized) }) { "All Item Tree Item Identifiers must be Tokenized prior to Item read-only resolution." }

        val identifiers = getUnambiguousUnresolvedIdentifiers(input).mapNotNull { it.tokenized }

        itemDao.insertIdentifiersBulk(identifiers)

        return resolveRO(input)
    }
}


/**
 * Fully or partially resolve an Item Tree based on the ItemIdentifier mapping.
 */
fun resolveItemTreeFromIdentifierMappings(
    itemTree: Item, identifierMappings: Map<Tokenized, IdentifierPkItemPk>
): Item {
    fun recurse(item: Item): Item {
        val resolved = resolveItemFromIdentifierMappings(item, identifierMappings)
        return resolved.withRelationships(resolved.rels.map { rel ->
            rel.withItem(recurse(rel.obj))
        })
    }

    return recurse(itemTree)
}

/**
 * Fully or partially resolve an Item based on the ItemIdentifier mapping.
 */
fun resolveItemFromIdentifierMappings(
    item: Item,
    identifierMappings: Map<Tokenized, IdentifierPkItemPk>
): Item {

    // Resolve the pairs of maybe-resolved ItemIdentifier, Item PK. There may be nulls.
    // If the ItemIdentifier wasn't known in the mapping, return it back with a null Item PK.
    val foundItemIdentifierItemPks: Collection<Pair<Identifier, Long?>> =
        item.identifiers.map {
            val identifierPkItemPk = identifierMappings[it.tokenized]
            if (identifierPkItemPk == null) {
                Pair(it, null)
            } else {
                Pair(it.withPk(identifierPkItemPk.identifierPk), identifierPkItemPk.itemPk)
            }
        }

    // The list of Identifiers. At this point they may or may not be resolved!
    val maybeResolvedIdentifiers = foundItemIdentifierItemPks.map(Pair<Identifier, Long?>::first)

    return if (item.identifiers.isEmpty()) {
        // If there are no Identifiers then we'll need to create a blank node, which happens in read-write resolution.
        // Pass through.
        item
    } else if (foundItemIdentifierItemPks.any { it.first.pk == null }) {
        // If any of the ItemIdentifiers was null, we failed to resolve it. We'll need read-write resolution, so pass.
        // Important for the relationship between read-only and read-write resolution:
        // Do not attach ItemIdentifier PKs, even if we know them, because the read-write resolution will need to fetch them again and create new item identifiers in a single transaction.
        item
    } else {
        // Now we're confident that all identifiers did resolve, we need to ask if the result was unambiguous.
        // Again, we can assume that there are no nulls in the Item PK field if all the Items resolved.
        // So, filtering out the nulls will have no effect but to remove the nullability of the type.
        val distinctItemPks = foundItemIdentifierItemPks.asSequence().mapNotNull { pair -> pair.second }.distinct()
            .toList()

        if (distinctItemPks.isEmpty()) {
            // If itemIdentifiers is not empty then the distinct values will not be empty.
            // This branch shouldn't ever happen. If it somehow does, fail-safe.
            item
        } else if (distinctItemPks.count() > 1) {
            // If they pointed to more than one distinct Item PK, that's a conflict.
            // We can't resolve it, so pass through item unchanged.
            item
        } else {
            // Else they point to one distinct Item PK. This is a successful match.
            val itemPk = distinctItemPks.first()

            // At this point we know they are all resolved.
            item.withPk(itemPk).withIdentifiers(maybeResolvedIdentifiers)
        }
    }
}

