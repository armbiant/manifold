package org.crossref.manifold.itemgraph

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.retrieval.statements.PropertyStatementFilter
import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import java.sql.Types
import java.time.OffsetDateTime

/**
 * DAO for Property statements within the Item Graph.
 */
@Repository
class PropertyDao(
    @Autowired
    val jdbcTemplate: JdbcTemplate,

    @Autowired
    val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Insert [PropertyStatement] into the Item Graph as the latest, unless @param[skipCurrent] is true.
     * These can be from multiple envelopes.
     * Input is pairs of Statement and the Envelope PK.
     */
    fun assertPropertyStatements(
        statements: Collection<Pair<PropertyStatement, Long>>,
        skipCurrent: Boolean = false
    ) {
        val params = statements.map { (statement, envelopePk) ->
            MapSqlParameterSource(
                mapOf(
                    "asserted_at" to statement.assertedAt,
                    "party_pk" to statement.assertingPartyPk,
                    "item_pk" to statement.itemPk,
                    "values" to statement.serialized(),
                    "envelope_pk" to envelopePk,

                    // Current is always true (i.e. this is the latest) unless it's specifically disabled.
                    "current" to !skipCurrent
                )
            ).apply { registerSqlType("asserted_at", Types.TIMESTAMP_WITH_TIMEZONE) }
        }.toTypedArray()

        npTemplate.batchUpdate(
            "insert into property_assertion (" +
                    "asserted_at, party_pk, item_pk, values, envelope_pk, current " +
                    ") values (" +
                    ":asserted_at, :party_pk, :item_pk, :values ::jsonb, :envelope_pk, :current" +
                    ")",
            params
        )
    }


    /**
     * For a set of Item PKs, get all Property statements asserted by the given Party PKs.
     */
    fun getCurrentSubjPropertyStatements(partyPks: Collection<Long>, itemPks: Set<Long>): Set<PropertyStatement> =
        // SQL can't cope with empty lists and the result would be empty in any case.
        if (partyPks.isEmpty() || itemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, party_pk, item_pk, values, envelope_pk " +
                        "FROM property_assertion " +
                        "WHERE current = TRUE " +
                        "AND item_pk IN (:item_pks) " +
                        "AND party_pk IN (:party_pks) ",
                mapOf("party_pks" to partyPks, "item_pks" to itemPks)
            ) { rs, _ ->
                PropertyStatement(
                    itemPk = rs.getLong("item_pk"),
                    values = parseJson(rs.getString("values")),
                    assertingPartyPk = rs.getLong("party_pk"),
                    assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
                )
            }.toSet()
        }

    /**
     * For a set of Item PKs, get all Property statements asserted by any Party.
     */
    fun getCurrentSubjPropertyStatements(itemPks: Set<Long>): Set<PropertyStatement> =
        // SQL can't cope with empty lists and the result would be empty in any case.
        if (itemPks.isEmpty()) {
            emptySet()
        } else {
            npTemplate.query(
                "SELECT asserted_at, party_pk, item_pk, values, envelope_pk " +
                        "FROM property_assertion " +
                        "WHERE current = TRUE " +
                        "AND item_pk IN (:item_pks) ",
                mapOf("item_pks" to itemPks)
            ) { rs, _ ->
                PropertyStatement(
                    itemPk = rs.getLong("item_pk"),
                    values = parseJson(rs.getString("values")),
                    assertingPartyPk = rs.getLong("party_pk"),
                    assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
                )
            }.toSet()
        }


    /**
     * For a set of Item PKs, get all Property statements asserted by any Party.
     */
    fun getAllStatements(itemPk: Long, partyPk: Long, current: Boolean = false): Set<PropertyStatement> =
        npTemplate.query(
            "SELECT asserted_at, party_pk, item_pk, values, envelope_pk " +
                    "FROM property_assertion " +
                    "WHERE current = :current " +
                    "AND item_pk IN (:item_pks) ",
            mapOf(
                "current" to current,
                "item_pks" to itemPk,
                "party_pk" to partyPk,
            )
        ) { rs, _ ->
            PropertyStatement(
                itemPk = rs.getLong("item_pk"),
                values = parseJson(rs.getString("values")),
                assertingPartyPk = rs.getLong("party_pk"),
                assertedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
            )
        }.toSet()


    /**
     * Fetch a page of Statement Items starting from the given Statement Pk.
     * Return an optional starting Statement Pk if there are more items.
     */
    fun getPropertyStatementRange(
        startPk: Long,
        count: Int,
        filter: PropertyStatementFilter
    ): List<Pair<PropertyStatement, EnvelopeBatchProvenance>> =
        npTemplate.query(
            "SELECT property_assertion.pk as property_assertion_pk, " +
                    "property_assertion.item_pk as property_assertion_item_pk, " +
                    "property_assertion.party_pk as property_assertion_party_pk, " +
                    "property_assertion.values as property_assertion_values, " +
                    "property_assertion.envelope_pk as property_assertion_envelope_pk, " +
                    "property_assertion.asserted_at as property_assertion_asserted_at, " +
                    "envelope_batch.pk as envelope_batch_pk, " +
                    "envelope_batch.user_agent as envelope_batch_user_agent, " +
                    "envelope_batch.ext_trace as envelope_batch_ext_trace " +
                    "FROM property_assertion " +
                    "JOIN envelope ON property_assertion.envelope_pk = envelope.pk " +
                    "JOIN envelope_batch ON envelope.envelope_batch_pk = envelope_batch.pk " +
                    "WHERE property_assertion.pk >= :start_pk " +
                    (if (filter.itemPk != null) {
                        "AND item_pk = :item_pk "
                    } else "") +
                    (if (filter.partyPk != null) {
                        "AND party_pk = :party_pk "
                    } else "") +
                    "LIMIT :count",
            mapOf(
                "start_pk" to startPk,
                "count" to count,
                "item_pk" to filter.itemPk,
                "party_pk" to filter.partyPk
            )
        ) { rs, _ ->
            Pair(propertyStatementFromRs(rs), envelopeProvenanceFromRs(rs))
        }


    fun startOfflineIngest() {
        val creates = listOf(
            "property_lookup" to "DROP INDEX IF EXISTS property_lookup",
            "current_property" to "ALTER TABLE property_assertion DISABLE TRIGGER current_property",
            "property_assertion_envelope_pk_idx" to "DROP INDEX IF EXISTS property_assertion_envelope_pk_idx",
            "property_assertion_item_pk_idx" to "DROP INDEX IF EXISTS property_assertion_item_pk_idx",
            "property_assertion_party_pk_idx" to "DROP INDEX IF EXISTS property_assertion_party_pk_idx",
            "property_envelope_idx" to "DROP INDEX IF EXISTS property_envelope_idx",
            "property_assertion_envelope_pk_fkey" to "ALTER TABLE property_assertion DROP CONSTRAINT IF EXISTS property_assertion_envelope_pk_fkey",
            "property_assertion_item_pk_fkey" to "ALTER TABLE property_assertion DROP CONSTRAINT IF EXISTS property_assertion_item_pk_fkey",
            "property_assertion_party_pk_fkey" to "ALTER TABLE property_assertion DROP CONSTRAINT IF EXISTS property_assertion_party_pk_fkey",
        )

        creates.forEach { (name, command) ->
            logDuration(logger, "drop $name") {
                npTemplate.update(
                    command,
                    MapSqlParameterSource()
                )
            }
        }
    }

    fun finishOfflineIngest() {
        val creates = listOf(
            "property_lookup" to "CREATE INDEX IF NOT EXISTS property_lookup ON property_assertion (item_pk, party_pk)",
            "current_property" to "ALTER TABLE property_assertion ENABLE TRIGGER current_property",
            "property_assertion_envelope_pk_idx" to "CREATE INDEX IF NOT EXISTS property_assertion_envelope_pk_idx on property_assertion (envelope_pk)",
            "property_assertion_item_pk_idx" to "CREATE INDEX IF NOT EXISTS property_assertion_item_pk_idx on property_assertion (item_pk)",
            "property_assertion_party_pk_idx" to "CREATE INDEX IF NOT EXISTS property_assertion_party_pk_idx on property_assertion (party_pk)",
            "property_envelope_idx" to "CREATE INDEX IF NOT EXISTS property_envelope_idx ON property_assertion(envelope_pk)",
            "property_assertion_envelope_pk_fkey" to "ALTER TABLE property_assertion ADD CONSTRAINT property_assertion_envelope_pk_fkey FOREIGN KEY (envelope_pk) REFERENCES envelope(pk)",
            "property_assertion_item_pk_fkey" to "ALTER TABLE property_assertion ADD CONSTRAINT property_assertion_item_pk_fkey FOREIGN KEY (item_pk) REFERENCES item(pk)",
            "property_assertion_party_pk_fkey" to "ALTER TABLE property_assertion ADD CONSTRAINT property_assertion_party_pk_fkey FOREIGN KEY (party_pk) REFERENCES item(pk)",
        )

        creates.forEach { (name, command) ->
            logDuration(logger, "create $name") {
                npTemplate.update(
                    command,
                    MapSqlParameterSource()
                )
            }
        }
    }

    /**
     * Update the 'current' flags for all assertions to set the most recent to be current.
     * Uses the `property_lookup` index, call finishOfflineIngest first.
     */
    fun fixupCurrent() {
        logger.info("Updating 'latest' flags on all property assertions. This may take a while.")
        npTemplate.update("UPDATE property_assertion SET current = FALSE WHERE current = TRUE", MapSqlParameterSource())

        npTemplate.update(
            "UPDATE property_assertion a " +
                    "SET current = true " +
                    "WHERE NOT EXISTS (" +
                    "   SELECT FROM property_assertion x " +
                    "   WHERE (x.item_pk, x.party_pk) " +
                    "       = (a.item_pk, a.party_pk) " +
                    "   AND x.pk > a.pk " +
                    ")", MapSqlParameterSource()
        )
        logger.info("Finished 'latest' flags on all property assertions!")
    }

}

fun parseJson(input: String): ObjectNode = ObjectMapper().reader().readTree(input).deepCopy()


fun propertyStatementFromRs(rs: ResultSet) = PropertyStatement(
    pk = rs.getLong("property_assertion_item_pk"),
    itemPk = rs.getLong("property_assertion_item_pk"),
    assertingPartyPk = rs.getLong("property_assertion_party_pk"),
    values = parseJson(rs.getString("property_assertion_values")),
    assertedAt = rs.getObject("property_assertion_asserted_at", OffsetDateTime::class.java)
)