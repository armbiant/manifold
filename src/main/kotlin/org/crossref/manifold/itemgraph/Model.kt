package org.crossref.manifold.itemgraph

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import org.crossref.manifold.itemtree.Item
import java.time.OffsetDateTime

/**
 * Statement of a relationship between two Item PKs.
 * Uses PKs for Items (i.e from, to, party) but a string for the relationship type.
 * The DAO will translate this in and out on retrieval.
 */
data class RelationshipStatement(
    val fromItemPk: Long,
    val relationshipType: String,
    val toItemPk: Long,
    val assertingPartyPk: Long,
    val status: Boolean,
    val assertedAt: OffsetDateTime,
    val pk: Long? = null
)

/**
 * Statement of the value for a given property value.
 * Uses PKs for Items (i.e from, to, party) but a string for the property name type.
 * The DAO will translate this in and out on retrieval.
 */
data class PropertyStatement(
    val itemPk: Long,
    val values: ObjectNode,
    val assertingPartyPk: Long,
    val assertedAt: OffsetDateTime,
    val pk: Long? = null
) {

    fun serialized(): String = ObjectMapper().writer()
        .writeValueAsString(values)

    /** Return a copy with the properties replaced with those provided in the argument.
     */
    fun replace(newProperties: PropertyStatement): PropertyStatement {
        val copy = values.deepCopy()
        for ((k, v) in newProperties.values.fields()) {
            copy.replace(k, v)
        }

        return PropertyStatement(itemPk, copy, assertingPartyPk, assertedAt, pk)
    }
}

/**
 * Various strategies for inserting an Item Tree into the Item Graph.
 * Each strategy describes how to compare the Item Tree against the existing Item Graph, and which Statements to produce
 * to achieve the desired effect.
 * Relationship assertions and property assertions are subtly different - we may want to re-state relationships without
 * affecting properties or vice versa.
 *
 * These are always executed in the scope of the given Asserting Party. They do not affect statements made by other
 * parties.
 */
enum class MergeStrategy(val mode: Int) {
    /**
     * No statements are asserted.
     */
    NONE(0),

    /**
     * All relationship statements are simply asserted without checking any prior assertions.
     * Good for:
     * - Fast initial data load where we know there is no prior data, or where we can add to existing data.
     * - Additive statements where we don't claim to know all assertions in the tree.
     * - Statements where the set of to_item_pks is huge or unbounded, so we can't use a merge strategy that retrieves
     *   them, e.g. maintainer assertions which map all members to all DOIs.
     */
    NAIVE(1),

    /**
     * Assert an Item Tree where the asserter supplies the entire tree. Union with whatever statements were there
     * before. Properties on items are merged, with new values replacing old where present. Relationships are merged,
     * meaning that new Relationships are added and prior ones are not re-asserted.
     * This is called 'closed' because the asserter asserts that the item tree they present represents everything about
     * that item, and is therefore finite. During ingestion, during merging, the entire item tree is retrieved. This
     * means it's unsuitable for unbounded trees, e.g. maintainer assertions, where one member may have millions of
     * DOIs.
     */
    UNION_CLOSED(2),

    /**
     * Asserting party replaces all relationships for the subject item with these.
     * If there were any pre-existing relationships that are not mentioned here, they should be removed.
     */
    CLOSED(3)
}

/**
 * An itemgraph by a member of an Item Tree. Describes who is trying to say what about the Item Tree, and what relationship they have to the Identifiers-to-Register.
 * @param assertedAt date time at which the assertions are made.
 * @param assertingParty the party making the assumption.
 * @param mergeStrategy what strategy to use when incorporating this into the Item Graph.
 */
data class ItemTreeAssertion(
    // Date time at which the assertions are made.
    val assertedAt: OffsetDateTime,

    // What strategy to use when incorporating this into the Item Graph.
    val mergeStrategy: MergeStrategy,

    // Identifier of the Authority.
    val assertingParty: Item,
) {
    fun withAssertingParty(newAssertingParty: Item): ItemTreeAssertion =
        ItemTreeAssertion(assertedAt, mergeStrategy, newAssertingParty)
}
