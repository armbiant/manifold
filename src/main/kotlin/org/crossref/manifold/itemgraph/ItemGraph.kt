package org.crossref.manifold.itemgraph

import io.micrometer.core.instrument.MeterRegistry
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.IngestionDao
import org.crossref.manifold.ingestion.Stage
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.springframework.stereotype.Service

@Service
class ItemGraph(
    val resolver: Resolver,
    val itemTreeStatementMapper: ItemTreeStatementMapper,
    val relationshipTypeRegistry: RelationshipTypeRegistry,
    val identifierTypeRegistry: IdentifierTypeRegistry,
    val propertyDao: PropertyDao,
    val relationshipDao: RelationshipDao,
    val itemDao: ItemDao,
    val ingestionDao: IngestionDao,

    /** Meter registry for metrics. These are all nullable, so it can be disabled for simpler testing.
     */
    val meterRegistry: MeterRegistry?
) {
    /** Given a fresh [EnvelopeBatch], tokenize and resolve, creating Items if needed.
     * Performing this task on the whole EnvelopeBatch means we get large batched database queries, for efficiency.
     */
    fun resolve(batch: EnvelopeBatch): EnvelopeBatch {
        // These are done on the whole envelope batch. The optimization take large chunks of identifiers, so we
        // want to pass them the largest chunk possible.
        val tokenized = tokenize(batch)
        val readOnlyResolved = resolver.resolveRO(tokenized)
        val readWriteResolved = resolver.resolveRW(readOnlyResolved)

        val batchPk = ingestionDao.createEnvelopeBatch(readWriteResolved)

        return readWriteResolved
            .withPk(batchPk)
            .withEnvelopes(readWriteResolved.envelopes.map {
                it.withPk(ingestionDao.createEnvelope(batchPk))
            })
    }

    /**
     * Safely Ingest the batch of Ingestion Envelopes. Log activity (and catch errors) to the ingestion_message table.
     *
     * The return state gives an indication that users can look in the Ingestion History table for more details.
     *
     * If @param[skipCurrent] is true, assertions will not be asserted as current. They get inserted more quickly,
     * as the index on the property and assertion statement tables don't need to be locked. But they do need fixing up
     * later.
     *
     * @return true on success, false on any failure
     */
    fun ingest(batch: EnvelopeBatch, skipCurrent: Boolean = false): Boolean {
        val envelopeBatchPk = ingestionDao.createEnvelopeBatch(batch)
        // This outer try-catch swallows any exception raised to indicate the stage that caused the error condition.
        try {
            ingestionDao.update(envelopeBatchPk, null, Stage.START_BATCH, true)
             assert(batch, skipCurrent)
        } catch (e: Exception) {
            ingestionDao.update(envelopeBatchPk, null, Stage.COMPLETE_BATCH, false, e.message)
            return false
        }
        ingestionDao.update(envelopeBatchPk, null, Stage.COMPLETE_BATCH, true)
        return true
    }

    /** Assert all Item Trees from the supplied Envelopes.
     * Performed on a whole batch of Envelopes for insert performance.
     * May throw exceptions if they arise.
     */
    fun assert(batch: EnvelopeBatch, skipCurrent: Boolean = false) {

        // Resolve all identifiers and primary keys for the envelopes.
        val resolved = resolve(batch)

        val relationshipStatements = resolved.envelopes.flatMap { envelope ->

            check(envelope.pk != null) { "All Envelope PKs must be resolved" }

            envelope.itemTrees.flatMap { itemTree ->

                itemTreeStatementMapper.toRelationshipStatementsByStrategy(
                    envelope,
                    itemTree
                ).map { Pair(it, envelope.pk) }
            }
        }

        relationshipDao.assertRelationships(
            relationshipStatements,
            relationshipTypeRegistry,
            skipCurrent
        )

        val propertyStatements = resolved.envelopes.flatMap { envelope ->
            check(envelope.pk != null) { "Envelope PK must be resolved" }
            envelope.itemTrees.flatMap { itemTree ->
                itemTreeStatementMapper.toPropertyStatementsByStrategy(
                    envelope,
                    itemTree
                ).map { Pair(it, envelope.pk) }
            }
        }

        propertyDao.assertPropertyStatements(
            propertyStatements,
            skipCurrent
        )
    }

    /**
     * Parse all identifiers in the [Envelope] into tokenized form, which enables resolution.
     * Includes Items in Envelope and in the contents.
     */
    private fun tokenize(envelope: Envelope): Envelope =
        envelope
            .withAssertion(envelope.assertion.withAssertingParty(identifierTypeRegistry.tokenizeIdentifiersIn(envelope.assertion.assertingParty)))
            .withItemTrees(envelope.itemTrees.map { itemTree ->
                val withStructuredIdentifiers = identifierTypeRegistry.tokenizeIdentifiersIn(itemTree)
                withStructuredIdentifiers
            })

    /**
     * Parse all identifiers in the [EnvelopeBatch] into Tokenized form, which enables resolution.
     * Includes Items in Envelope and in the contents.
     */
    private fun tokenize(envelope: EnvelopeBatch): EnvelopeBatch =
        envelope.withEnvelopes(envelope.envelopes.map(this::tokenize))

    /**
     * Get all [RelationshipStatement]s for the @param[subject], @param[relationship], @param[obj] triple, asserted by @param[assertedBy].
     * By default, returns only current relationships. If @param[current] is false, retrieves only non-current relationships.
     */
    fun getRelationshipStatements(
        subject: Identifier,
        relationship: String,
        obj: Identifier,
        assertedBy: Identifier,
        state: Boolean = true,
        current: Boolean = true
    ): Collection<RelationshipStatement> {

        // Read-only because this is a query. If they don't exist, don't create them.
        val resolvedSubj = resolver.resolveRO(subject)
        val resolvedObj = resolver.resolveRO(obj)

        val resolvedAssertedBy = resolver.resolveRO(assertedBy)
        val resolvedRelationship = relationshipDao.resolveRelationshipTypeRO(relationship)

        if (resolvedSubj.pk == null || resolvedObj.pk == null || resolvedRelationship == null || resolvedAssertedBy.pk == null) {
            return emptyList()
        }

        return relationshipDao.getAllStatements(
            resolvedAssertedBy.pk,
            resolvedSubj.pk,
            resolvedRelationship,
            resolvedObj.pk,
            state,
            current
        )
    }

    /**
     * Get all [PropertyStatement]s for the @param[subject], asserted by @param[assertedBy].
     * By default, returns only current properties. If @param[current] is false, retrieves only non-current
     * relationships.
     *
     * The semantics of the Item Graph dictates that only one PropertyStatement can be current at one time, so when
     * @param[current] is true, this will return either an empty collection, or a collection with one item.
     *
     * There can be any number of historical past non-current PropertyStatements, so when @param[current] is false, a
     * larger collection may be returned.
     */
    fun getPropertyStatements(
        subject: Identifier,
        assertedBy: Identifier,
        current: Boolean = true
    ): Collection<PropertyStatement> {
        val resolvedSubj = resolver.resolveRO(subject)
        val resolvedAssertedBy = resolver.resolveRO(assertedBy)

        if (resolvedSubj.pk == null || resolvedAssertedBy.pk == null) {
            return emptyList()
        }

        return propertyDao.getAllStatements(resolvedSubj.pk, resolvedAssertedBy.pk, current)
    }
}