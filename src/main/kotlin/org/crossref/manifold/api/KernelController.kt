package org.crossref.manifold.api


import org.crossref.manifold.itemgraph.PropertyDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.meta.ManifoldStats
import org.crossref.manifold.meta.getStats
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.retrieval.IdentifierRetriever
import org.crossref.manifold.retrieval.itemtree.ItemTreeRetriever
import org.crossref.manifold.retrieval.statements.PropertyStatementFilterBuilder
import org.crossref.manifold.retrieval.statements.PropertyStatementRetriever
import org.crossref.manifold.retrieval.statements.RelationshipStatementFilterBuilder
import org.crossref.manifold.retrieval.statements.RelationshipStatementRetriever
import org.crossref.manifold.views.PropertyStatementWithProvenance
import org.crossref.manifold.views.RelationshipStatementWithProvenance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import javax.servlet.http.HttpServletResponse
import org.crossref.manifold.itemgraph.ItemDao

/**
 * REST APIs for Manifold kernel-level data.
 */
@RestController
class KernelController(
    @Autowired
    val sqlResolver: RelationshipDao,

    @Autowired
    val relationshipStatementFilterBuilder: RelationshipStatementFilterBuilder,

    @Autowired
    val propertyStatementFilterBuilder: PropertyStatementFilterBuilder,

    @Autowired
    val itemTreeRetriever: ItemTreeRetriever,

    @Autowired
    val relationshipStatementRetriever: RelationshipStatementRetriever,

    @Autowired
    val propertyStatementRetriever: PropertyStatementRetriever,

    @Autowired
    val relationshipDao: RelationshipDao,

    @Autowired
    val propertyDao: PropertyDao,

    @Autowired
    val identifierTypeRegistry: IdentifierTypeRegistry,

    @Autowired
    val itemDao: ItemDao

) {

    /**
     * Statistics for the whole Item Graph.
     */
    @GetMapping("/v2/kernel/stats")
    fun getStats(): ResponseEnvelope<ManifoldStats> {
        val stats = sqlResolver.getStats()

        return ResponseEnvelope<ManifoldStats>("ok", "manifold-stats", message = stats)
    }

    /**
     * Get the list of known Relationship types.
     */
    @GetMapping("/v2/kernel/relationship-types", produces = ["application/json"])
    fun getRelationshipTypes(): ResponseEnvelope<RelationshipTypesView> {
        val types = sqlResolver.getRelationshipTypes()

        return ResponseEnvelope("ok", "relationship-types-list", message = types)
    }

    @GetMapping("/v2/relationship-statements-with-provenance", produces = ["application/json"])
    fun relationshipStatements(
        @RequestParam(name = "cursor", required = false, defaultValue = "0") cursor: Long,
        @RequestParam(name = "rows", required = false, defaultValue = "20") rows: Int,
        @RequestParam(name = "subject", required = false) subject: String?,
        @RequestParam(name = "object", required = false) obj: String?,
        @RequestParam(name = "relationshipType", required = false) relationshipType: String?,
        @RequestParam(name = "assertedBy", required = false) assertedBy: String?,
    ): ResponseEnvelope<RelationshipStatementsWithProvenanceView> {
        val filter = relationshipStatementFilterBuilder.build(subject, obj, relationshipType, assertedBy)
        val (statements, nextCursor) = relationshipStatementRetriever.getPage(cursor, rows, filter)

        // This IdentifierRetriever has a lifetime only of this API request.
        val identifierRetriever = IdentifierRetriever(itemDao, relationshipDao, identifierTypeRegistry)

        // Load up a cache of all known Identifiers for entities mentioned in this page.
        identifierRetriever.prime(statements.flatMap {
            listOf<Long>(
                it.first.fromItemPk,
                it.first.toItemPk,
                it.first.assertingPartyPk
            )
        }.toSet())

        // Now map into the right view, incorporating the statement and its provenance.
        val statementViews = statements.map {
            RelationshipStatementWithProvenance.from(
                it.first,
                it.second,
                identifierRetriever,
                identifierTypeRegistry
            )
        }
        val message = RelationshipStatementsWithProvenanceView(statementViews, nextCursor)

        return ResponseEnvelope("ok", "relationship-statements-with-provenance-list", message = message)
    }

    @GetMapping("/v2/property-statements-with-provenance", produces = ["application/json"])
    fun propertyStatements(
        @RequestParam(name = "cursor", required = false, defaultValue = "0") cursor: Long,
        @RequestParam(name = "rows", required = false, defaultValue = "20") rows: Int,
        @RequestParam(name = "subject", required = false) subject: String?,
        @RequestParam(name = "assertedBy", required = false) assertedBy: String?,
    ): ResponseEnvelope<PropertyStatementsWithProvenanceView> {
        val filter = propertyStatementFilterBuilder.build(subject, assertedBy)
        val (statements, nextCursor) = propertyStatementRetriever.getPage(cursor, rows, filter)

        // This IdentifierRetriever has a lifetime only of this API request.
        val identifierRetriever = IdentifierRetriever(itemDao, relationshipDao, identifierTypeRegistry)

        // Load up a cache of all known Identifiers for entities mentioned in this page.
        identifierRetriever.prime(statements.flatMap { listOf<Long>(it.first.itemPk, it.first.assertingPartyPk) }
            .toSet())

        // Now map into the right view, incorporating the statement and its provenance.
        val statementViews = statements.map {
            PropertyStatementWithProvenance.from(
                it.first,
                it.second,
                identifierRetriever,
                identifierTypeRegistry
            )
        }
        val message = PropertyStatementsWithProvenanceView(statementViews, nextCursor)

        return ResponseEnvelope("ok", "property-statements-with-provenance-list", message = message)
    }
}
