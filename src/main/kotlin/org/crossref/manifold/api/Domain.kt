/**
 * Data structures for the API.
 */
package org.crossref.manifold.api

import com.fasterxml.jackson.annotation.JsonProperty
import org.crossref.manifold.views.PropertyStatementWithProvenance
import org.crossref.manifold.views.RelationshipStatementWithProvenance

/**
 * Container for all API responses.
 */
data class ResponseEnvelope<K>(
    @JsonProperty("status")
    val status: String,

    @JsonProperty("message-type")
    val messageType: String,

    @JsonProperty("message-version")
    val messageVersion: String = "1.0.0",

    @JsonProperty("message")
    val message: K
)


/**
 * Statistics around an Item.
 */
data class ItemStatsView(

    @JsonProperty("item-pk")
    var pk: Long,

    @JsonProperty("total-item-identifiers")
    var numItemIdentifiers: Long,

    @JsonProperty("total-incoming-relationships")
    var numIncomingRelationships: Long,

    @JsonProperty("total-outgoing-relationships")
    var numOutgoingRelationships: Long,
)

/**
 * List of Relationship types.
 */
data class RelationshipTypesView(
    @JsonProperty("items")
    val values: List<String>
)

/**
 * List of relationships.
 */
data class RelationshipListView(
    @JsonProperty("relationships")
    val values: List<RelationshipView>
)

/**
 * View of a Relationship, including an inline view of each Item.
 */
data class RelationshipView(
    @JsonProperty("from-item")
    val fromItem: ItemView,

    @JsonProperty("to-item")
    val toItem: ItemView,

    @JsonProperty("asserting-item")
    val assertingItem: ItemView,

    @JsonProperty("relationship-type")
    val relationshipTypeName: String,

    @JsonProperty("asserted-at")
    val assertedAt: String
)

data class ItemView(
    @JsonProperty("identifiers")
    val identifiers: List<ItemIdentifierView>,
)

data class ItemIdentifierView(
    @JsonProperty("identifier-type")
    val identifierType: String,

    @JsonProperty("identifier")
    val identifier: String
)
data class RelationshipStatementsWithProvenanceView(
    @JsonProperty("relationship-statements-with-provenance")
    val statements: List<RelationshipStatementWithProvenance>,

    @JsonProperty("next-cursor")
    val nextCursor: Long?
)


data class PropertyStatementsWithProvenanceView(
    @JsonProperty("property-statements-with-provenance")
    val statements: List<PropertyStatementWithProvenance>,

    @JsonProperty("next-cursor")
    val nextCursor: Long?
)
