package org.crossref.manifold.bulk

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.Closeable
import java.io.File
import java.io.Flushable

/**
 * Writer for an EnvelopeBatch Archive, which is a GZipped Tar file containing a sequence of Envelope Batch files.
 * For ingestion in bulk.
 * This must be used in a managed context such as Kotlin's `use`, and is not thread safe.
 */
class EnvelopeBatchArchiveWriter(file: File) : Closeable, Flushable {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    val objectMapper: JsonMapper = JsonMapper.builder()
        .addModule(JavaTimeModule())
        .build()

    private val writer = TarGzWriter(file)

    /**
     * Add a file containing @param[envelopeBatches].
     * The filename does not need to be unique to be written, but it's useful for tracability.
     */
    fun add(recordFilename: String, envelopeBatches: Collection<EnvelopeBatch>) {
        writer.add(recordFilename) {
            objectMapper.writeValue(it, envelopeBatches)
        }
    }

    override fun close() {
        writer.close()
    }

    override fun flush() {
        writer.flush()
    }
}