package org.crossref.manifold.bulk

import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.*

class TarGzWriter(file: File) : Closeable, Flushable {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val outStream = FileOutputStream(file)

    private val tarStream = TarArchiveOutputStream(
        GzipCompressorOutputStream(BufferedOutputStream(outStream))
    )

    private var count = 0
    private var startTime = System.currentTimeMillis()

    /**
     * Close and flush the file.
     */
    override fun close() {
        tarStream.close()
        outStream.close()
    }

    fun add(recordFilename: String, f: ((stream: OutputStream) -> Unit)) {
        val tarEntry = TarArchiveEntry(recordFilename)
        val writer = ByteArrayOutputStream()
        f(writer)
        writer.flush()
        tarEntry.size = writer.size().toLong()
        tarStream.putArchiveEntry(tarEntry)
        tarStream.write(writer.toByteArray())
        tarStream.closeArchiveEntry()
        logger.debug("Wrote $recordFilename")
        count++
        if (count % 10000 == 0) {
            val duration = (System.currentTimeMillis() - startTime) / 1000
            val averageRate = (count.toFloat() / duration.toFloat()).toInt()
            logger.info("Wrote $count files to Tar Archive $recordFilename in $duration seconds, $averageRate per second")
        }
    }


    /**
     * Flush the output stream.
     */
    override fun flush() {
        outStream.flush()
    }
}
