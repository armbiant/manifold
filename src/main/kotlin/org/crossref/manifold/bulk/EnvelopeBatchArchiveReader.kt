package org.crossref.manifold.bulk

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.scanTarGz
import java.io.File

/**
 * Reader for an EnvelopeBatch Archive, as created with [EnvelopeBatchArchiveWriter].
 */
suspend fun readEnvelopeBatchArchive(file: File, f: suspend (batches: List<EnvelopeBatch>) -> Unit) {
    /** Mapper that can read JSON files from the Tar file.
     * AUTO_CLOSE_SOURCE is important for tar streams as they are files of files, therefore have specific file-handling
     * semantics. Normal parsing would close the stream prematurely.
     */
    val mapper = JsonMapper.builder()
        .addModule(JavaTimeModule())
        .configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false)
        .build().registerKotlinModule()

    scanTarGz(file) { _, reader ->
        val myObjects: List<EnvelopeBatch?>? =
            mapper.readValue(reader, object : TypeReference<List<EnvelopeBatch?>?>() {})

        val batches = myObjects?.mapNotNull { it }
        if (batches != null) {
            f(batches)
        }
    }
}
