package org.crossref.manifold.retrieval.statements

import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

data class RelationshipStatementFilter(
    val subjectPk: Long? = null,
    val objectPk: Long? = null,
    val relationshipTypePk: Long? = null,
    val partyPk: Long? = null
)

/**
 * Map query parameters into a filter, looking up the various items in the process.
 */
@Service
class RelationshipStatementFilterBuilder(
    @Autowired
    val identifierTypeRegistry: IdentifierTypeRegistry,

    @Autowired
    val relationshipTypeRegistry: RelationshipTypeRegistry,

    @Autowired
    val itemDao: ItemDao,

    @Autowired
    val relationshipDao: RelationshipDao
) {
    /**
     * Given a set of optional filters (which use ItemIdentifiers and RelationshipTypes, to be resolved) construct an AssertionFilter.
     */
    fun build(
        subjectIdentifier: String?,
        objectIdentifier: String?,
        relationshipType: String?,
        assertedBy: String?
    ): RelationshipStatementFilter {
        val fromPk = if (subjectIdentifier == null) {
            null
        } else {
            val response = itemDao.findItemAndIdentifierPk(
                identifierTypeRegistry.tokenize(
                    subjectIdentifier
                )

            )?.itemPk
            if (response == null) {
                throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "subject filter supplied but didn't recognise it"
                )
            }
            response
        }

        val toPk = if (objectIdentifier == null) {
            null
        } else {
            val response = itemDao.findItemAndIdentifierPk(

                identifierTypeRegistry.tokenize(objectIdentifier)

            )?.itemPk
            if (response == null) {
                throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "object filter supplied but didn't recognise it"
                )
            }
            response
        }

        val partyPk = if (assertedBy == null) {
            null
        } else {
            val response = itemDao.findItemAndIdentifierPk(

                identifierTypeRegistry.tokenize(
                    assertedBy
                )
            )
                ?.itemPk
            if (response == null) {
                throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "assertedBy filter supplied but didn't recognise it"
                )
            }
            response
        }

        val relationshiptypePk = if (relationshipType == null) {
            null
        } else {
            val response = relationshipTypeRegistry.resolve(relationshipType)
            if (response == null) {

                throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "relationshipType filter supplied but didn't recognise it"
                )
            }
            response
        }

        return RelationshipStatementFilter(
            subjectPk = fromPk,
            objectPk = toPk,
            relationshipTypePk = relationshiptypePk,
            partyPk = partyPk
        )
    }
}