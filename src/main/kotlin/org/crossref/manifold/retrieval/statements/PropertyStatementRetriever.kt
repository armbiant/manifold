package org.crossref.manifold.retrieval.statements

import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.PropertyDao
import org.crossref.manifold.itemgraph.PropertyStatement
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PropertyStatementRetriever(
    @Autowired
    val propertyDao: PropertyDao
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Fetch a page of Statement Items starting from the given Statement Pk.
     * Return an optional starting Statement Pk if there are more items.
     */
    fun getPage(
        startPk: Long,
        count: Int,
        filter: PropertyStatementFilter,
    ): Pair<List<Pair<PropertyStatement, EnvelopeBatchProvenance>>, Long?> {

        // Fetch one extra as a cursor for the next page, but don't show it.
        val fetchCount = count + 1

        val result = propertyDao.getPropertyStatementRange(startPk, fetchCount, filter)

        // If we returned less than the desired amount plus one, that means it's the last page.
        return if (result.count() < fetchCount) {
            Pair(result, null)
        } else {
            // If we returned more than the desired amount, keep only the desired amount and use the extra
            // as the cursor for the next page.
            Pair(result.subList(0, count), result.last().first.pk)
        }
    }
}
