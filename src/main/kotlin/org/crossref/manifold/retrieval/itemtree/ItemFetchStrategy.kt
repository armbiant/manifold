package org.crossref.manifold.retrieval.itemtree

import org.crossref.manifold.modules.consts.RelationshipTypes


/**
 * A set of strategies and authorities for retrieving an Item Tree.
 */
data class ItemFetchConfiguration(
    /**
     * Fetch strategy to use with respect to the root node of the tree.
     */
    val rootStrategies: Collection<ItemFetchStrategy.Strategy> = emptySet(),

    /**
     * Fetch strategy to use with respect to each node of the tree recursively.
     */
    val eachStrategies: Collection<ItemFetchStrategy.Strategy> = emptySet(),

    /**
     * Set of Item PKs for parties we consult when applying the root and each-node strategy.
     */
    val authorityRoots: Set<Long> = emptySet(),

    /**
     * Set of parties we always treat as authorities.
     */
    val authorities: Set<Long> = emptySet(),

    /**
     * Just retrieve all assertions, regardless of authority.
     */
    val all: Boolean = false,

    /**
     * Maximum tree depth to recurse.
     */
    val maxDepth: Int
)

/**
 * Item Fetch Strategy is a piece of syntax that describes how an Item Tree should be fetched. It covers the depth
 * of the item tree, whose assertions we want to include.
 * In the future, it could accommodate more specialised strategies.
 */
object ItemFetchStrategy {
    const val DEFAULT = "default"
    private const val ALL = "all"
    private const val REGISTRATION_AGENCY = "registration-agency"
    private const val STEWARD = "steward"
    private const val REGISTRATION_AGENCY_ROOT = "registration-agency-root"
    private const val STEWARD_ROOT = "steward-root"
    private const val PARTNER = "partner"

    sealed class Strategy

    /**
     * This strategy indicates that assertions should be retrieved when they are asserted by a party that is the object
     * of the given relationship type with respect to any given node of the tree.
     *
     * Contrast with RootObjRelationship, which only looks for connections to the root node, this looks at each node in
     * turn.
     *
     * Useful to ask e.g. "how does the maintainer describe their own items, and for related authors, how do they
     * describe themselves?"
     */
    data class ObjRelationship(val relationshipType: String) : Strategy()

    /**
     * A perspective is an applied Item Fetch Strategy.
     */
    fun fromPerspective(
        perspective: String,
        authorityRoots: Set<Long>,
        authorities: Set<Long>,
        maxDepth: Int
    ): ItemFetchConfiguration =
        when (perspective) {
            REGISTRATION_AGENCY -> ItemFetchConfiguration(
                eachStrategies = setOf(ObjRelationship(RelationshipTypes.RA_REL_TYPE)),
                authorityRoots = authorityRoots,
                maxDepth = maxDepth
            )
            REGISTRATION_AGENCY_ROOT -> ItemFetchConfiguration(
                rootStrategies = setOf(ObjRelationship(RelationshipTypes.RA_REL_TYPE)),
                authorityRoots = authorityRoots,
                maxDepth = maxDepth
            )
            STEWARD_ROOT -> ItemFetchConfiguration(
                rootStrategies = setOf(ObjRelationship(RelationshipTypes.STEWARD)),
                authorityRoots = authorityRoots,
                maxDepth = maxDepth
            )
            STEWARD -> ItemFetchConfiguration(
                eachStrategies = setOf(ObjRelationship(RelationshipTypes.STEWARD)),
                authorityRoots = authorityRoots,
                maxDepth = maxDepth
            )
            PARTNER -> ItemFetchConfiguration(
                authorities = authorities,
                maxDepth = maxDepth
            )
            ALL -> ItemFetchConfiguration(
                all = true,
                maxDepth = maxDepth
            )
            DEFAULT -> ItemFetchConfiguration(
                eachStrategies = setOf(
                    ObjRelationship(RelationshipTypes.RA_REL_TYPE),
                    ObjRelationship(RelationshipTypes.STEWARD)
                ),
                authorityRoots = authorityRoots,
                authorities = authorities,
                maxDepth = maxDepth
            )

            else -> throw Exception("Unrecognised perspective: $perspective")
        }
}


