package org.crossref.manifold.retrieval.itemtree

import org.crossref.manifold.itemgraph.*
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.consts.IdentifierTypes
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.retrieval.IdentifierRetriever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


/**
 * The Item Tree retriever is the counterpart to the Item Graph's Item Tree ingester. Given an Identifier (such as a
 * DOI) retrieve the tree of items that hang off it.
 *
 * Because there's no conceptual limitation to which relationship assertions can be made, insertion is relatively
 * simple. But for retrieval, we must be selective about which items retrieve because there could be anything in the
 * Item Graph. There are therefore various strategies that can be used for retrieval.
 *
 */
@Service
class ItemTreeRetriever(
    @Autowired
    val identifierTypeRegistry: IdentifierTypeRegistry,

    @Autowired
    val relationshipTypeRegistry: RelationshipTypeRegistry,

    @Autowired
    val relationshipDao: RelationshipDao,

    @Autowired
    val itemDao: ItemDao,

    @Autowired
    val propertyDao: PropertyDao,
) {
    /**
     * Apply a set of strategies to the given node, returning the authorities according to the authority roots.
     * @return set of Item PKs of authorities for this node.
     */
    fun getAuthorities(
        itemPk: Long,
        strategies: Collection<ItemFetchStrategy.Strategy>,
        authorityRoots: Set<Long>
    ): Set<Long> {
        val relationshipTypes : Set<String> = strategies.map {
            when (it) {
                is ItemFetchStrategy.ObjRelationship -> it.relationshipType
            }
        }.toSet()
        val relationshipTypesPks = relationshipTypeRegistry.resolveMany(relationshipTypes)

        return relationshipDao.getCurrentSubjRelStatements(
            authorityRoots,
            setOf(itemPk),
            relationshipTypesPks,
        ).map { it.toItemPk }.toSet()
    }

    /**
     * Retrieve an Item Tree starting at the given root node.
     * This recursively fetches a tree, consulting appropriate authorities according to @param[config].
     */
    fun get(rootItemPk: Long, config: ItemFetchConfiguration): Item? {

        // This cache is mutable, and builds up a set of mappings of identifier to item.
        // Though it is used in the recursive case, it is not immutable, so different branches of the recursion can take advantage of it.
        val itemPkIdentifierCache = IdentifierRetriever(
            itemDao,
            relationshipDao,
            identifierTypeRegistry
        )

        // First we need to retrieve the set of authority parties with respect to the root.
        val rootStrategyAuthorities = getAuthorities(rootItemPk, config.rootStrategies, config.authorityRoots)

        /**
         * @param[itemPk] the Item PK at this point of recursion
         * @param[authorities] the set of parties we treat as authorities, at this point of recursion. This may change throughout recursion, and is immutable.
         */
        fun recurse(itemPk: Long, authorities: Set<Long>, depth: Int = 0): Item {

            // Set of parties that match the criteria for authority with respect to this node in the recursion.
            val strategyAuthorities = getAuthorities(itemPk, config.eachStrategies, config.authorityRoots)

            // The sum of all trusted parties at this point of recursion through the tree
            // This includes prior recursive cases, root strategy, and initial trusted parties.
            val recursiveAuthorities = authorities + strategyAuthorities

            // Find any relationships that meet the criteria.
            // Either those by specified parties, or all. 
            val relatedItems = if (config.all) {
                relationshipDao.getCurrentSubjRelationshipStatements(setOf(itemPk))
            } else {
                relationshipDao.getCurrentSubjRelationshipStatements(recursiveAuthorities, setOf(itemPk))
            }

            // Find all properties that meet the criteria.
            val propertyStatements = if (config.all) {
                propertyDao.getCurrentSubjPropertyStatements(setOf(itemPk))
            } else {
                propertyDao.getCurrentSubjPropertyStatements(recursiveAuthorities, setOf(itemPk))
            }

            // Retrieve large match of ItemIdentifiers in one go, for the item, its subject and object, and asserting
            // parties.
            itemPkIdentifierCache.prime(
                (relatedItems.flatMap { listOf(it.fromItemPk, it.toItemPk, it.assertingPartyPk) } +
                        propertyStatements.map { it.assertingPartyPk } +
                        itemPk).toSet())

            // We can't proceed if we didn't recognise it.
            val itemIdentifier =
                itemPkIdentifierCache.get(itemPk) ?: throw Exception("Couldn't retrieve Item ID $itemPk")

            // The assertions made by various parties. Group per-relationship, so we can state which parties asserted
            // them.
            // Don't need to include subject in the groupBy operation, because this whole function call is about the
            // subject.
            val byRelationship = relatedItems.groupBy { Pair(it.relationshipType, it.toItemPk) }

            val relationships = byRelationship.map { (relTypItemPk, assertions) ->
                // Kotlin doesn't support nested destructuring.
                val (relationshipType, relItemPk) = relTypItemPk

                // All assertions here will differ only by asserting party. Gather them up.
                val assertedBy = relationshipStatementsToAssertingParties(assertions, itemPkIdentifierCache)
                Relationship(
                    relTyp = relationshipType,
                    obj = recurse(relItemPk, recursiveAuthorities, depth + 1),
                    assertedBy = assertedBy
                )
            }

            // Don't group the properties. As each Property object is a whole set of properties, it's less meaningful to
            // find the overlapping asserted fields.
            val properties = propertyStatements.map {
                Properties(
                    values = it.values,
                    assertedBy = propertyStatementsToAssertingParties(listOf(it), itemPkIdentifierCache)
                )
            }

            // Don't recurse past the stipulated depth.
            return if (depth > config.maxDepth) {
                Item(rels = emptyList(), identifiers = itemIdentifier, pk = itemPk)
            } else {
                Item(rels = relationships, identifiers = itemIdentifier, pk = itemPk, properties = properties)
            }
        }

        // Start recursion with the supplied authorities, plus the authorities derived from the root.
        return recurse(rootItemPk, config.authorities + rootStrategyAuthorities)
    }

    /**
     * Construct an Item Identifier URI, i.e. http://id.crosref.org/item/55 as a fully resolved ItemIdentifier object.
     */
    fun itemPkIdentifier(itemPk: Long): Identifier {
        val itemPkIdentifierUrl = IdentifierTypes.ITEM_URI + itemPk
        return Identifier(
            itemPkIdentifierUrl,
            identifierTypeRegistry.tokenize(itemPkIdentifierUrl)
        )
    }

    /** For a set of RelationshipStatements, return a collection of Items that represent the asserting parties as Items,
     * complete with Identifiers.
     */
    fun relationshipStatementsToAssertingParties(
        relationshipStatements: List<RelationshipStatement>,
        itemPkIdentifierCache: IdentifierRetriever
    ) =
        relationshipStatements.map {

            val itemIdentifiers = itemPkIdentifierCache.get(it.assertingPartyPk) ?: emptyList()

            val itemPkIdentifier = itemPkIdentifier(it.assertingPartyPk)

            Item(
                identifiers = itemIdentifiers + itemPkIdentifier,
                pk = it.assertingPartyPk
            )
        }

    fun propertyStatementsToAssertingParties(
        propertyStatements: List<PropertyStatement>,
        itemPkIdentifierCache: IdentifierRetriever
    ) =
        propertyStatements.map {

            val itemIdentifiers = itemPkIdentifierCache.get(it.assertingPartyPk) ?: emptyList()

            val itemPkIdentifier = itemPkIdentifier(it.assertingPartyPk)

            Item(
                identifiers = itemIdentifiers + itemPkIdentifier,
                pk = it.assertingPartyPk
            )
        }
}
