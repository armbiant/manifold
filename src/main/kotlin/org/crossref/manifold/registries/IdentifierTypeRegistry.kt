package org.crossref.manifold.registries

import org.apache.jena.shared.PrefixMapping
import org.crossref.manifold.ConflictException
import org.crossref.manifold.itemgraph.IdentifierTypeDao
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Tokenized
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct


/** A prefix to indicate that the URL isn't recognised as a [Persistent Identifier](https://en.wikipedia.org/wiki/Persistent_identifier)
 * with a known structure. E.g. a URL on a random website.
 */
const val NON_PERSISTENT = "non-persistent"

/**
 * Max number of characters allowed in a prefix.
 */
const val MAX_PREFIX_CHARS = 16

/**
 * The default resource filename for where the corresponding schema should be found.
 */
const val SCHEMA_RESOURCE_FILENAME = "schema/identifiers.schema.json"

/**
 * Registry of recognised identifier types, categorised as various types of persistent identifier, plus a category of
 * non-persistent identifiers. Keeps track of the types of identifiers in use by modules.
 *
 * Parses Identifier URI strings to (prefix label, suffix) pairs prior to database, and combines them back.
 * This is done by using RDF's 'prefix' concept - representing the prefix of a URI with a prefix label.
 *
 * Doing this brings several advantages:
 * 1. Allows modules to document which identifiers they use, and record those in a central registry.
 *    This is useful because one of the jobs of a module is to map from external vocabularies to internal ones.
 *    Doing it this way demonstrates the mapping explicitly, which is useful for specification and maintainability.
 *
 * 2. Compresses common strings. This is useful for data compression, and avoids duplicating e.g. 'https://doi.org' repeatedly
 *    in the database. Breaking this out also helps with database indexing and e.g. partitioning.
 *
 * 3. Forms a natural taxonomy of identifier types in use. Allowing us to e.g. find identifiers of a given type.
 *
 * 4. Allows us to identify which Item Identifiers conform to our designed 'structured identifier' types (e.g. PIDs),
 *    and which ones are just free-form URIs. We may want to attach different semantics to different types of identifiers.
 *
 * 4. Allows mapping from several expressions of the same identifier into, and out of, a common model.
 *    e.g. DOIs can be expressed as http or https, dx.doi.org or doi.org .
 *
 * The registration methods aren't threadsafe, but they are only called from the manifest constructors.
 * Don't add mappings on the fly!
 */
@Service
class IdentifierTypeRegistry(
    @Autowired
    val identifierTypeDao: IdentifierTypeDao,
    val schemaFileName: String = SCHEMA_RESOURCE_FILENAME
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    // The Canonical model has the default mapping.
    // It is unambiguous, meaning that every URI prefix is unique and every namespace label is unique.
    // It's worthwhile using the PrefixMapping class from Jena because this gives us a standard representation of
    // canonical mappings, which can be used in other contexts, such as RDF output.
    private val canonicalPrefixes: PrefixMapping = PrefixMapping.Factory.create()

    private val prefixToPk = mutableMapOf<String, Int>()
    private val pkToPrefix = mutableMapOf<Int, String>()

    // We also allow a set of prefixes, for input only, that map into duplicate namespace labels.
    // We don't use Jena for this because there may be conflicting mappings (i.e. more than one URI prefix maps to the
    // same label) which is (deliberately) unsupported.
    private val supplementaryInputPrefixes = mutableMapOf<String, String>()

    /**
     * Load the registry of identifier types from the Identifiers schema file.
     * This is called as part of the initialization of the registry, so that it's populated by the time it's used.
     * This will also detect any conflicts within the Identifiers schema file, forming a regression check for the file.
     */
    @PostConstruct
    fun load() {
        logger.info("Loading Identifier Types")

        val tree = loadJsonResource(schemaFileName)

        val options = tree.findPath("anyOf")
        if (options.isEmpty) {
            val error = "Identifiers schema: root item must be 'anyOf'"
            logger.error(error)
            throw Exception(error)
        }

        options.asSequence().iterator().forEach {
            val pattern = it.get("pattern")?.asText()
            val prefix = it.get("prefix")?.asText()
            val alias = it.get("alias")?.asText()
            val blank = it.get("blank")?.asBoolean()

            if (pattern == null || !pattern.startsWith("^")) {
                val error = "Identifiers schema: Pattern must be present and start with '^' regex metacharacter in: $it"
                logger.error(error)
                throw Exception(error)
            }

            // In the schema the URI is represented as a simple regex, with a '^' metacharacter as the first
            // char.
            val uri = pattern.substring(1)

            if (prefix != null && alias != null) {
                val error = "Identifiers schema: Either 'prefix' or 'alias' can be supplied, not both. In: $it"
                logger.error(error)
                throw Exception(error)
            }

            if (prefix == null && alias == null) {
                val error = "Identifiers schema: Either 'prefix' or 'alias' must be supplied. In: $it"
                logger.error(error)
                throw Exception(error)
            }

            if (blank == null) {
                val error = "Identifiers schema: 'blank' must be supplied. In: $it"
                logger.error(error)
                throw Exception(error)
            }

            // If 'prefix' is present then this is the canonical mapping for this URI.
            // There can be no conflicts.
            if (prefix != null) {

                // We only check for duplicates for canonical ones.
                // Aliases are duplicates by definition.
                if (canonicalPrefixes.getNsURIPrefix(uri) !== null) {
                    val error = "Identifiers schema: Duplicate URI pattern in: $it"
                    logger.error(error)
                    throw Exception(error)
                }

                if (canonicalPrefixes.getNsPrefixURI(prefix) !== null) {
                    val error = "Identifiers schema: Duplicate Prefix in: $it"
                    logger.error(error)
                    throw Exception(error)
                }

                registerCanonicalIdentifierType(prefix, uri, blank)
            }

            // If 'alias' is present then the URI is aliased to the same prefix.
            // Multiple URIs can be aliased to the same prefix.
            if (alias != null) {
                registerSupplementaryInputMapping(alias, uri)
            }
        }

        logger.info("Finished Loading Identifier Types")
    }

    /**
     * Register a canonical identifier URI and prefix label. Conflicting URIs or prefix labels aren't allowed.
     * @throws Exception on conflict.
     */
    fun registerCanonicalIdentifierType(prefixLabel: String, uri: String, blank: Boolean) {
        check(prefixLabel.length <= MAX_PREFIX_CHARS) { "Prefix can be no greater than $MAX_PREFIX_CHARS characters" }

        // Either the URI or the prefix label might have been previously registered.
        // We need to detect if this new pair introduces a conflict or not.
        // Jena's PrefixMapping allows for duplicates, which is too loose for this use case.
        val existingPrefixToUri = canonicalPrefixes.getNsPrefixURI(prefixLabel)
        val existingUriToPrefix = canonicalPrefixes.getNsURIPrefix(uri)

        if (existingPrefixToUri == null && existingUriToPrefix == null) {
            // If neither existed before, add it.
            logger.debug("Registering canonical prefix mapping: $prefixLabel = $uri")
            canonicalPrefixes.setNsPrefix(prefixLabel, uri)
        } else if (existingPrefixToUri != null && existingUriToPrefix != null) {
            if (existingPrefixToUri == uri && existingUriToPrefix == prefixLabel) {
                // If both the URI label and the prefix were found, and they were as supplied, that's a duplicate, so skip.
                logger.info("Skipping re-register canonical prefix mapping: $prefixLabel = $uri")

            } else {
                // If they were both found, but mapped to something else, then that's a problem.
                val message =
                    "Tried to re-register conflicting canonical prefix mapping: $prefixLabel = $uri. Prior: $canonicalPrefixes"
                logger.error(message)
                throw(ConflictException(message))
            }
        } else {
            // If only one of them is found, then there's a conflict.
            val message =
                "Tried to re-register conflicting canonical prefix mapping: $prefixLabel = $uri. Prior: $canonicalPrefixes"
            logger.error(message)
            throw(ConflictException(message))
        }

        val pk = identifierTypeDao.resolve(prefixLabel, blank)
        pkToPrefix[pk] = prefixLabel
        prefixToPk[prefixLabel] = pk
    }

    /**
     * Register a URI to prefix mapping for use only on ingestion. This is a separate method to [registerCanonicalIdentifierType] because callers should know that they're registering an input-only one.
     * @throws Exception on conflict.
     */
    fun registerSupplementaryInputMapping(prefixLabel: String, uri: String) {
        check(prefixLabel.length <= MAX_PREFIX_CHARS) { "Prefix can be no greater than $MAX_PREFIX_CHARS characters" }

        val existingUriToPrefix = supplementaryInputPrefixes[uri]

        // Can't put the same URI in twice mapped to different things.
        if (existingUriToPrefix != null && existingUriToPrefix != prefixLabel) {
            val message =
                "Tried to re-register conflicting URI prefix: $uri but it was already mapped to $existingUriToPrefix."
            logger.error(message)
            throw(ConflictException(message))
        }

        supplementaryInputPrefixes[uri] = prefixLabel
    }


    /** Split a short URI into prefix and suffix.
     * This expects to be called on a string that is a shortened URI, and it's private, so it's safe.
     * The calling method ensures it's not called with an un-shortened URI, so it can safely assume that the first colon is a namespace separator.
     */
    private fun splitShort(shortForm: String): Tokenized? {
        // Awkward, but find the first instance of a colon.
        val splitAt = shortForm.indexOf(":")

        // If there's no colon
        // This line is impossible to trigger in a test but an important safety valve.
        if (splitAt < 0) {
            return null
        }

        return Tokenized(shortForm.substring(0, splitAt), shortForm.substring(splitAt + 1))
    }

    /**
     * Convert the input identifier URI to a tokenized format, if the URI is recognised as being a Persistent
     * Identifier. If it isn't, return null.
     */
    fun tokenizePersistentIdentifier(input: String): Tokenized? {
        val canonicalResult = canonicalPrefixes.shortForm(input)

        // If this did resolve to something different, then return that.
        if (canonicalResult != null && canonicalResult != input) {
            return splitShort(canonicalResult)
        }

        // If not, try it on the supplementary input mapping.
        for ((uri, prefix) in supplementaryInputPrefixes) {
            if (input.startsWith(uri)) {
                return Tokenized(prefix, input.substring(uri.length))
            }
        }

        // Otherwise, we didn't match it, so null.
        return null
    }

    /**
     * Convert a URI to a structured representation. If there isn't one, return a pair that indicates that it isn't structured so that it can be stored alongside structured ones.
     */
    fun tokenize(input: String): Tokenized {
        return tokenizePersistentIdentifier(input) ?: Tokenized(NON_PERSISTENT, input)
    }

    fun tokenize(itemIdentifier: Identifier): Identifier =

        if (itemIdentifier.tokenized == null) {
            itemIdentifier.withTokenized(tokenize(itemIdentifier.uri))
        } else {
            itemIdentifier
        }

    /** If the given pair has a recognised prefix, expand it.
     * Otherwise, don't and throw exception. This is important because the default behaviour is simply to prepend the prefix literally which wouldn't result in a recognisable identifier.
     */
    fun tokenizedToUri(tokenized: Tokenized): String =
    // If this was not-persistent, there's no prefix to add.
        // Check first because it's cheap and non-persistent ItemIdentifies are very common.
        if (tokenized.prefix == NON_PERSISTENT) {
            tokenized.suffix
            // If it is actually of a potentially recognised type, look it up and reconstruct.
        } else if (canonicalPrefixes.getNsPrefixURI(tokenized.prefix) != null) {
            canonicalPrefixes.expandPrefix(tokenized.prefix + ":" + tokenized.suffix)
        } else {
            throw Exception("Found a tokenized ItemIdentifier with unrecognised prefix: $tokenized")
        }

    /** Convert a [Tokenized] Identifier into a full Identifier with a URI.
     * PK is not resolved.
     */
    fun tokenizedToIdentifier(tokenized: Tokenized, itemPk: Long) = Identifier(
        uri = tokenizedToUri(tokenized),
        tokenized = tokenized,
        pk = itemPk
    )

    /**
     * Register a set of canonical Identifier mappings, as part of module registration.
     */
    fun registerCanonical(identifierMappings: Collection<Triple<String, String, Boolean>>) {
        identifierMappings.forEach { registerCanonicalIdentifierType(it.first, it.second, it.third) }
    }

    /**
     * Register a set of supplementary Identifier mappings, as part of module registration.
     */
    fun registerSupplementary(identifierMappings: Collection<Pair<String, String>>) {
        identifierMappings.forEach { registerSupplementaryInputMapping(it.first, it.second) }
    }

    /**
     * Recursively tokenize all ItemIdentifiers to structured ones iff they are not already tokenized.
     * @returns item tree with all identifiers converted to tokenized versions.
     */
    fun tokenizeIdentifiersIn(inputItem: Item): Item {
        val itemIdentifiers =
            inputItem.identifiers.map { itemIdentifier ->
                if (itemIdentifier.tokenized == null) {
                    itemIdentifier.withTokenized(tokenize(itemIdentifier.uri))
                } else {
                    itemIdentifier
                }
            }

        return inputItem.withIdentifiers(itemIdentifiers)
            .withRelationships(inputItem.rels.map { it.withItem(tokenizeIdentifiersIn(it.obj)) })
    }

    /** Resolve a prefix to its PK in the database.
      */
    fun resolvePrefix(value: String): Int =
        prefixToPk[value] ?: throw Exception("Unrecognised Identifier Prefix value: $value")

    /** Look up a prefix from its PK.
     */
    fun reversePrefix(pk: Int): String {
        val result = pkToPrefix[pk]

        if (result == null) {
            throw Exception("Unrecognised Identifier Prefix PK $pk")
        } else {
            return result
        }
    }
}