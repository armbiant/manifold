package org.crossref.manifold.registries

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.FileNotFoundException

/** Load JSON Schema File from resource filename or error.
 */
fun loadJsonResource(schemaFileName: String) : JsonNode {
    val mapper = ObjectMapper()
    val inputStream = Thread.currentThread().contextClassLoader
        .getResourceAsStream(schemaFileName)
    if (inputStream == null) {
        throw FileNotFoundException("Schema: file ${schemaFileName} missing")
    }
    return mapper.readTree(inputStream)
}