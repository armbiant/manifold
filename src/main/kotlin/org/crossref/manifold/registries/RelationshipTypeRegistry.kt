package org.crossref.manifold.registries

import org.crossref.manifold.itemgraph.RelationshipDao
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

/**
 * The default resource filename for where the corresponding schema should be found.
 */
const val RELATIONSHIPS_RESOURCE_FILENAME = "schema/relationships.schema.json"


/**
 * Store of all known relationship types. Modules register these on startup.
 * This is used in the resolution of Item Trees, as well as vocabulary mapping.
 * If the relationship isn't mentioned here, it isn't getting into the Item Graph!
 * A module should register all of its relation types, because if Manifold doesn't know about them, it will ignore them.
 * However, a given module isn't constrained by the relationship types that it declares, and could in theory use a
 * relationship type that another one uses.
 */
@Service
class RelationshipTypeRegistry(
    @Autowired
    val resolver: RelationshipDao,

    val schemaFileName: String = RELATIONSHIPS_RESOURCE_FILENAME
) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    val resolved = hashMapOf<String, Long>()
    val reverse = hashMapOf<Long, String>()

    /**
     * Load the registry of relationship types from the Relationship Types schema file.
     * This is called as part of the initialization of the registry, so that it's populated by the time it's used.
     * This will also detect any conflicts within the schema file, forming a regression check for the file.
     */
    @PostConstruct
    fun load() {
        logger.info("Loading Relationship Types")

        val tree = loadJsonResource(schemaFileName)

        // This schema file can be strict enough to enforce the 'exactly one of' semantics, unlike
        // [IdentifierTypeRegistry].
        val options = tree.findPath("oneOf")
        if (options.isEmpty) {
            val error = "Relationships schema: root item must be 'oneOf'"
            logger.error(error)
            throw Exception(error)
        }

        options.asSequence().iterator().forEach {
            val value = it.get("const")?.asText()

            if (value == null) {
                val error = "Relationships schema: Missing 'const' key in: $it"
                logger.error(error)
                throw Exception(error)
            }

            if (resolved.containsKey(value)) {
                val error = "Relationships schema: Duplicate value in: $it"
                logger.error(error)
                throw Exception(error)
            }

            register(value)
        }

        logger.info("Finished Loading Relationship Types")
    }

    /**
     * Register the given relationship types as strings.
     */
    private fun register(type: String) {
        val pk = resolver.resolveRelationshipType(type)
        logger.debug("Add relationship $type, now relationship pk $pk")

        this.resolved[type] = pk
        this.reverse[pk] = type
    }

    /**
     * Resolve the relationship type name.
     */
    fun resolve(name: String): Long? = resolved[name]

    /**
     * resolve a collection of relationship types to an unordered collection of PKs.
     * This is useful for retrieving a set of things from a database where the order doesn't matter.
     */
    fun resolveMany(names: Collection<String>): Set<Long> = names.mapNotNull { resolve(it) }.toSet()

    /**
     * Resolve a PK back into the name. Error if we didn't recognise the PK, because that would constitute a break
     * in the model: putting something into the DB from a mapping and then the mapping somehow being out of sync.
     * @throws Exception if we didn't recognise the PK
     */
    fun reverse(pk: Long): String {
        val result = reverse[pk]

        if (result == null) {
            throw Exception("Didn't recognise Relationship Type PK $pk")
        } else {
            return result
        }
    }
}
