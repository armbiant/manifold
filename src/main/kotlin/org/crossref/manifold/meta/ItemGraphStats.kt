package org.crossref.manifold.meta

import com.fasterxml.jackson.annotation.JsonProperty
import org.crossref.manifold.api.ItemStatsView
import org.crossref.manifold.itemgraph.RelationshipDao

/**
 * Get the current statistics around the data stored in the graph.
 */

fun RelationshipDao.getStats(): ManifoldStats =
    ManifoldStats(
        numItems = requireNotNull(jdbcTemplate.queryForObject("select count(pk) from item;", Long::class.java)),

        totalRelationshipStatements = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from assertion;",
                Long::class.java
            )
        ),

        totalRelationshipStatementSummaries = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from assertion where current = true;",
                Long::class.java
            )
        ),

        totalPropertyStatements = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from property_assertion;",
                Long::class.java
            )
        ),

        totalPropertyStatementSummaries = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from property_assertion where current = true;",
                Long::class.java
            )
        ),

        numItemIdentifiers = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from item_identifier;",
                Long::class.java
            )
        ),

        numRelationshipTypes = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from relationship_type;",
                Long::class.java
            )
        ),

        numEnvelopes = requireNotNull(jdbcTemplate.queryForObject("select count(pk) from envelope;", Long::class.java)),

        numEnvelopeBatches = requireNotNull(
            jdbcTemplate.queryForObject(
                "select count(pk) from envelope_batch;",
                Long::class.java
            )
        ),
    )

/**
 * Get the current statistics around a given Item.
 */

fun RelationshipDao.graphStatsSummary(pk: Long): ItemStatsView? =

    // Check if it exists.
    if (npTemplate.queryForObject(
            "select pk from item where item.pk = :pk",
            mapOf("pk" to pk),
            Long::class.java
        ) == null
    ) {
        null
    } else {
        val numItemIdentifiers =
            requireNotNull(
                npTemplate.queryForObject(
                    "select count(pk) from item_identifier where item_identifier.item_pk = :pk",
                    mapOf("pk" to pk),
                    Long::class.java
                )
            )
        val numKnownOutgoingRelationships =
            requireNotNull(
                npTemplate.queryForObject(
                    "select count(pk) from assertion where current = true AND assertion.from_item_pk = :pk;",
                    mapOf("pk" to pk),
                    Long::class.java
                )
            )
        val numKnownIncomingRelationships =
            requireNotNull(
                npTemplate.queryForObject(
                    "select count(pk) from assertion where where current = true AND assertion.to_item_pk = :pk;",
                    mapOf("pk" to pk),
                    Long::class.java
                )
            )

        ItemStatsView(
            pk = pk,
            numItemIdentifiers = numItemIdentifiers,
            numIncomingRelationships = numKnownIncomingRelationships,
            numOutgoingRelationships = numKnownOutgoingRelationships
        )
    }

/**
 * Statistics that represent the number of Items, Relationships etc in the store.
 */
data class ManifoldStats(

    /**
     * Total number of Items.
     */
    @JsonProperty("num-items")
    val numItems: Long,

    /**
     * Total number of known Relationship Statements.
     */
    @JsonProperty("total-relationship-statements")
    val totalRelationshipStatements: Long,

    /**
     * Total number of known Relationship Statement Summaries.
     */
    @JsonProperty("total-current-relationship-statements")
    val totalRelationshipStatementSummaries: Long,

    /**
     * Total number of known Property Statements.
     */
    @JsonProperty("total-property-statements")
    val totalPropertyStatements: Long,

    /**
     * Total number of known Property Statement Summaries.
     */
    @JsonProperty("total-current-property-statements")
    val totalPropertyStatementSummaries: Long,

    /**
     * Total number of known Identifiers, of any type.
     */
    @JsonProperty("total-item-identifiers")
    val numItemIdentifiers: Long,

    /**
     * Total number of known relationship types.
     */
    @JsonProperty("total-relationship-types")
    val numRelationshipTypes: Long,

    /**
     * Total number of envelopes ever asserted
     */
    @JsonProperty("total-envelopes")
    val numEnvelopes: Long,

    /**
     * Total number of envelopes ever asserted
     */
    @JsonProperty("total-envelope-batches")
    val numEnvelopeBatches: Long,
)
