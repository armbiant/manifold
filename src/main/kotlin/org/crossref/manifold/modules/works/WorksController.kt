package org.crossref.manifold.modules.works

import clojure.lang.Keyword
import clojure.lang.RT
import org.crossref.cayenne
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.registries.AuthorityRegistry
import org.crossref.manifold.retrieval.itemtree.ItemFetchStrategy
import org.crossref.manifold.retrieval.itemtree.ItemTreeRetriever
import org.crossref.manifold.util.clojure.toClj
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

/**
 * REST APIs for the Citeproc-JSON 'works' view.
 */
@RestController
class WorksController(
    val itemTreeRetriever: ItemTreeRetriever,
    val itemResolver: Resolver,
    val authorityRegistry: AuthorityRegistry
) {
    init {
        // Configure Cayenne's identifier types.
        // Required because the identifier setup is part of Cayenne's dynamic 'core' configuration system.
        cayenne.boot()
    }

    @GetMapping("/v2/works/{*identifier}", produces = ["application/json"])
    fun getWork(@PathVariable identifier: String): String = work(ItemFetchStrategy.DEFAULT, identifier)

    @GetMapping("/v2/perspective/{perspective}/works/{*identifier}", produces = ["application/json"])
    fun getPerspectiveWork(
        @PathVariable perspective: String,
        @PathVariable identifier: String
    ): String = work(perspective, identifier)

    /**
     * Retrieve the Citeproc-json render of the Item Tree for the given Identifier.
     */
    private fun work(perspective: String, identifierStr: String): String {
        // Need to trim the leading slash.
        val identifier = itemResolver.resolveRO(Identifier(identifierStr.substring(1)))
        if (identifier.pk == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")
        }

        val strategy = ItemFetchStrategy.fromPerspective(
            perspective,
            authorityRegistry.getAuthorityRootPks(itemResolver),
            authorityRegistry.getAuthorityPks(itemResolver),
            3
        )
        val itemTree = itemTreeRetriever.get(identifier.pk, strategy)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")

        /** The Item Tree is converted from JSON to Clojure types. For the most part this is generic, but the following
         * fields require special treatment, and require the value to be a keyword, not a string.
         */
        val fieldsToMakeValuesKeywords = setOf("type", "subtype", "kind")

        val converted = itemTree.toClj(fieldsToMakeValuesKeywords)

        val esDoc = cayenne.itemToEsDoc(converted)

        // This field isn't meaningful (it's designed to indicate when the document was indexed in elastic) so remove it.
        val cleanedEsDoc = RT.dissoc(esDoc, Keyword.intern("indexed"))

        val citeproc = cayenne.esDocToCiteproc(cleanedEsDoc)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")

        return cayenne.writeJsonString(citeproc)
    }
}
