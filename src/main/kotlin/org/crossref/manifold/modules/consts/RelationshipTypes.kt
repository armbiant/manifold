package org.crossref.manifold.modules.consts

/**
 * Collection of Relationship type constants. These correspond to the content of relationships.schema.json and are
 * included to support Module authors in using a shared vocabulary.
 */
object RelationshipTypes {
    /**
     * Records that the Object Item is the maintainer of the Subject.
     */
    const val STEWARD = "steward"

    /**
     * Records that the object Item is the Registration Agency of the Subject.
     * This is always going to be Crossref for this module.
     */
    const val RA_REL_TYPE = "registration-agency"

    /**
     * This has a resolution URL of that (landing page modelled as an Item).
     */
    const val RESOLUTION_URL_REL_TYPE = "resolution-url"

    const val MEMBER_OF = "member-of"
}

