package org.crossref.manifold.modules.consts

/**
 * Collection of Identifier type constants. These correspond to the content of identifiers.schema.json and are included
 * to support Module authors in using a shared vocabulary.
 */
object IdentifierTypes {
    /**
     * The prefix used to mark Crossref-owned Blank Nodes.
     */
    const val CR_BLANK_NODE_PREFIX = "http://id.crossref.org/blank/"

    /**
     * Item applies to the whole Manifold kernel, so we bake it into the Identifier Type registry.
     */
    const val ITEM_URI = "http://id.crossref.org/item/"
    const val ITEM_PREFIX = "item"

    // Prefix for non-persistent items. This is defined elsewhere as it's fundamental to the identifier type registry,
    // and referenced here to define it in the same location as consts.
    const val NON_PERSISTENT_PREFIX = org.crossref.manifold.registries.NON_PERSISTENT
}
