package org.crossref.manifold.modules.eventdata.support.ingester

import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.consts.IdentifierTypes.CR_BLANK_NODE_PREFIX
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.eventdata.support.Action
import org.crossref.manifold.modules.eventdata.support.EvidenceRecord
import org.crossref.manifold.modules.eventdata.support.Match
import org.crossref.manifold.modules.eventdata.support.Page
import org.crossref.manifold.retrieval.view.markBlankNodes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.OffsetDateTime

/**
 * This package contains a collection of function dedicated to the mapping
 * of Event Data evidence records to manifold Item Trees.
 *
 * The code is highly domain specific, containing assumptions and specificities
 * that originate from event data.
 *
 * The evidence records are produced by the Event Data Percolator and they are stored
 * in JSON format. The main path to reach an event is page -> action -> match.
 *
 * In Event Data, observations made by the various agents are attempted to be matched to DOIs.
 * A successful match is turned into an event. An action contains both the observations and
 * the matches and the resulting events. As the events result from observations and matches we can ignore
 * them during the mapping and gather all needed data from the former entities.
 *
 */

private const val RELATIONSHIP_RETWEET = "is-retweet-of"
private const val RELATIONSHIP_OBJECT = "object"
private const val RELATIONSHIP_SOURCE = "source"

const val TWITTER = "twitter"
const val WIKIPEDIA = "wikipedia"
const val REDDIT = "reddit"
const val REDDIT_LINKS = "reddit-links"
const val STACK_EXCHANGE = "stackexchange"
const val NEWSFEED = "newsfeed"
const val WORDPRESS_DOT_COM = "wordpressdotcom"
const val HYPOTHESIS = "hypothesis"

private val regexTwitterId = """^twitter://status\?id=(\d+)$""".toRegex()
private val regexTwitterIdLegacy = """^http://twitter.com/.*/statuses/(\d+)$""".toRegex()
private val regexTwitterAuthor = """^twitter://user\?screen_name=(\w+)$""".toRegex()
private val regexTwitterAuthorLegacy = """^http://www\.twitter\.com/(\w+)$""".toRegex()
private val assertingParty = Item().withIdentifier(Identifier(Items.CROSSREF_AUTHORITY))

class NullAuthorException : Exception("Null author")
class TwitterAuthorRegexException(private val authorUrl: String) :
    Exception("Failed to obtain author url from $authorUrl")

class TwitterIdRegexException(private val url: String) : Exception("Failed to obtain tweetId from $url")
object EvidenceRecordConverter {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)
}


/**
 * Get the value of the first group of this [Regex.matchEntire] applied to str.
 *
 * @receiver [Regex]
 * @return String?
 */
fun Regex.parse(str: String?) = this.matchEntire(str.orEmpty())?.groups?.get(1)?.value

/**
 * Creates a map of properties for the subject of the reified relationship.
 *
 * The properties map needs to match a certain item type.
 * In event data the subject is always a url therefore the type is 'web-resource'.
 * The subtype is 'annotation' or 'web-content' depending on the agent's relationship type.
 *
 * Further properties are included if present and depending on the agent.
 * The item type and subtype are always included to the map.
 *
 * @receiver [Action]
 */
fun Action.getSubjectProperties(sourceId: String, extra: Map<String, String>? = emptyMap()): Map<String, String> {

    val m = mutableMapOf<String, String>()

    m["type"] = "web-resource"

    m["subtype"] = when (sourceId) {
        HYPOTHESIS -> if (relationTypeId == "annotates") "annotation" else "web-content"
        else -> "web-content"
    }

    subj?.issued?.let { m["issued"] = it }

    if (sourceId !in setOf(HYPOTHESIS, TWITTER)) {
        subj?.title?.let { m["title"] = it }
    }

    if (sourceId == TWITTER) {
        extra?.get("username")?.let { m["username"] = it }
    }

    if (sourceId == STACK_EXCHANGE) {
        subj?.author?.url?.let { m["username"] = it }
    }

    return m
}

/**
 * Creates a map of properties for the "observation part" of the reified relationship.
 *
 * The properties map needs to match a certain item type.
 * For event data the intermediate object of a reified relationship always points to an
 * already known or possibly existing DOI. Therefore, the type of the item is 'reference'
 * and the subtype 'work'.
 *
 * The 'text' property is mandatory.
 * It holds the value of the observation in the form of unstructured text.
 *
 * @receiver [Match]
 *
 */
fun Match.getObservationProperties(): Map<String, String> {

    val m = mutableMapOf<String, String>()

    m["type"] = "reference"

    m["subtype"] = "work"

    m["text"] = this.value

    return m
}

/**
 * Converts a Page to a list of envelop pairs.
 * The processing takes place at the action level.
 * We only process actions that have matches.
 *
 * For more details on the returned results see [envelopesFrom].
 *
 * @receiver [Page]
 */
private fun Page.toItemTrees(
    sourceId: String,
    filename: String,
    timestamp: String
): List<Pair<Envelope, Envelope>> =

    actions
        .filter { action -> action.matches.isNotEmpty() }
        .mapNotNull { action ->
            try {
                // We create a two hop relationship between the subject web-resource,
                // the intermediate reference item and the target work object.
                envelopesFrom(
                    subj = action.url,
                    relType = action.relationTypeId,
                    observedAt = action.occurredAt,
                    matchedAt = timestamp,
                    matches = action.matches,
                    subjectProperties = action.getSubjectProperties(sourceId)
                )
            } catch (e: Exception) {
                EvidenceRecordConverter.logger.error("Exception while processing $sourceId action ${action.id} in evidence record $filename")
                e.printStackTrace()
                null
            }
        }

/**
 * Creates one or more reified relationships between a subject and one or more objects.
 *
 * The importance of this function is to guarantee the structure of the reified
 * model. We want to create item trees with a structure of:
 *
 *     subject -reference-> referenceItem -object-> work
 *
 * In event data agents make observations, where a URL subject observes a possible mention
 * to a DOI on a certain resource on the internet, for example Twitter, Wikipedia, blogs etc.
 *
 * The observations are verified by the Event Data Percolator, that either confirms discovered DOIs
 * or attempts to discover possible ones based on the observation. The matches are used to construct
 * events. The event represents the relationship between the subject and each of the discovered DOIs.
 * The discovered DOIs are the object of this relationship.
 *
 * The function creates the first part (subject to intermediate item/s) of the reified relationship as an
 * 'observations' envelope and the second part (intermediate item/s to object/s) as a 'matches' envelope.
 * The asserting party for both envelopes is Crossref.
 *
 * The function returns a Pair of observationsEnvelope to matchesEnvelope
 * so that we can maintain the link between these two related envelopes.
 *
 * @return Pair<K, V> where K = observations envelope, V = matches envelope
 */
private fun envelopesFrom(
    subj: String,
    relType: String,
    observedAt: String,
    matchedAt: String,
    matches: List<Match>,
    subjectProperties: Map<String, String?> = emptyMap(),
    additionalObservationRelationships: List<Relationship> = emptyList()
): Pair<Envelope, Envelope> {


    // Convert matches to relationships keeping track of which relationship
    // matches each DOI object.
    val candidates = matches.map { match ->
        // To properly mark blank nodes we need to provide the parent node that
        // these relationships will be attached to further down the pipeline.
        // Therefore, we start from our subject even though in the end we will
        // only keep the relationships in this operation.
        val rel = Item(listOf(Identifier(subj)))
            .withRelationship(
                Relationship(
                    relTyp = relType,
                    obj = Item()
                        .withPropertiesFromMap(match.getObservationProperties())
                )
            )
            .let {
                markBlankNodes(it, CR_BLANK_NODE_PREFIX)
            }
            .rels[0]

        // These pairs are necessary because we want to attribute part of the tree to
        // one source of provenance and the rest to another (two different envelopes).
        // Therefore, we need to keep track of which of the produced relationships matches
        // each of the matched DOIs as it would be more complicated and error-prone to try and
        // identify them from the observation text for example.
        rel to match.match
    }

    // Create one item tree for the observations,
    // credited to the agent
    val observations = Item(listOf(Identifier(subj)))
        .withRelationships(candidates.map { it.first })
        .withPropertiesFromMap(subjectProperties)
        .withAdditionalObservationRelationships(additionalObservationRelationships)

    // Create multiple item trees for the matches,
    // credited to the percolator
    val matchedItems = candidates.map {
        Item()
            .withIdentifier(it.first.obj.identifiers[0])
            .withRelationships(
                listOf(
                    Relationship(
                        relTyp = RELATIONSHIP_OBJECT,
                        obj = Item()
                            .withIdentifier(Identifier(it.second))
                    )
                )
            )
    }

    val observationsEnvelope = Envelope(
        listOf(observations),
        ItemTreeAssertion(
            assertedAt = OffsetDateTime.parse(observedAt),
            mergeStrategy = MergeStrategy.UNION_CLOSED,
            assertingParty = assertingParty
        )
    )

    val matchesEnvelope = Envelope(
        matchedItems,
        ItemTreeAssertion(
            assertedAt = OffsetDateTime.parse(matchedAt),
            mergeStrategy = MergeStrategy.UNION_CLOSED,
            assertingParty = assertingParty
        )
    )

    return observationsEnvelope to matchesEnvelope
}

fun Item.withAdditionalObservationRelationships(additional: List<Relationship>) =
    // A deep dive to inject additional relationships
    withRelationships(rels.map { relationship ->
        relationship.withItem(
            relationship.obj.withRelationships(
                relationship.obj.rels
                        + additional
            )
        )
    })


/**
 * Constructs a twitter agent subject identifier.
 * For the structure of the identifier see [twitterIdentifierFrom].
 *
 * This function is meant to construct an identifier for the actual tweet observed and its author
 * ignoring any related original tweet and original author. The logic of capturing the current tweet
 * author and id is the same for both original tweets and retweets.
 *
 * @return [Triple] of <author: String, tweetId: String, compositeIdentifier: String>.
 * @throws [IllegalStateException] when author is missing. This should not happen if the Action is a twitter agent action.
 * @receiver [Action]
 */
fun Action.getTwitterSubjectIdentifierComponents(): Triple<String, String, String> {

    if (subj?.author?.url == null) {
        throw NullAuthorException()
    }

    val author = setOf(regexTwitterAuthor, regexTwitterAuthorLegacy)
        .firstNotNullOfOrNull { it.parse(subj?.author?.url) }

    if (author.isNullOrBlank()) {
        throw TwitterAuthorRegexException(subj?.author?.url)
    }

    val tweetId = setOf(regexTwitterId, regexTwitterIdLegacy)
        .firstNotNullOfOrNull { it.parse(url) }

    if (tweetId.isNullOrBlank()) {
        throw TwitterIdRegexException(url)
    }

    return Triple(author, tweetId, twitterIdentifierFrom(author, tweetId))
}

/**
 * Constructs a twitter agent object identifier.
 * For the structure of the identifier see [twitterIdentifierFrom].
 *
 * This is highly specific to modeling retweets. Contrary to all other agents and
 * even to Twitter agent original tweets, retweets are not modelled as a reified relationship. See [toReTweet].
 * Therefore, retweets are model as a direct assertion between a subject twitter identifier and an object twitter identifier.
 *
 * @return Identifier representing the original author and tweetId referenced in a retweet.
 * @throws [IllegalStateException] if either the author or the tweetId are null or blank.
 * @receiver [Action]
 */
fun Action.getTwitterObjectIdentifier(): String {

    if (subj?.originalTweetAuthor == null) {
        throw NullAuthorException()
    }

    val author = setOf(regexTwitterAuthor, regexTwitterAuthorLegacy)
        .firstNotNullOfOrNull { it.parse(subj?.originalTweetAuthor) }

    if (author.isNullOrBlank()) {
        throw TwitterAuthorRegexException(subj?.originalTweetAuthor)
    }

    val tweetId = setOf(regexTwitterId, regexTwitterIdLegacy)
        .firstNotNullOfOrNull { it.parse(subj?.originalTweetUrl) }

    if (tweetId.isNullOrBlank()) {
        throw TwitterIdRegexException(subj?.originalTweetUrl.orEmpty())
    }

    return twitterIdentifierFrom(author, tweetId)
}

/**
 * Constructs a Pair of [Envelope] objects for a Twitter agent original tweet.
 *
 * For original tweets the main difference between Twitter records and other agents'
 * records is the composite identifier.
 *
 * Similarly to other agents, original tweets are modeled as reified relationships with
 * the subject being the [Action.url] and the object being the [Match.match].
 *
 * For more details @see [envelopesFrom], [getTwitterSubjectIdentifierComponents].
 *
 * @receiver [Match]
 * @return Pair<Envelope, Envelope>
 */
private fun Action.toOriginalTweet(
    action: Action,
    timestamp: String
): Pair<Envelope, Envelope> {

    val subjectIdentifier = action.getTwitterSubjectIdentifierComponents()

    return envelopesFrom(
        subj = subjectIdentifier.third,
        relType = action.relationTypeId,
        observedAt = action.occurredAt,
        matchedAt = timestamp,
        matches = matches,
        subjectProperties = action.getSubjectProperties(TWITTER, mapOf("username" to subjectIdentifier.first)),
    )
}


/**
 * Constructs a Pair of [Envelope] objects for a Twitter agent retweet.
 *
 * Contrary to other agents and to original tweets, retweets are modelled as direct assertions
 * between a subject and object tweet identifier, with the subject being the [Action.url]
 * and the object being the [Action.subj.originalTweetUrl].
 *
 * @see [getTwitterSubjectIdentifierComponents],[getTwitterObjectIdentifier]
 * @receiver [Match]
 * @return Pair<Envelope, Envelope>
 */
private fun Action.toReTweet(assertingParty: Item): Pair<Envelope, Envelope?> {

    val subjectIdentifier = getTwitterSubjectIdentifierComponents()

    val objectIdentifier = getTwitterObjectIdentifier()

    val itemTree = Item()
        .withIdentifier(Identifier(subjectIdentifier.third))
        .withRelationships(
            listOf(
                Relationship(
                    relTyp = RELATIONSHIP_RETWEET,
                    obj = Item().withIdentifier(Identifier(objectIdentifier))
                )
            )
        ).withPropertiesFromMap(getSubjectProperties(TWITTER))


    val envelope = Envelope(
        listOf(itemTree),
        ItemTreeAssertion(
            assertedAt = OffsetDateTime.parse(this.occurredAt),
            mergeStrategy = MergeStrategy.UNION_CLOSED,
            assertingParty = assertingParty
        )
    )

    return envelope to null
}

/**
 * Check if an action is a retweet.
 *
 * @receiver [Action]
 * @return true if a tweet's url does not match its original url, false otherwise.
 */
fun Action.isReTweet() = subj?.originalTweetUrl != null && url != subj.originalTweetUrl

/**
 * Create a composite twitter identifier.
 *
 * Twitter identifiers have the form of:
 *
 *   "twitter://status?author=${author}&tweet-id=${tweetId}"
 *
 * @return twitter identifier string.
 */
fun twitterIdentifierFrom(author: String, tweetId: String) =
    "twitter://status?author=$author&tweet-id=$tweetId"

/**
 * Dispatch processing to [toOriginalTweet] and [toReTweet] accordingly
 * gathering and returning the results.
 *
 * @see [isReTweet],[toReTweet],[toOriginalTweet].
 * @receiver [Page]
 */
private fun Page.toTwitterItemTrees(
    filename: String,
    timestamp: String
): List<Pair<Envelope, Envelope?>> =
    actions
        .filter { action -> action.matches.isNotEmpty() }
        .flatMap { action ->

            val results = mutableListOf<Pair<Envelope, Envelope?>>()
            try {

                // We may get one retweet assertion
                if (action.isReTweet()) {
                    results.add(action.toReTweet(assertingParty))
                }

                results.add(action.toOriginalTweet(action, timestamp))

            } catch (e: Exception) {
                when (e) {
                    is NullAuthorException,
                    is TwitterIdRegexException,
                    is TwitterAuthorRegexException -> EvidenceRecordConverter.logger.error("${e.message} - Evidence record: $filename, action: ${action.id}")

                    else -> throw e
                }
            }

            results
        }

/**
 * Constructs a Pair of [Envelope] objects for a Wikipedia agent evidence record.
 * Takes care of attaching a 'source' relationship from the intermediate item to the item
 * holding the edit url for a wikipedia page.
 *
 * @see [envelopesFrom]
 * @receiver [Page]
 * @return Pair<Envelope, Envelope>
 */
private fun Page.toWikipediaItemTrees(filename: String, timestamp: String): List<Pair<Envelope, Envelope>> {

    val (valid, rest) =
        actions.filter { action -> action.matches.isNotEmpty() }
            .partition { action -> !action.subj?.url.isNullOrBlank() }

    rest.forEach {
        EvidenceRecordConverter.logger.error("Missing attribute action.subj.url for $WIKIPEDIA action ${it.id} in evidence record $filename")
    }

    return valid
        .map { action ->

            // This is an item holding the edit URL for the current Wikipedia page.
            // We filter out the tittle because it is not needed in this instance
            // (it will be included in the subject properties).
            val additionalObservationRelationships = listOf(
                Relationship(
                    relTyp = RELATIONSHIP_SOURCE,
                    Item(listOf(Identifier(action.subj?.url!!)))
                        .withPropertiesFromMap(
                            action.getSubjectProperties(WIKIPEDIA)
                                .minus("title")
                        )
                )
            )

            // We create a two hop relationship between the subject web-resource,
            // the intermediate reference item and the target work object.
            envelopesFrom(
                subj = action.url,
                relType = action.relationTypeId,
                observedAt = action.occurredAt,
                matchedAt = timestamp,
                matches = action.matches,
                subjectProperties = action.getSubjectProperties(WIKIPEDIA),
                additionalObservationRelationships = additionalObservationRelationships
            )
        }
}

/**
 * Constructs a Pair of [Envelope] objects for a hypothes.is agent evidence record.
 * Takes care of attaching a 'source' relationship from the intermediate item to the item
 * holding the json url for a hypothes.is pid.
 *
 * @see [envelopesFrom]
 * @receiver [Page]
 * @return Pair<Envelope, Envelope>
 */
private fun Page.toHypothesisItemTrees(filename: String, timestamp: String): List<Pair<Envelope, Envelope>> {
    val (valid, rest) =
        actions.filter { action -> action.matches.isNotEmpty() }
            .partition { action -> !action.subj?.jsonUrl.isNullOrBlank() }

    rest.forEach {
        EvidenceRecordConverter.logger.error("Missing attribute action.subj.jsonUrl for $HYPOTHESIS action ${it.id} in evidence record $filename")
    }

    return valid
        .filter { action -> action.matches.isNotEmpty() }
        .map { action ->

            // Represent the json URL as a source relationship.
            val additionalObservationRelationships = listOf(
                Relationship(
                    relTyp = RELATIONSHIP_SOURCE,
                    Item(listOf(Identifier(action.subj?.jsonUrl!!)))
                        .withPropertiesFromMap(
                            action.getSubjectProperties(HYPOTHESIS)
                                .plus("subtype" to "web-content")
                        )
                )
            )

            // We create a two hop relationship between the subject web-resource,
            // the intermediate reference item and the target work object.
            envelopesFrom(
                subj = action.url,
                relType = action.relationTypeId,
                observedAt = action.occurredAt,
                matchedAt = timestamp,
                matches = action.matches,
                subjectProperties = action.getSubjectProperties(HYPOTHESIS),
                additionalObservationRelationships = additionalObservationRelationships
            )
        }
}


/**
 * Dispatch processing to the appropriate extension.
 *
 * @see [toHypothesisItemTrees], [toWikipediaItemTrees], [toTwitterItemTrees]
 * @receiver [Page]
 * @return list of Pair<Envelope, Envelope?> or emptyList()
 */
private fun Page.convert(
    sourceId: String,
    filename: String,
    timestamp: String
): List<Pair<Envelope, Envelope?>> =
    when (sourceId) {
        REDDIT,
        REDDIT_LINKS,
        STACK_EXCHANGE,
        NEWSFEED,
        WORDPRESS_DOT_COM -> toItemTrees(sourceId, filename, timestamp)

        HYPOTHESIS -> toHypothesisItemTrees(filename, timestamp)

        WIKIPEDIA -> toWikipediaItemTrees(filename, timestamp)

        TWITTER -> toTwitterItemTrees(filename, timestamp)
        else -> {
            EvidenceRecordConverter.logger.warn("Skipping $filename due to unknown sourceId: $sourceId")
            emptyList()
        }
    }

/**
 * Accepts an Eventdata [EvidenceRecord] object and returns
 * an item graph representation in the form of multiple envelope batches.
 *
 *  Multiple envelope batches may be produced per evidence record.
 *  Empty envelope batches are discarded.
 *
 * @see [convert]
 * @return none, one or many of [EnvelopeBatch].
 */
fun convertEvidenceRecordToEnvelopeBatch(
    filename: String,
    evidenceRecord: EvidenceRecord
): List<EnvelopeBatch> {

    // Get pairs of envelopes where:
    // key = agent accredited envelopes,
    // value = percolator accredited envelopes
    val envelopes = evidenceRecord.pages.flatMap { page ->
        page.convert(evidenceRecord.sourceId, filename, evidenceRecord.timestamp)
    }

    // Gather all keys in an agent envelope batch
    val agentEnvelopeBatch = EnvelopeBatch(
        envelopes.map { it.first }.filter { envelope -> envelope.itemTrees.isNotEmpty() },
        EnvelopeBatchProvenance("Crossref ${evidenceRecord.sourceId} agent/1.0", filename)
    )

    // Gather all values in a percolator envelope batch
    val percolatorEnvelopeBatch = EnvelopeBatch(
        envelopes = envelopes.mapNotNull { it.second }
            .filter { envelope -> envelope.itemTrees.isNotEmpty() },
        EnvelopeBatchProvenance("Crossref percolator/1.0", filename)
    )

    // Return non-empty envelope batches
    return listOf(percolatorEnvelopeBatch, agentEnvelopeBatch)
        .filter { it.envelopes.isNotEmpty() }
}