package org.crossref.manifold.modules.eventdata

import io.micrometer.core.instrument.MeterRegistry
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.crossref.manifold.modules.StartupTask
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.services.s3.S3Client

@Configuration(Module.EVENT_DATA)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val EVENT_DATA = "event-data"
        const val INGEST_JSON_SNAPSHOT = "ingest-json-snapshot"
        const val SOURCE_ALLOW_LIST = "source-allow-list"
        const val BUS_CONTROLLER = "bus-controller"
        const val JWT_SECRETS = "jwt-secrets"
        const val AGENTS_ALLOW_LIST = "agents-allow-list"
        const val CONVERT_RECORDS_SRC = "convert-records-src"
        const val CONVERT_RECORDS_DST = "convert-records-dst"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = EVENT_DATA,
                description = "Crossref Event Data events"
            )
        )
    }

    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [INGEST_JSON_SNAPSHOT])
    fun jsonSnapshotIngester(
        meterRegistry: MeterRegistry?,
        itemGraph: ItemGraph,
        @Value("\${$EVENT_DATA.$INGEST_JSON_SNAPSHOT}") jsonSnapshotFilename: String,
        @Value("\${$EVENT_DATA.$SOURCE_ALLOW_LIST:}") sourceWhitelist: Set<String>
    ): StartupTask = StartupTask({
        EventSnapshotIngester.ingestJsonSnapshot(
            meterRegistry,
            itemGraph,
            jsonSnapshotFilename,
            sourceWhitelist
        )
    }, {
        EventSnapshotIngester.destroy()
    })

    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [BUS_CONTROLLER])
    fun busController(
        meterRegistry: MeterRegistry?,
        itemGraph: ItemGraph,
        @Value("\${$EVENT_DATA.$JWT_SECRETS:}") jwtSecrets: Set<String>,
        @Value("\${$EVENT_DATA.$SOURCE_ALLOW_LIST:}") sourceWhitelist: Set<String>
    ): BusController = BusController(
        meterRegistry,
        itemGraph,
        jwtSecrets,
        sourceWhitelist
    )

    /**
     * An ingester for Event Data evidence records.
     * The ingester subscribes to an SQS queue awaiting S3 published notifications.
     */
    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [S3_NOTIFICATION_QUEUE])
    fun s3EvidenceRecordIngester(
        itemGraph: ItemGraph,
        s3Client: S3Client,
        @Value("\${$EVENT_DATA.$AGENTS_ALLOW_LIST}") agentsAllowList: Array<String>
    ): S3EvidenceRecordIngester = S3EvidenceRecordIngester(
        itemGraph,
        s3Client,
        agentsAllowList.toSet()
    )

    /**
     * Read one or many evidence record archives from src and produce item tree archives
     * stored at dst.
     *
     * @param src Absolute path to file or directory
     * @param dst Absolute path to directory
     */
    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [CONVERT_RECORDS_SRC])
    fun evidenceRecordSnapshotConverter(
        @Value("\${$EVENT_DATA.$CONVERT_RECORDS_SRC}") src: String,
        @Value("\${$EVENT_DATA.$CONVERT_RECORDS_DST}") dst: String,
        @Value("\${$EVENT_DATA.$AGENTS_ALLOW_LIST}") agentsAllowList: Array<String>
    ): StartupTask = StartupTask({
        EvidenceRecordSnapshotConverter.run(src, dst, agentsAllowList.toSet())
    })
}
