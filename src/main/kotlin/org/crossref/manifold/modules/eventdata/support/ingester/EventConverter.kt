package org.crossref.manifold.modules.eventdata.support.ingester

import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.eventdata.support.Event
import java.time.OffsetDateTime

private const val RELATIONSHIP_RETWEET = "is-retweet-of"
private const val RELATIONSHIP_OBJECT = "object"

/**
 * Convert an object to an Item
 */
@Deprecated("Needs to be updated")
fun Event.toItem(): Item? =

    if (subj.pid.isBlank() || obj.pid.isBlank()) {
        logger.warn("WARNING! Incomplete event entry. Will not covert. $this")
        null
    } else {

        val objItem = Item(identifiers = listOf(Identifier(obj.pid)))

        when (sourceId) {
            "twitter" -> {
                val properties = mapOf(
                    "title" to subj.title,
                    "author" to subj.author,
                    "original-tweet-url" to subj.originalTweetUrl,
                    "original-tweet-author" to subj.originalTweetAuthor,
                    "alternative-id" to subj.alternativeId
                ).filter { (k, v) -> v != null }

                Item(
                    listOf(Identifier(subj.pid)),
                    listOf(Relationship(relationTypeId, objItem)),
                ).withPropertiesFromMap(properties)
            }
            else -> Item(
                listOf(Identifier(subj.pid)),
                listOf(Relationship(relationTypeId, objItem)),
            )
        }
    }

/**
 * Convert an event to an envelope
 */
@Deprecated("Needs to be updated")
fun parseEventToEnvelopeBatch(
    event: Event,
    mergeStrategy: MergeStrategy = MergeStrategy.NAIVE
): EnvelopeBatch? =
    event.toItem()?.let { item ->
        val itemTreeAssertion = ItemTreeAssertion(
            assertedAt = OffsetDateTime.parse(event.occurredAt),
            assertingParty = Item().withIdentifier(Identifier(Items.CROSSREF_AUTHORITY)),
            mergeStrategy = mergeStrategy
        )

        val envelope = Envelope(
            listOf(item),
            itemTreeAssertion
        )
        EnvelopeBatch(listOf(envelope), EnvelopeBatchProvenance("Crossref ${event.sourceToken}", event.id))
    }