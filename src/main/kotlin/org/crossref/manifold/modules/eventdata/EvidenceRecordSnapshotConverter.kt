package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.crossref.manifold.bulk.EnvelopeBatchArchiveWriter
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.modules.eventdata.support.EvidenceRecord
import org.crossref.manifold.modules.eventdata.support.ingester.convertEvidenceRecordToEnvelopeBatch
import org.crossref.manifold.modules.unixml.support.PARSE_PARALLELISM
import org.crossref.manifold.modules.unixml.support.sendTarGzTo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString
import kotlin.io.path.isDirectory
import kotlin.io.path.name
import kotlin.system.exitProcess

object EvidenceRecordSnapshotConverter {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper: ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setDateFormat(StdDateFormat().withColonInTimeZone(true))
        .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        .registerModule(KotlinModule())

    fun run(src: String, dst: String, agentsAllowList: Set<String>) {
        val srcPath = Path(src).toAbsolutePath()
        val dstPath = Path(dst).toAbsolutePath()

        require(dstPath.isDirectory()) { "dstPath must be a directory" }

        if (srcPath.isDirectory()) {
            srcPath.toFile().listFiles()
                .filter { it.isFile && it.extension == "gz" }
                .forEach {
                    val file = Path(
                        dstPath.absolutePathString(),
                        "${it.name.removeSuffix(".tar.gz")}.tree.tar.gz"
                    ).toFile()
                    convertSingleFile(it, file, agentsAllowList)
                }
        } else {
            val file = Path(
                dstPath.absolutePathString(),
                "${srcPath.name.removeSuffix(".tar.gz")}.tree.tar.gz"
            ).toFile()
            convertSingleFile(srcPath.toFile(), file, agentsAllowList)
        }
        exitProcess(0)
    }


    private fun convertSingleFile(baseFile: File, outputFile: File, agentsAllowList: Set<String>) {
        val archive = EnvelopeBatchArchiveWriter(outputFile)

        val contentFilenameChannel = Channel<Pair<String, String>>(PARSE_PARALLELISM)
        val envelopeBatchChannel = Channel<Pair<String, Collection<EnvelopeBatch>>>(PARSE_PARALLELISM)

        runBlocking {

            val parseTasks = (0..PARSE_PARALLELISM).map<Int, Job> { it ->
                launch(CoroutineName("parse-evidence-record-$it") + Dispatchers.IO) {
                    logger.info("Start Evidence record ingester on ${Thread.currentThread().name}")

                    for ((itemFilename, content) in contentFilenameChannel) {
                        try {
                            try {
                                mapper.readValue<EvidenceRecord>(content)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            val evidenceRecord = mapper.readValue<EvidenceRecord>(content)

                            if(evidenceRecord.sourceId in agentsAllowList){
                                val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(
                                    filename = itemFilename,
                                    evidenceRecord = evidenceRecord
                                )
                                    .filter { envelopeBatch -> envelopeBatch.envelopes.isNotEmpty() }

                                if (envelopeBatches.isNotEmpty()) {
                                    envelopeBatchChannel.send(itemFilename to envelopeBatches)
                                }
                            }
                        } catch (exception: Exception) {
                            logger.error("Failed file $itemFilename")
                            exception.printStackTrace()
                        }
                    }

                }
            }

            val writeTask = launch(CoroutineName("write") + Dispatchers.IO) {
                try {
                    archive.use {
                        for ((filename, envelopeBatches) in envelopeBatchChannel) {
                            archive.add(filename, envelopeBatches)
                        }
                    }
                } catch (e: Exception) {
                    logger.error("Archive writing: ${e.message}")
                }
            }


            // Read the Zip file sequentially in this thread. Others will continue in the background.
            logger.info("Read TGZ...")

            sendTarGzTo(baseFile, contentFilenameChannel)

            logger.info("Wait for parsing")
            parseTasks.joinAll()
            envelopeBatchChannel.close()

            logger.info("Wait for writing...")
            writeTask.join()
            logger.info("Finished writing...")
        }

    }
}