package org.crossref.manifold.modules.eventdata.support.bus

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.MergeStrategy
import org.crossref.manifold.modules.eventdata.support.Event
import org.crossref.manifold.modules.eventdata.support.ingester.logger
import org.crossref.manifold.modules.eventdata.support.ingester.parseEventToEnvelopeBatch
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

fun extractToken(headers: HttpHeaders): String? = headers["authorization"]
    ?.first { it.startsWith("Bearer") }
    ?.substringAfter("Bearer ", "error")
    .takeIf { !it.equals("error") }

fun postEvent(
    multiJwtVerifier: MultiJwtVerifier,
    headers: HttpHeaders,
    itemGraph: ItemGraph,
    sourceWhitelist: Set<String>,
    event: Event
): Boolean =
    multiJwtVerifier
        .verify(extractToken(headers))
        .let { jwt ->
            if (jwt != null && jwt.subject.equals(event.sourceId)) {
                if (sourceWhitelist.contains(event.sourceId)) {
                    try {
                        val envelopeBatch = parseEventToEnvelopeBatch(event, MergeStrategy.UNION_CLOSED)

                        if (envelopeBatch != null) {
                            itemGraph.ingest(envelopeBatch)
                            logger.info("EVENT ${envelopeBatch.provenance.externalTrace}")
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        false
                    }
                    true
                } else {
                    logger.warn("Ignoring event ${event.id} of non allowed source: ${event.sourceId}")
                    false
                }
            } else {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid or missing authentication token")
            }
        }
