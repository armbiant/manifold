package org.crossref.manifold.modules.relationships

import org.crossref.manifold.api.ItemIdentifierView
import org.crossref.manifold.api.ItemView
import org.crossref.manifold.api.RelationshipListView
import org.crossref.manifold.api.RelationshipView
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.registries.IdentifierTypeRegistry

class RelationshipService(
    private val resolver: Resolver,
    private val itemDao: ItemDao,
    private val relationshipDao: RelationshipDao,
    private val identifierTypeRegistry: IdentifierTypeRegistry
) {
    fun getRelationships(uri: String): RelationshipListView {
        val resolvedUri = resolver.resolveRO(Identifier(uri))
        return if (resolvedUri.pk == null) {
            RelationshipListView(emptyList())
        } else {
            val pkInSet = setOf(resolver.resolveRO(Identifier(uri)).pk as Long)
            val relationshipStatements =
                relationshipDao.getCurrentRelsResolvingReified(pkInSet)
            val itemViewsByItemPk: Map<Long, ItemView> =
                itemDao.fetchItemIdentifierPairsForItemPks(relationshipStatements.flatMap {
                    setOf(
                        it.assertingPartyPk,
                        it.fromItemPk,
                        it.toItemPk
                    )
                }.toSet())
                    .groupBy({ it.first },
                        { ItemIdentifierView(it.second.prefix, identifierTypeRegistry.tokenizedToUri(it.second)) })
                    .mapValues {
                        ItemView(
                            it.value
                        )
                    }
            RelationshipListView(relationshipStatements.map {
                RelationshipView(
                    itemViewsByItemPk[it.fromItemPk] as ItemView,
                    itemViewsByItemPk[it.toItemPk] as ItemView,
                    itemViewsByItemPk[it.assertingPartyPk] as ItemView,
                    it.relationshipType,
                    it.assertedAt.toString()
                )
            })
        }
    }
}
