package org.crossref.manifold.modules.relationships

import org.crossref.manifold.api.RelationshipListView
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

@ResponseBody
@RequestMapping("/v2/relationships")
class RelationshipController(private val relationshipService: RelationshipService) {
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRelationships(@RequestParam uri: String): RelationshipListView = relationshipService.getRelationships(uri)
}
