package org.crossref.manifold.modules.dump

import org.crossref.manifold.itemgraph.RelationshipDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.stereotype.Service
import javax.annotation.PreDestroy

/**
 * If the user supplies a "DUMP_GRAPHVIZ" argument, save the whole database to that filename in GraphViz DOT format before exit.
 */
@Service
class DumpSql(
    @Autowired
    val env: Environment,

    @Autowired
    private val sqlResolver: RelationshipDao
) {


    @PreDestroy
    fun destroy() {
        val filename = env["DUMP_SQL"]

        if (filename != null) {
            println("Dump SQL: $filename")

            sqlResolver.dumpSql(filename)
        }
    }
}


/**
 * For debugging purposes, dump the DB to a file.
 */
fun RelationshipDao.dumpSql(filename: String) =
    jdbcTemplate.execute("SCRIPT to '${filename}'")
