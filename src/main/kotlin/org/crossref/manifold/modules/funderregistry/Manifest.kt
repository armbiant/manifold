package org.crossref.manifold.modules.funderregistry

import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Service


val manifest = Manifest(
    name = "funderRegistry",
    description = "Crossref Funder Registry",
)

@Service(value = "funderRegistry")
class Boot(
    @Autowired
    val modules: ModuleRegistrar
) : ApplicationRunner {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Throws(Exception::class)
    override fun run(args: ApplicationArguments) {
        modules.register(manifest)
    }
}