package org.crossref.manifold.modules.unixml

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.crossref.manifold.bulk.EnvelopeBatchArchiveWriter
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.modules.unixml.support.PARSE_PARALLELISM
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.crossref.manifold.modules.unixml.support.sendTarGzTo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File

object UniXMLSnapshotConverter {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun run(baseFile: File, outputFile: File) {
        val archive = EnvelopeBatchArchiveWriter(outputFile)

        val contentFilenameChannel = Channel<Pair<String, String>>(PARSE_PARALLELISM)
        val envelopeBatchChannel = Channel<Pair<String, Collection<EnvelopeBatch>>>(PARSE_PARALLELISM)

        runBlocking {

            val parseTasks = (0..PARSE_PARALLELISM).map<Int, Job> {
                launch(CoroutineName("parse-xml-$it") + Dispatchers.IO) {
                    logger.info("Start XML ingester on ${Thread.currentThread().name}")

                    for ((itemFilename, content) in contentFilenameChannel) {
                        try {

                            // Set bulk to true to indicate the context for the envelopes.
                            // See docs for [parseUniXmlToEnvelopes].
                            val envelopeBatches = parseUniXmlToEnvelopes(
                                itemFilename,
                                content,
                                bulk = true
                            )

                            envelopeBatchChannel.send(itemFilename to envelopeBatches)
                        } catch (exception: Exception) {
                            logger.error("error in file $itemFilename: ${exception.message}")
                        }
                    }

                }
            }

            val writeTask = launch(CoroutineName("write") + Dispatchers.IO) {
                try {
                    archive.use {
                        for ((filename, envelopeBatches) in envelopeBatchChannel) {
                            archive.add(filename, envelopeBatches)
                        }
                    }
                } catch (e: Exception) {
                    logger.error("Archive writing: ${e.message}")
                }
            }


            // Read the Zip file sequentially in this thread. Others will continue in the background.
            logger.info("Read TGZ...")

            sendTarGzTo(baseFile, contentFilenameChannel)

            logger.info("Wait for parsing")
            parseTasks.forEach { it.join() }
            envelopeBatchChannel.close()

            logger.info("Wait for writing...")
            writeTask.join()
            logger.info("Finished writing...")
        }


    }
}