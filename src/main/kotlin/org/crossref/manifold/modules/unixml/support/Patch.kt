package org.crossref.manifold.modules.unixml.support

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.itemtree.propertiesFromSimpleMap

/**
 * These functions patch or extract data that are ultimately derived from CRM-items.
 * This data is outside the metadata XML, so we need to be very mindful of using it.
 */
object CrmItems {

    /**
     * Retrieve the Member Item (i.e. that with a http://id.crossref.org/member/ identifier)
     * From anywhere in the tree, may be in the ancestors.
     */
    fun getPublisherMemberId(item: Item): List<Item> =
        item.rels.flatMap {
            // Find other Items in the 'publisher' role with relation to this one.
            if (it.relTyp == "publisher") {
                // Of which, find any identifier that says it's a member.
                it.obj.identifiers
                    .filter { i -> i.uri.startsWith("https://id.crossref.org/member/") }

                    // Don't take the whole 'publisher' object as it mixes up all kinds of unrelated stuff, such as
                    // member id, prefix, member name and publisher name.
                    // Instead, just create a new Item with just that Identifier.
                    .map { Item().withIdentifier((it)) }

            } else {
                emptyList()
            }
        } + item.rels.flatMap { getPublisherMemberId(it.obj) }


    /**
     * Remove any relationship of 'publisher' from the Item Tree.
     * This is because this data is derived from <crm-items> and we'll get it from another route.
     * @return item tree with Publisher item removed at any depth.
     */
    fun removePublisher(item: Item): Item =
        item.withRelationships(item.rels.mapNotNull {
            if (it.relTyp == "publisher") {
                null
            } else {
                it.withItem(removePublisher(it.obj))
            }
        })
}


/**
 * These functions patch or extract data from the XML member-supplied metadata.
 */
object Metadata {
    private val patchRemoveRelTypesToDrop = setOf(
        // these should be folded in as an identifier directly.
        "issn",
        "isbn",

        // these should be properties.
        "acronym",
        "location",
        "approved",
        "published",
        "update-policy",
        "abstract",
        "degree",
        "start",

        "published-print",
        "published-online",
        "deposited",
        "first-deposited",
        "content-created",
        "content-updated",
        "published-print",
        "published-online",
        "published-other",
        "title",

        // unsure
        "assertion",
        "cited-count",
    )

    /**
     * For an Item Tree, retrieve all landing page items, and the items the landing page relates to.
     * The purpose of each node is to be enough to make an assertion that links the Item and the Landing Page. So we
     * return empty Item objects, with simple Identifiers attached.
     * @return a set of pairs of (Content Item, Landing Page Item).
     */
    fun getDoiRegistrations(itemTree: Item): Set<Item> {
        // Set of pairs of content item (e.g. article) to landing page item.
        // The content item only needs its identifier, not the rest of its baggage.
        val itemLandingPages = mutableSetOf<Item>()

        // This recursive function looks at two levels. The `item` variable points to the current node, and then we look
        // at the items via its immediate relations.
        fun recurse(item: Item) {
            // If there's a resource resolution for this, it's a DOI.
            if (item.rels.any { it.relTyp == "resource-resolution" }) {
                itemLandingPages.add(Item().withIdentifier(item.identifiers.first()))
            }

            // And recurse down.
            for (rel in item.rels) {
                recurse(rel.obj)
            }
        }

        recurse(itemTree)

        return itemLandingPages
    }

    /**
     * Cayenne's item tree parser currently represents a few things as Items that really should be properties.
     * Recursively walk the tree, extracting those relationships, patching them into Properties by merging with existing
     * properties.
     *
     * This remodelling will be addressed in future versions of Cayenne, so this code should be removed when all named
     * relationships have been removed.
     */
    fun patchRemoveRelTypes(item: Item): Item {
        // Split the relationships into those that do and don't match the list of types to drop.
        val (toProps, keepRels) = item.rels.partition { patchRemoveRelTypesToDrop.contains(it.relTyp) }

        // Although Item Tree allows for multiple Property Statements (for different provenances), in this module we
        // expect to have at most one Property. Retrieve that, or create a blank one.
        val itemProps: Properties = item.properties.firstOrNull() ?: propertiesFromSimpleMap(emptyMap())

        // Convert relationship type into property name and value into property value.
        val relsAsProps = propertiesFromSimpleMap(toProps.map {
            Pair(it.relTyp, it.obj.properties.firstOrNull()?.values)
        }.toMap())

        // Then merge with existing properties on this Item.
        val combinedProps = itemProps.merge(relsAsProps)

        // Reconstruct the item, patching in the new combined Properties and the Relationships with the unwanted ones
        // removed.
        // Relationships are also the point of recursion in the tree, so recurse in applying this function to sub-trees.
        val newItem = item
            .withProperties(listOf(combinedProps))
            .withRelationships(keepRels.map { relationship ->
                relationship.withItem(
                    patchRemoveRelTypes(relationship.obj)
                )
            })

        return newItem
    }

    /**
     * Remove known ambiguities such as ISSNs and Supplementary IDs for now because they cause a lot of ambiguous
     * Identifiers.
     * This will be fixed in a future Item Tree parser.
     */
    fun removeKnownAmbiguities(item: Item): Item =
        item
            .withIdentifiers(item.identifiers.filterNot {
                it.uri.startsWith("http://id.crossref.org/issn/") or
                        it.uri.startsWith("http://id.crossref.org/supp/")
            })
            .withRelationships(item.rels.map { it.withItem(removeKnownAmbiguities(it.obj)) })
}


