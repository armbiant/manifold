package org.crossref.manifold.modules.unixml

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class XmlSingleIngester(
    private val itemGraph: ItemGraph,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)


    fun ingest(trace: String, content: String) {

        val envelopeBatches = parseUniXmlToEnvelopes(
            trace,
            content,
        )

        // Ingest all envelope batches
        envelopeBatches.forEach {
            logger.info("XML ${it.provenance.externalTrace}")
            itemGraph.ingest(it)
        }
    }
}