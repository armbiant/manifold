package org.crossref.manifold.modules.itemTree

import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.registries.AuthorityRegistry
import org.crossref.manifold.retrieval.itemtree.ItemFetchStrategy
import org.crossref.manifold.retrieval.itemtree.ItemTreeRetriever
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

/**
 * Note that these endpoints take a full Identifier URL as an endpoint.
 * We also accept the non-URL DOI form as special case, and redirect back to the same endpoint with the full-URL DOI.
 */
@RestController
class ItemTreeController(
    val itemTreeRetriever: ItemTreeRetriever,
    val itemResolver: Resolver,
    val authorityRegistry: AuthorityRegistry
) {
    /**
     * Get an Item Tree with the default perspective.
     */
    @GetMapping("/v2/item-tree/{*identifier}", produces = ["application/json"])
    fun getItemTree(@PathVariable identifier: String): Item = itemTree(ItemFetchStrategy.DEFAULT, identifier)

    /**
     * Get an Item Tree with the specified perspective.
     */
    @GetMapping("/v2/perspective/{perspective}/item-tree/{*identifier}", produces = ["application/json"])
    fun getPerspectiveItemTree(
        @PathVariable perspective: String,
        @PathVariable identifier: String
    ): Item = itemTree(perspective, identifier)

    private fun itemTree(
        perspective: String,
        identifierStr: String
    ): Item {
        // Need to trim the leading slash.
        val identifier = itemResolver.resolveRO(Identifier(identifierStr.substring(1)))
        if (identifier.pk == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")
        }

        val strategy = ItemFetchStrategy.fromPerspective(
            perspective,
            authorityRegistry.getAuthorityRootPks(itemResolver),
            authorityRegistry.getAuthorityPks(itemResolver),
            3
        )
        return itemTreeRetriever.get(identifier.pk, strategy)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")
    }
}
