package org.crossref.manifold.modules.itemTree

import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.HandlerMapping
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * When a URL path is supplied with a non-URL DOI, redirect back to the same path but with the full DOI URL.
 */
class ItemTreeInterceptor : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val pathVars = request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE) as Map<*, *>
        val maybeDoi = (pathVars.getOrDefault("identifier", "~") as String).substring(1)
        return if (maybeDoi.startsWith("10.")) {
            response.status = HttpServletResponse.SC_MOVED_PERMANENTLY
            response.setHeader("Location", "${request.requestURI.removeSuffix(maybeDoi)}https://doi.org/$maybeDoi")
            false
        } else {
            true
        }
    }
}
