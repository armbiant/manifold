package org.crossref.manifold.modules.graphviz

import org.crossref.manifold.itemgraph.RelationshipDao
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.stereotype.Service
import java.io.File
import javax.annotation.PreDestroy


/**
 * If the user supplies a "DUMP_GRAPHVIZ" argument, save the whole database to that filename in GraphViz DOT format before exit.
 */
@Service
class Dump {

    @Autowired
    private val env: Environment? = null

    @Autowired
    private val sqlResolver: RelationshipDao? = null

    @PreDestroy
    fun destroy() {
        val filename = env?.get("GRAPHVIZ_DUMP")

        if (filename != null) {
            println("Dump GraphViz: $filename")

            sqlResolver!!.dbToGraphViz(filename)
        }
    }
}

/**
 * For debugging purposes, dump the DB to a GraphViz DOT file.
 * This dumps all Items and Relationships, not Statements.
 */
fun RelationshipDao.dbToGraphViz(filename: String) {
    File(filename).bufferedWriter().use { out ->
        out.write("digraph relations {")

        out.write("rankdir = \"LR\"")

        jdbcTemplate.query("SELECT pk from item") { rs ->
            val pk = rs.getLong("pk")
            out.write("  I${pk} [shape=circle, label=\"Item ${pk}\"] \n")
        }

        jdbcTemplate.query("SELECT from_item_pk, to_item_pk, relationship_type.value as relationship_type_name from relationship join relationship_type on relationship_type_pk = relationship_type.pk") { rs ->
            out.write(
                "  I${rs.getLong("from_item_pk")} -> I${rs.getLong("to_item_pk")} [shape=circle, label=\"${
                    rs.getString(
                        "relationship_type_name"
                    )
                }\"] \n"
            )
        }

        jdbcTemplate.query("SELECT pk, value, item_pk from item_identifier") { rs ->
            val identifierPk = rs.getString("pk")
            out.write("  ID${identifierPk} [shape=rect, label=\"${rs.getString("value")}\"] \n")
            out.write("  ID${identifierPk} -> I${rs.getLong("item_pk")} \n")
        }

        out.write("}")
    }
}