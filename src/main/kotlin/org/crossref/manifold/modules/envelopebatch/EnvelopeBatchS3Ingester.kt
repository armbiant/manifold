package org.crossref.manifold.modules.envelopebatch

import com.fasterxml.jackson.databind.ObjectMapper
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.envelopebatch.Module.Companion.ENVELOPE_BATCH
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.crossref.messaging.aws.s3.S3EventNotification
import org.crossref.messaging.aws.sqs.SqsListener
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest

class EnvelopeBatchS3Ingester(
    private val itemGraph: ItemGraph,
    private val objectMapper: ObjectMapper,
    private val s3Client: S3Client
) {
    @SqsListener("$ENVELOPE_BATCH.$S3_NOTIFICATION_QUEUE")
    fun ingestS3Files(s3EventNotification: S3EventNotification) {
        for ((objectKey, bucketName) in s3EventNotification.records) {
            val s3Object = s3Client.getObject(
                GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(objectKey)
                    .build()
            )

            val content = s3Object.buffered().reader().readText()

            val envelopeBatches = objectMapper.readValue(content, Array<EnvelopeBatch>::class.java)

            envelopeBatches.forEach {
                itemGraph.ingest(it)
            }
        }
    }
}
