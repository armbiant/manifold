package org.crossref.manifold.modules.envelopebatch

import com.fasterxml.jackson.databind.ObjectMapper
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.services.s3.S3Client

@Configuration(Module.ENVELOPE_BATCH)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val ENVELOPE_BATCH = "envelope-batch"
        const val JSON_QUEUE = "json-queue"
    }

    init {
        moduleRegistrar.register(Manifest(ENVELOPE_BATCH, "Ingest envelope batches from SQS and S3"))
    }

    @Bean
    @ConditionalOnProperty(prefix = ENVELOPE_BATCH, name = [JSON_QUEUE])
    fun envelopeBatchS3Ingester(
        itemGraph: ItemGraph,
        objectMapper: ObjectMapper,
        s3Client: S3Client
    ): EnvelopeBatchS3Ingester = EnvelopeBatchS3Ingester(itemGraph, objectMapper, s3Client)

    @Bean
    @ConditionalOnProperty(prefix = ENVELOPE_BATCH, name = [JSON_QUEUE])
    fun envelopeBatchJsonMessageIngester(
        itemGraph: ItemGraph
    ): EnvelopeBatchJsonMessageIngester = EnvelopeBatchJsonMessageIngester(itemGraph)
}
