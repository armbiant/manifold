package org.crossref.manifold.itemtree

import org.slf4j.Logger
import org.slf4j.LoggerFactory

var logger: Logger = LoggerFactory.getLogger("org.crossref.manifold.itemtree.checks")


/**
 * Run a function against every Item in the Item Tree recursively. Return true if and only if the function returns true
 * in all cases for every item in the tree.
 */
fun allItems(item: Item, f: (item: Item) -> Boolean): Boolean = if (f(item)) {
    item.rels.all { allItems(it.obj, f) }
} else {
    false
}

/**
 * All ItemIdentifiers must have a tokenized representation.
 */
fun allIdentifiersTokenized(itemTree: Item): Boolean =
    allItems(itemTree) { item ->
        item.identifiers.all {
            if (it.tokenized == null) {
                val error = "ItemIdentifier $it had no tokenized representation."
                logger.error(error)
                false
            } else {
                true
            }
        }
    }


/**
 * All Items must have been resolved to Item PKs.
 * Note: This is different to ItemIdentifiers being resolved.
 */
fun allItemsResolved(itemTree: Item): Boolean =
    allItems(itemTree) { item ->
        if (item.pk == null) {
            // Don't log the whole item, it's recursive!
            val error = "Item $item with ${item.identifiers} was not resolved."
            logger.error(error)
            false
        } else {
            true
        }
    }


/**
 * All ItemIdentifiers must have been resolved to ItemIdentifier PKs.
 * Note: This is different to Items being resolved.
 */
fun allItemIdentifiersResolved(itemTree: Item): Boolean =
    allItems(itemTree) { item ->
        item.identifiers.all {
            if (it.pk == null) {
                val error = "ItemIdentifier $it had no resolved PK."
                logger.error(error)
                false
            } else {
                true
            }

        }
    }
