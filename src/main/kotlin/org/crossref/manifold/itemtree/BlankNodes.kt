package org.crossref.manifold.retrieval.view

import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import java.security.MessageDigest

/**
 * Hash an input string to construct a suffix for a Blank Node.
 * Algorithm isn't really important, though it uses SHA-1.
 */
fun hash(input: String): String {
    val bytes = MessageDigest
        .getInstance("SHA-1")
        .digest(input.toByteArray())

    return bytes.joinToString("") { "%02x".format(it) }
}

/**
 * For every Item without any ItemIdentifiers, add [blank node](https://en.wikipedia.org/wiki/Blank_node) ItemIdentifiers.
 * A Blank Node represents an anonymous resource within an RDF graph. At the time of ingestion, the extent of that RDF
 * graph is the Item Tree itself.
 *
 * However, when we translate that into the wider Item Graph, it is essential that Blank Node identifiers don't conflict
 * between Item Trees: The reason for using an Identifier is so that two Item Trees can talk about the same thing. If
 * there's no Identifier then we don't *know* that two Items are the same, even if they look identical (if we knew we'd
 * give them the same Identifier!).
 *
 * So we must create these using an algorithm that doesn't repeat the same Blank Node Identifier in different contexts.
 *
 * Another requirement is that the same Item Tree should produce the same Blank Node Identifiers repeatedly. Although it's
 * not a problem if they are new each time, it would create un-needed bloat.
 *
 * So, the algorithm takes a hash of Item's properties and relationships, in the context of the parent's identifiers.
 *
 * It is expected that individual modules apply this to item trees, as necessary, before sending to the Ingester.
 */
fun markBlankNodes(itemTree: Item, prefix: String): Item {
    fun recurse(item: Item, parentId: String, relType: String): Item =
        if (item.identifiers.isNotEmpty()) {
            // If there are any ItemIdentifiers for this Item, then no need to add a Blank Node.
            // Recurse into its child nodes and mark the context of this node, derived from its ItemIdentifiers.
            val thisId = (item.identifiers.map { it.uri }).joinToString(":")
            item.withRelationships(item.rels.map { rel -> rel.withItem(recurse(rel.obj, thisId, rel.relTyp)) })
        } else {
            // If there are no ItemIdentifiers for this Item, we need to add Blank Nodes!

            // Get this Item's properties enough to identify it.
            // Capture the parent's own ID and relationship type in the identity of this one.
            val self =
                parentId + ":" + relType + "+" + item.properties.toString() + ":" + item.rels.map { it.obj.identifiers }
            val blankNodeIdentifier = prefix + hash(self)

            // Attach the Blank Node identifier, and attach relationships which are the result of recursing into them.
            item
                .withIdentifier(Identifier(blankNodeIdentifier))
                .withRelationships(item.rels.map { rel ->
                    rel.withItem(
                        recurse(
                            rel.obj,
                            blankNodeIdentifier,
                            rel.relTyp
                        )
                    )
                })
        }

    return recurse(itemTree, "ROOT", "ROOT")
}
