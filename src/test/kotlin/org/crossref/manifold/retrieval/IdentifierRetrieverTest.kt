package org.crossref.manifold.retrieval

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Tokenized
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class IdentifierRetrieverTest {
    // Item 1 is an Article which has three Identifiers.
    private val articleDoi = "https://doi.org/10.5555/12345678"
    private val articleDoiTokenized = Tokenized("doi", "10.5555/12345678")
    private val articleDoiIdentifier = Identifier(uri = articleDoi, tokenized = articleDoiTokenized, pk = 1)

    private val articleItem = "http://id.crossref.org/item/44"
    private val articleItemTokenized = Tokenized("cr-item", "44")
    private val articleItemIdentifier = Identifier(uri = articleItem, tokenized = articleItemTokenized, pk = 1)

    private val articlePmid = "https://pubmed.ncbi.nlm.nih.gov/34906956/"
    private val articlePmidTokenized = Tokenized("pmid", "34906956")
    private val articlePmidIdentifier = Identifier(uri = articlePmid, tokenized = articlePmidTokenized, pk = 1)

    // Item 2 is a Member which has two Identifiers.
    private val memberDoi = "https://doi.org/10.5555/999999999"
    private val memberDoiTokenized = Tokenized("doi", "10.5555/999999999")
    private val memberDoiIdentifier = Identifier(uri = memberDoi, tokenized = memberDoiTokenized, pk = 2)

    private val memberMemberId = "http://id.crossref.org/member/999999999"
    private val memberMemberIdTokenized = Tokenized("cr-member", "999999999")
    private val memberMemberIdIdentifier = Identifier(uri = memberMemberId, tokenized = memberMemberIdTokenized, pk = 2)

    private val itemDao = mockk<ItemDao>()
    private val relationshipDao = mockk<RelationshipDao>()
    private val identifierTypeRegistry = mockk<IdentifierTypeRegistry>()

    @BeforeEach
    fun setup() {
        // Can retrieve Item 1's identifiers.
        every {
            itemDao.fetchItemIdentifierPairsForItemPks(setOf(1))
        } returns listOf(
            Pair(1, articleDoiTokenized),
            Pair(1, articleItemTokenized),
            Pair(1, articlePmidTokenized)
        )

        // Can retrieve Item 2's identifiers.
        every {
            itemDao.fetchItemIdentifierPairsForItemPks(setOf(2))
        } returns listOf(
            Pair(2, memberDoiTokenized),
            Pair(2, memberMemberIdTokenized)
        )

        // Empty set of Identifiers returns empty set of results.
        every {
            itemDao.fetchItemIdentifierPairsForItemPks(setOf<Long>())
        } returns listOf()

        // IdentifierTypeRegistry can construct URI for each of the mentioned structured Identifiers.
        every {
            identifierTypeRegistry.tokenizedToIdentifier(articleDoiTokenized, 1)
        } returns articleDoiIdentifier

        every {
            identifierTypeRegistry.tokenizedToIdentifier(articleItemTokenized, 1)
        } returns articleItemIdentifier

        every {
            identifierTypeRegistry.tokenizedToIdentifier(articlePmidTokenized, 1)
        } returns articlePmidIdentifier

        every {
            identifierTypeRegistry.tokenizedToIdentifier(memberDoiTokenized, 2)
        } returns memberDoiIdentifier

        every {
            identifierTypeRegistry.tokenizedToIdentifier(memberMemberIdTokenized, 2)
        } returns memberMemberIdIdentifier
    }

    @Test
    fun `get should return none when cache is empty`() {
        val cache = IdentifierRetriever(itemDao, relationshipDao, identifierTypeRegistry)
        assertNull(cache.get(1), "When cache is empty should return none for a request.")
    }

    @Test
    fun `get should return the result once cache has been primed with a given item`() {
        val cache = IdentifierRetriever(itemDao, relationshipDao, identifierTypeRegistry)

        // Pre-check - they're not in the cache before fetching.
        assertNull(cache.get(1), "Empty object should not return a result when empty.")
        assertNull(cache.get(2), "Empty object should not return a result when empty.")

        // Ask the cache to prime with both IDs. Usually this is called repeatedly with some duplicates.
        cache.prime(setOf(1))
        cache.prime(setOf(1, 2))

        // Once primed they can be retrieved.
        assertEquals(
            setOf(articleDoiIdentifier, articlePmidIdentifier, articleItemIdentifier),
            cache.get(1)!!.toSet()
        )
        assertEquals(
            setOf(memberDoiIdentifier, memberMemberIdIdentifier),
            cache.get(2)!!.toSet()
        )
    }

    /**
     * Ensure that the cache for X isn't evicted if we retrieve Y.
     */
    @Test
    fun `repeated calls to prime should not remove items from cache`() {
        val cache = IdentifierRetriever(itemDao, relationshipDao, identifierTypeRegistry)

        // Pre-check - they're not in the cache before fetching.
        assertNull(cache.get(1), "Empty object should not return a result when empty.")

        // Retrieve first time will retrieve from Item Graph.
        cache.prime(setOf(1))
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(1)) }

        // Now retrieve something different - item 2.
        cache.prime(setOf(2))
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(2)) }

        // Now call prime with item 1 again - it should still be in cache so the Item Graph DAO isn't called again.
        cache.prime(setOf(1))
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(1)) }
    }

    @Test
    fun `prime should not retrieve already primed pks`() {
        val cache = IdentifierRetriever(itemDao, relationshipDao, identifierTypeRegistry)

        // Fetch first time.
        cache.prime(setOf(1))

        // The Cache should have called out to the DAO to get that item PK.
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(1)) }

        // Fetch again.
        cache.prime(setOf(1))

        // The DAO should not have been called again for that item.
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(1)) }

        // Fetch again.
        cache.prime(setOf(1, 2))

        // The DAO should not have been called again for the known item, but is called for an unknown one.
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(1)) }
        verify(exactly = 1) { itemDao.fetchItemIdentifierPairsForItemPks(setOf(2)) }
    }
}