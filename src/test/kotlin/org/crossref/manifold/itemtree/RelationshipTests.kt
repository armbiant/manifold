package org.crossref.manifold.itemtree

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class RelationshipTest {

    @Test
    fun withItem() {
        val origItem = Item(pk = 500)
        val relationship = Relationship(relTyp = "cites", origItem)

        assertEquals(relationship.obj, origItem)

        val newItem = Item(pk = 999)
        val withItem = relationship.withItem(newItem)

        assertEquals(withItem.obj, newItem, "withItem should return new Relationship object with new Item")
        assertEquals(
            relationship.obj,
            origItem,
            "withItem should not change the Item in the original Relationship object"
        )
    }
}