package org.crossref.manifold.itemtree

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class ItemTest {
    @Test
    fun withPk() {
        val item = Item()

        assertEquals(item.pk, null, "Should be created with no Pk")

        val withPk = item.withPk(500)
        assertEquals(500, withPk.pk, "withPk should attach PK")
        assertEquals(null, item.pk, "withPk should not affect original")
    }

    @Test
    fun blankNode() {
        val item = Item()
        assertTrue(item.identifiers.isEmpty())
        assertEquals(true, item.hasNoIdentifiers(), "Item with no Identifiers is considered a blank node.")
    }
}
