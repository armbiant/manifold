package org.crossref.manifold.itemtree

import org.crossref.manifold.itemgraph.RelationshipStatement
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.OffsetDateTime
import java.time.ZoneOffset

val NOW: OffsetDateTime = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC)!!

internal class ItemTreeTest {
    // This is a tree with some unknown relationship types that haven't been resolved.
    // The unknown ones are considered unresolved because they don't have PKs.
    private val partiallyUnresolvedRelationships =
        Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.5555/12345678"))).withRelationships(
            listOf(
                Relationship(
                    "citation",
                    Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.6666/8764321"))).withRelationships(
                        listOf(
                            Relationship(
                                "UNKNOWN_ABC",
                                Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.0000/0000")))
                                    .withRelationships(listOf(Relationship("citation", Item())))
                            )
                        )
                    )
                ), Relationship(
                    "citation",
                    Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.7777/24242424")))
                ), Relationship(
                    "funder",
                    Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.8888/98989898")))
                ), Relationship(
                    // This relationship should be returned as it's unresolved.
                    "UNKNOWN_XYZ",
                    Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.9999/9999"))).withRelationships(
                        listOf(
                            Relationship(
                                "citation", Item()
                            )
                        )
                    )
                )
            )
        )

    // A tree of fully resolved Relationships and Items.
    private val fullyResolvedTree =
        Item().withPk(100).withIdentifiers(listOf(Identifier("http://dx.doi.org/10.5555/12345678").withPk(1000)))
            .withRelationships(
                listOf(
                    Relationship(
                        "citation",
                        Item().withPk(101)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.6666/8764321").withPk(1001)))
                            .withRelationships(
                                listOf(
                                    Relationship(
                                        "discusses",
                                        Item().withPk(102)
                                            .withIdentifiers(
                                                listOf(
                                                    Identifier("http://dx.doi.org/10.0000/0000").withPk(
                                                        1002
                                                    )
                                                )
                                            )
                                            .withRelationships(listOf(Relationship("citation", Item().withPk(103))))
                                    )
                                )
                            )
                    ), Relationship(
                        "citation",
                        Item().withPk(104)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.7777/24242424").withPk(1003)))
                    ), Relationship(
                        "funder",
                        Item().withPk(105)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.8888/98989898").withPk(1004)))
                    ), Relationship(
                        // This relationship should be returned as it's unresolved.
                        "funder",
                        Item().withPk(106)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.9999/9999").withPk(1005)))
                            .withRelationships(
                                listOf(
                                    Relationship(
                                        "citation", Item().withPk(107)
                                    )
                                )
                            )
                    )
                )
            )

    private val partiallyResolvedIdentifiers =
        Item().withPk(100).withIdentifiers(listOf(Identifier("http://dx.doi.org/10.5555/12345678").withPk(1000)))
            .withRelationships(
                listOf(
                    Relationship(
                        "citation",
                        Item().withPk(101)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.6666/8764321", null)))
                            .withRelationships(
                                listOf(
                                    Relationship(
                                        "discusses",
                                        Item().withPk(102)
                                            .withIdentifiers(
                                                listOf(
                                                    Identifier("http://dx.doi.org/10.0000/0000").withPk(
                                                        1002
                                                    )
                                                )
                                            )
                                            .withRelationships(listOf(Relationship("citation", Item().withPk(103))))
                                    )
                                )
                            )
                    ), Relationship(
                        "citation",
                        Item().withPk(104)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.7777/24242424", null)))
                    ), Relationship(
                        "funder",
                        Item().withPk(105)
                            .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.8888/98989898").withPk(1004)))
                    ), Relationship(
                        // This relationship should be returned as it's unresolved.
                        "funder",
                        Item().withPk(106).withIdentifiers(listOf(Identifier("http://dx.doi.org/10.9999/9999", null)))
                            .withRelationships(
                                listOf(
                                    Relationship(
                                        "citation", Item().withPk(107)
                                    )
                                )
                            )
                    )
                )
            )

    @Test
    fun relationshipTriplePksTest() {

        assertEquals(
            setOf(
                RelationshipStatement(100, "citation", 101, 900, true, NOW),
                RelationshipStatement(101, "discusses", 102, 900, true, NOW),
                RelationshipStatement(102, "citation", 103, 900, true, NOW),
                RelationshipStatement(100, "citation", 104, 900, true, NOW),
                RelationshipStatement(100, "funder", 105, 900, true, NOW),
                RelationshipStatement(100, "funder", 106, 900, true, NOW),
                RelationshipStatement(106, "citation", 107, 900, true, NOW),

                ),
            toRelationshipStatements(fullyResolvedTree, 900, NOW),
            "Set of relationship triples should be extracted from the entire tree based on their Item Pks and RelationshipType Pks."
        )
    }

    @Test
    fun allIdentifiersTest() {
        assertEquals(
            setOf(
                "http://dx.doi.org/10.5555/12345678",
                "http://dx.doi.org/10.6666/8764321",
                "http://dx.doi.org/10.0000/0000",
                "http://dx.doi.org/10.7777/24242424",
                "http://dx.doi.org/10.8888/98989898",
                "http://dx.doi.org/10.9999/9999"
            ),
            allIdentifiers(partiallyUnresolvedRelationships),
            "All identifiers should be retrieved from all levels of the tree, whether resolved or not."
        )
    }

    @Test
    fun unresolvedIdentifiersTest() {
        assertTrue(
            setOf(
                Identifier("http://dx.doi.org/10.6666/8764321", null),
                Identifier("http://dx.doi.org/10.7777/24242424", null),
                Identifier("http://dx.doi.org/10.9999/9999", null)
            ).toList().equals(unresolvedIdentifiers(partiallyResolvedIdentifiers).toList()),
            "Those identifiers with null PKs should be returned."
        )
    }

    @Test
    fun getAllRelTypesTest() {
        assertEquals(
            setOf("UNKNOWN_ABC", "UNKNOWN_XYZ", "citation", "funder"),
            getAllRelTypes(partiallyUnresolvedRelationships),
            "Should retrieve distinct set of all relationship types, at all levels of the tree."
        )
    }

    @Test
    fun countRelationshipsTest() {
        val single = Item()

        assertEquals(countRelationships(single), 0, "Single item has no relationships")

        // Item cites book has an author.
        val three = Item().withRelationships(
            listOf(
                Relationship(
                    "cites", Item().withRelationships(listOf(Relationship("author", Item())))
                )
            )
        )

        assertEquals(2, countRelationships(three), "Items three deep should have two Relationships")

        // Item cites book has three authors.
        val lotsOfAuthors = Item().withRelationships(
            listOf(
                Relationship(
                    "cites", Item().withRelationships(
                        listOf(
                            Relationship("author", Item()),
                            Relationship("author", Item()),
                            Relationship("author", Item())
                        )
                    )
                )
            )
        )

        assertEquals(4, countRelationships(lotsOfAuthors), "Multiple relationships on one Item should be counted.")
    }

    @Test
    fun countItemsTest() {
        val single = Item()

        assertEquals(countItems(single), 1, "Single node should be one")

        // Item cites book has an author.
        val three = Item().withRelationships(
            listOf(
                Relationship(
                    "cites", Item().withRelationships(listOf(Relationship("author", Item())))
                )
            )
        )

        assertEquals(3, countItems(three), "Three deep should have three Items")
    }
}