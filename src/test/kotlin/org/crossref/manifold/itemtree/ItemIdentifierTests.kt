package org.crossref.manifold.itemtree

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class IdentifierTest {
    @Test
    fun withPk() {
        val identifier = Identifier("https://dx.doi.org/10.5555/1345678")

        assertEquals(identifier.pk, null, "Should be created with no Pk")

        val withPk = identifier.withPk(50)
        assertEquals(50, withPk.pk, "withPk should attach PK")
        assertEquals(null, identifier.pk, "withPk should not affect original")
    }
}
