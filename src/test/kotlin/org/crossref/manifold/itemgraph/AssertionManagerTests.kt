package org.crossref.manifold.itemgraph

import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.itemtree.jsonObjectFromMap
import org.crossref.manifold.modules.consts.Items
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.time.OffsetDateTime
import java.time.ZoneOffset

val NOW: OffsetDateTime = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC)!!

const val CROSSREF_ITEM_ID: Long = 999

// A tree of fully resolved Relationships and Items.
private val fullyResolvedTree =
    Item().withPk(100).withIdentifiers(listOf(Identifier("http://dx.doi.org/10.5555/12345678").withPk(1000)))
        .withRelationships(
            listOf(
                Relationship(
                    "citation",
                    Item().withPk(101)
                        .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.6666/8764321").withPk(1001)))
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "discusses",
                                    Item().withPk(102)
                                        .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.0000/0000").withPk(1002)))
                                        .withRelationships(listOf(Relationship("citation", Item().withPk(103))))
                                )
                            )
                        ).withPropertiesFromMap(
                            mapOf(
                                "title" to "this is the title",
                                "abstract" to "this is the abstract",
                                "cited-by-count" to 8
                            )
                        )
                ), Relationship(
                    "citation",
                    Item()
                        .withPk(104)
                        .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.7777/24242424").withPk(1003)))
                        .withPropertiesFromMap(mapOf("sequence-number" to 5))
                ), Relationship(
                    "funder",
                    Item()
                        .withPk(105)
                        .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.8888/98989898").withPk(1004)))
                        .withPropertiesFromMap(mapOf("sequence-number" to 6))
                ), Relationship(
                    // This relationship should be returned as it's unresolved.
                    "funder",
                    Item().withPk(106)
                        .withIdentifiers(listOf(Identifier("http://dx.doi.org/10.9999/9999").withPk(1005)))
                        .withRelationships(
                            listOf(
                                Relationship(
                                    "citation", Item().withPk(107)
                                )
                            )
                        )
                        .withPropertiesFromMap(mapOf("sequence-number" to 7))
                )
            )
        )

// The relationship Pks that correspond to the above tree.
private val fullyResolvedTreeRelationshipStatements = setOf(
    RelationshipStatement(100, "citation", 101, CROSSREF_ITEM_ID, true, NOW),
    RelationshipStatement(101, "discusses", 102, CROSSREF_ITEM_ID, true, NOW),
    RelationshipStatement(102, "citation", 103, CROSSREF_ITEM_ID, true, NOW),
    RelationshipStatement(100, "citation", 104, CROSSREF_ITEM_ID, true, NOW),
    RelationshipStatement(100, "funder", 105, CROSSREF_ITEM_ID, true, NOW),
    RelationshipStatement(100, "funder", 106, CROSSREF_ITEM_ID, true, NOW),
    RelationshipStatement(106, "citation", 107, CROSSREF_ITEM_ID, true, NOW)
)

// The set of 'from' PKs from the above triples.
private val relFromItemPks = setOf<Long>(100, 101, 102, 106)

private val fullyResolvedPropertyStatements = setOf(
    PropertyStatement(
        itemPk = 101,
        values = jsonObjectFromMap(
            mapOf(
                "title" to "this is the title",
                "abstract" to "this is the abstract",
                "cited-by-count" to 8
            )
        ),
        assertingPartyPk = 999,
        assertedAt = NOW,
        pk = null
    ),
    PropertyStatement(
        itemPk = 104,
        values = jsonObjectFromMap(mapOf("sequence-number" to 5)),
        assertingPartyPk = 999,
        assertedAt = NOW,
        pk = null
    ),
    PropertyStatement(
        itemPk = 105,
        values = jsonObjectFromMap(mapOf("sequence-number" to 6)),
        assertingPartyPk = 999,
        assertedAt = NOW,
        pk = null
    ),
    PropertyStatement(
        itemPk = 106,
        values = jsonObjectFromMap(mapOf("sequence-number" to 7)),
        assertingPartyPk = 999,
        assertedAt = NOW,
        pk = null
    )
)


// The set of 'from' PKs from the above triples.
private val propFromItemPks = setOf<Long>(101, 104, 105, 106)

/** The behaviour of toRelationshipStatementsByStrategy and toPropertyStatementsByStrategy are very closely linked.
 * They are called at the same time and should both implement the [MergeStrategy] semantics as they apply to properties
 * and relationships respectively. Therefore, they are tested here on the same inputs.
 */
@ExtendWith(MockKExtension::class)
class ItemTreeStatementMapperTest {

    private val relationshipDao = mockk<RelationshipDao>()
    private val propertyDao = mockk<PropertyDao>()

    private val itemTreeStatementMapper = ItemTreeStatementMapper(relationshipDao, propertyDao)

    private val resolvedParty =
        Item().withPk(CROSSREF_ITEM_ID).withIdentifier(Identifier(Items.CROSSREF_AUTHORITY).withPk(1))

    @Test
    fun `calling toRelationshipStatementsByStrategy with MergeStrategy NONE should assert nothing`() {
        val assertion = ItemTreeAssertion(
            NOW, MergeStrategy.NONE, resolvedParty
        )
        val envelope = Envelope(listOf(fullyResolvedTree), assertion)

        val relationshipStatements =
            itemTreeStatementMapper.toRelationshipStatementsByStrategy(envelope, fullyResolvedTree)

        assertTrue(relationshipStatements.isEmpty(), "NONE merge strategy should produce no RelationshipStatements")
    }

    @Test
    fun `calling toPropertyStatementsByStrategy with MergeStrategy NONE should assert nothing`() {
        val assertion = ItemTreeAssertion(
            NOW, MergeStrategy.NONE, resolvedParty
        )
        val envelope = Envelope(listOf(fullyResolvedTree), assertion)

        val propertyStatements = itemTreeStatementMapper.toPropertyStatementsByStrategy(envelope, fullyResolvedTree)

        assertTrue(propertyStatements.isEmpty(), "NONE merge strategy should produce no PropertyStatements")
    }

    @Test
    fun `calling toRelationshipStatementsByStrategy with MergeStrategy NAIVE should assert all relationships`() {
        val assertion = ItemTreeAssertion(
            NOW, MergeStrategy.NAIVE, resolvedParty
        )
        val envelope = Envelope(listOf(fullyResolvedTree), assertion)

        val propertyStatements = itemTreeStatementMapper.toPropertyStatementsByStrategy(envelope, fullyResolvedTree)

        assertEquals(fullyResolvedPropertyStatements, propertyStatements)
    }

    @Test
    fun `calling toPropertyStatementsByStrategy with MergeStrategy NAIVE should assert all properties`() {
        val assertion = ItemTreeAssertion(
            NOW, MergeStrategy.NAIVE, resolvedParty
        )
        val envelope = Envelope(listOf(fullyResolvedTree), assertion)

        val relationshipStatements =
            itemTreeStatementMapper.toRelationshipStatementsByStrategy(envelope, fullyResolvedTree)

        assertEquals(fullyResolvedTreeRelationshipStatements, relationshipStatements)
    }

    @Test
    fun `calling toRelationshipStatementsByStrategy with MergeStrategy UNION should assert all triples except those that the resolver said were previously asserted`() {
        val assertion = ItemTreeAssertion(
            NOW, MergeStrategy.UNION_CLOSED, resolvedParty
        )
        val envelope = Envelope(listOf(fullyResolvedTree), assertion)

        // A randomly chosen couple of assertions that could have been previously asserted.
        // Two of these overlap with the above set of assertions, one of them is completely different.
        val previouslyAssertedRelationshipPks = setOf(
            RelationshipStatement(100, "citation", 101, CROSSREF_ITEM_ID, true, NOW),
            RelationshipStatement(101, "discusses", 102, CROSSREF_ITEM_ID, true, NOW),

            // This one is relevant because of it's 'from' id, but isn't mentioned in the item tree being asserted.
            RelationshipStatement(101, "discusses", 9999, CROSSREF_ITEM_ID, true, NOW)
        )

        // The resolver returns that three relationships were there before, of which two are contained in the new set.
        every {
            relationshipDao.getCurrentSubjRelationshipStatements(listOf(CROSSREF_ITEM_ID), relFromItemPks)
        } returns previouslyAssertedRelationshipPks

        val relationshipStatements =
            itemTreeStatementMapper.toRelationshipStatementsByStrategy(envelope, fullyResolvedTree)

        // The above set of relationships, less the previously asserted ones.
        val newRelationships = setOf(
            RelationshipStatement(102, "citation", 103, CROSSREF_ITEM_ID, true, NOW),
            RelationshipStatement(100, "citation", 104, CROSSREF_ITEM_ID, true, NOW),
            RelationshipStatement(100, "funder", 105, CROSSREF_ITEM_ID, true, NOW),
            RelationshipStatement(100, "funder", 106, CROSSREF_ITEM_ID, true, NOW),
            RelationshipStatement(106, "citation", 107, CROSSREF_ITEM_ID, true, NOW)
        )

        assertEquals(
            newRelationships,
            relationshipStatements,
        )
    }

    @Test
    fun `calling toPropertyStatementsByStrategy with MergeStrategy UNION should assert all triples except those that the resolver said were previously asserted`() {
        val assertion = ItemTreeAssertion(
            NOW, MergeStrategy.UNION_CLOSED, resolvedParty
        )
        val envelope = Envelope(listOf(fullyResolvedTree), assertion)

        // Here's two Property Statements.
        // The first one exercises all aspects of union. The second one is identical to the one being asserted.
        val previouslyAssertedPropertyStatements = setOf(
            PropertyStatement(
                itemPk = 101, values = jsonObjectFromMap(
                    mapOf(
                        // This is being re-asserted as identical.
                        "title" to "this is the title",

                        // This was here before, is not being re-asserted. But we expect to keep it because this is a union operation.
                        "sequence-number" to 8888,

                        // This was 5, but we expect it to be updated to be 8.
                        "cited-by-count" to 5
                    )
                ), assertingPartyPk = 999, assertedAt = NOW, pk = null
            ),

            PropertyStatement(
                itemPk = 104,
                values = jsonObjectFromMap(mapOf("sequence-number" to 5)),
                assertingPartyPk = 999,
                assertedAt = NOW,
                pk = null
            ),
        )

        every {
            propertyDao.getCurrentSubjPropertyStatements(listOf(CROSSREF_ITEM_ID), propFromItemPks)
        } returns previouslyAssertedPropertyStatements

        val propertyStatements = itemTreeStatementMapper.toPropertyStatementsByStrategy(envelope, fullyResolvedTree)

        // One specified Property statement (item 101) changed (though within it the fields had various changes), so it should be re-asserted.
        // One Property statement (item 104) didn't change from what was there before, so they **should not** be re-asserted.
        // The other two (item 105, 106) weren't there before so are being asserted for the first time.
        val newProperties = setOf(
            PropertyStatement(
                itemPk = 101, values = jsonObjectFromMap(
                    mapOf(
                        // This was here before and is unchanged.
                        "title" to "this is the title",

                        // This was here before, is not being added, but should be kept.
                        "sequence-number" to 8888,

                        // This was here before and is being changed.
                        "cited-by-count" to 8,

                        // This was not here before, but is being added.
                        "abstract" to "this is the abstract"
                    )
                ), assertingPartyPk = 999, assertedAt = NOW, pk = null
            ),

            PropertyStatement(
                itemPk = 105,
                values = jsonObjectFromMap(mapOf("sequence-number" to 6)),
                assertingPartyPk = 999,
                assertedAt = NOW,
                pk = null
            ),
            PropertyStatement(
                itemPk = 106,
                values = jsonObjectFromMap(mapOf("sequence-number" to 7)),
                assertingPartyPk = 999,
                assertedAt = NOW,
                pk = null
            )
        )

        assertEquals(
            newProperties.toSet(),
            propertyStatements.toSet(),
        )
    }
}