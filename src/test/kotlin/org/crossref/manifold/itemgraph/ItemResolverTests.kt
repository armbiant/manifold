package org.crossref.manifold.itemgraph

import io.mockk.clearMocks
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.itemtree.Tokenized
import org.crossref.manifold.registries.IdentifierTypeRegistry
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith


/** Test for read-write resolution.
 */
@ExtendWith(MockKExtension::class)
internal class ItemResolverTestRW {
    private val dao = mockk<RelationshipDao>()


    @BeforeEach
    fun reset() {
        clearMocks(dao)
    }

    /**
     * Where an Item Identifier in an Item Tree has already been resolved prior (e.g. through read-write resolution) the Read-Write resolver should not request those Item Identifiers again from the DAO.
     * This makes sure that there's no duplicate work, meaning that resolve read-write does truly provide an optimization step prior to this.
     */
    @Test
    fun `resolveRW should not request already resolved items`() {

    }
}

/** Tests for read-only mode resolution.
 */
@ExtendWith(MockKExtension::class)
internal class ResolverROTests {
    private val relationshipDao = mockk<RelationshipDao>()
    private val itemDao = mockk<ItemDao>()
    private val identifierTypeRegistry = mockk<IdentifierTypeRegistry>()

    private val resolver = Resolver(relationshipDao, itemDao, identifierTypeRegistry)

    @BeforeEach
    fun reset() {
        clearMocks(relationshipDao)
    }

    /**
     * resolveReadOnly should gather every unknown ItemIdentifier and ask the DAO about it.
     * Where it does know about it, it should resolve those Items and ItemIdentifiers in the tree.
     * Where not, it should pass Items and ItemIdentifiers through unchanged.
     */
    @Test
    fun `resolveReadOnly should resolve Item Identifiers where ItemGraphDao knows about them`() {
        // This is missing all Item and ItemIdentifier PKs.
        val unresolved =
            Item()
                .withIdentifiers(
                    listOf(
                        Identifier(
                            "http://dx.doi.org/10.5555/12345678",
                            tokenized = Tokenized("doi", "10.5555/12345678")
                        )
                    )
                )
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item()
                                .withIdentifiers(
                                    listOf(
                                        Identifier(
                                            "http://dx.doi.org/10.6666/8764321",
                                            tokenized = Tokenized("doi", "10.6666/8764321")
                                        )
                                    )
                                )
                                .withRelationships(
                                    listOf(
                                        Relationship(
                                            "discusses",
                                            Item()
                                                .withIdentifiers(
                                                    listOf(
                                                        Identifier(
                                                            "http://dx.doi.org/10.0000/0000",
                                                            tokenized = Tokenized("doi", "10.0000/0000")
                                                        )
                                                    )
                                                )
                                                .withRelationships(
                                                    listOf(
                                                        Relationship(
                                                            "citation",
                                                            Item()
                                                        )
                                                    )
                                                )
                                        )
                                    )
                                )
                        )
                    )
                )

        // This DAO doesn't know about all Identifiers in the Item Tree.
        every {
            itemDao.getIdentifierMappingsRO(any())
        } returns mapOf(
            Tokenized("doi", "10.5555/12345678") to IdentifierPkItemPk(1000, 100),
            Tokenized("doi", "10.6666/8764321") to IdentifierPkItemPk(1001, 101)
        )

        // We therefore expect every Item to be resolved to its Item PK and every Item Identifier to be resolved to its PK where we did know.
        // And those we didn't remain unresolved.
        // Also, relationship types are untouched.
        val expected =
            Item().withPk(100)
                .withIdentifiers(
                    listOf(
                        Identifier(
                            "http://dx.doi.org/10.5555/12345678",
                            tokenized = Tokenized("doi", "10.5555/12345678")
                        ).withPk(1000)
                    )
                )
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withPk(101).withIdentifiers(
                                listOf(
                                    Identifier(
                                        "http://dx.doi.org/10.6666/8764321",
                                        tokenized = Tokenized("doi", "10.6666/8764321")
                                    ).withPk(
                                        1001
                                    )
                                )
                            )
                                .withRelationships(
                                    listOf(
                                        Relationship(
                                            "discusses",
                                            Item()
                                                .withIdentifiers(
                                                    listOf(
                                                        Identifier(
                                                            "http://dx.doi.org/10.0000/0000",
                                                            tokenized = Tokenized("doi", "10.0000/0000"),
                                                            pk = null
                                                        )
                                                    )
                                                )
                                                .withRelationships(
                                                    listOf(
                                                        Relationship(
                                                            "citation",
                                                            Item()
                                                        )
                                                    )
                                                )
                                        )
                                    )
                                )
                        )
                    )
                )

        val result = resolver.resolveRO(unresolved)

        assertEquals(expected, result, "Expected Item Tree resolved for known ItemIdentifiers.")

        // Resolver must have called it for every identifier mentioned in the Item Tree.
        verify {
            itemDao.getIdentifierMappingsRO(
                listOf(
                    Tokenized("doi", "10.5555/12345678"),
                    Tokenized("doi", "10.6666/8764321"),
                    Tokenized("doi", "10.0000/0000")
                )
            )
        }
    }

    /**
     * Where an Item Identifier in an Item Tree has already been resolved prior (e.g. through a prior run) the Read-Only resolver should not request those Item Identifiers again.
     * This makes sure that we can run this repeatedly on the same tree and not repeat (or lose) any work.
     */
    @Test
    fun `resolveReadOnly should not request already resolved items`() {
        // This has some resolved Items and Identifiers, but is missing the Item for "http://dx.doi.org/10.0000/0000".
        val input = Item().withPk(100)
            .withIdentifiers(
                listOf(
                    Identifier(
                        "http://dx.doi.org/10.5555/12345678",
                        tokenized = Tokenized("doi", "10.5555/12345678")
                    ).withPk(1000)
                )
            )
            .withRelationships(
                listOf(
                    Relationship(
                        "citation",
                        Item().withPk(101).withIdentifiers(
                            listOf(
                                Identifier(
                                    "http://dx.doi.org/10.6666/8764321", tokenized = Tokenized("doi", "10.6666/8764321")
                                ).withPk(
                                    1001
                                )
                            )
                        )
                            .withRelationships(
                                listOf(
                                    Relationship(
                                        "discusses",
                                        Item()
                                            .withIdentifiers(
                                                listOf(
                                                    Identifier(
                                                        "http://dx.doi.org/10.0000/0000",
                                                        pk = null,
                                                        tokenized = Tokenized("doi", "10.0000/0000")
                                                    )
                                                )
                                            )
                                            .withRelationships(
                                                listOf(
                                                    Relationship(
                                                        "citation",
                                                        Item()
                                                    )
                                                )
                                            )
                                    )
                                )
                            )
                    )
                )
            )

        // This DAO doesn't know about any Identifiers, because that's not relevant for the test.
        every {
            itemDao.getIdentifierMappingsRO(any())
        } returns mapOf()

        val result = resolver.resolveRO(input)

        assertEquals(
            input,
            result,
            "When the DAO doesn't know about any identifiers, the input should be unchanged."
        )

        // Ensure that the DAO invoked only with the item identifier that wasn't known about.
        verify {
            itemDao.getIdentifierMappingsRO(listOf(Tokenized("doi", "10.0000/0000")))
        }

    }

    /**
     * If an Item has multiple ItemIdentifiers, but they all point to the same Item, that Item should be resolved.
     */
    @Test
    fun `resolveReadOnly should resolve Items where there are multiple ItemIdentifiers in agreement`() {

        // The root has a single identifier, but the next one has two ItemIdentifiers.
        val input =
            Item()
                .withIdentifiers(
                    listOf(
                        Identifier(
                            "http://dx.doi.org/10.5555/12345678",
                            tokenized = Tokenized("doi", "10.5555/12345678")
                        )
                    )
                )
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withIdentifiers(
                                listOf(
                                    Identifier(
                                        "http://dx.doi.org/10.6666/8764321",
                                        tokenized = Tokenized("doi", "10.6666/8764321")
                                    ),
                                    Identifier(
                                        "http://dx.doi.org/10.7777/567567567",
                                        tokenized = Tokenized("doi", "10.7777/567567567")
                                    )
                                )
                            )
                        )
                    )
                )


        every {
            itemDao.getIdentifierMappingsRO(any())
        } returns mapOf(
            Tokenized("doi", "10.5555/12345678") to IdentifierPkItemPk(1000, 100),
            // These two map to the same Item.
            Tokenized("doi", "10.6666/8764321") to IdentifierPkItemPk(1001, 101),
            Tokenized("doi", "10.7777/567567567") to IdentifierPkItemPk(1002, 101)
        )

        val result = resolver.resolveRO(input)

        val expected = Item().withPk(100)
            .withIdentifiers(
                listOf(
                    Identifier(
                        "http://dx.doi.org/10.5555/12345678",
                        tokenized = Tokenized("doi", "10.5555/12345678")
                    ).withPk(1000)
                )
            )
            .withRelationships(
                listOf(
                    Relationship(
                        "citation",
                        Item().withPk(101).withIdentifiers(
                            listOf(
                                Identifier(
                                    "http://dx.doi.org/10.6666/8764321",
                                    tokenized = Tokenized("doi", "10.6666/8764321")
                                ).withPk(1001),
                                Identifier(
                                    "http://dx.doi.org/10.7777/567567567",
                                    tokenized = Tokenized("doi", "10.7777/567567567")
                                ).withPk(1002)
                            )
                        )
                    )
                )
            )


        assertEquals(
            expected,
            result,
            "When there are multiple ItemIdentifiers for the same Item, and they agree on the Item PK, the Item should be resolved with that PK."
        )
    }

    /**
     * If an Item has multiple ItemIdentifiers, but they don't all point to the same Item, that Item should no tbe resolved.
     */
    @Test
    fun `resolveReadOnly should not resolve Items where there are multiple ItemIdentifiers in conflict`() {
        // The root has a single identifier, but the next one has two ItemIdentifiers.
        val input =
            Item()
                .withIdentifiers(
                    listOf(
                        Identifier(
                            "http://dx.doi.org/10.5555/12345678",
                            tokenized = Tokenized("doi", "10.5555/12345678")
                        )
                    )
                )
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withIdentifiers(
                                listOf(
                                    Identifier(
                                        "http://dx.doi.org/10.6666/8764321",
                                        tokenized = Tokenized("doi", "10.6666/8764321")
                                    ),
                                    Identifier(
                                        "http://dx.doi.org/10.7777/567567567",
                                        tokenized = Tokenized("doi", "10.7777/567567567")
                                    )
                                )
                            )
                        )
                    )
                )

        every {
            itemDao.getIdentifierMappingsRO(any())
        } returns mapOf(
            Tokenized("doi", "10.5555/12345678") to IdentifierPkItemPk(1000, 100),

            // These two map to different Item PKs!
            Tokenized("doi", "10.6666/8764321") to IdentifierPkItemPk(1001, 101),
            Tokenized("doi", "10.7777/567567567") to IdentifierPkItemPk(1002, 999)
        )

        val result = resolver.resolveRO(input)

        val expected = Item().withPk(100)
            .withIdentifiers(
                listOf(
                    Identifier(
                        "http://dx.doi.org/10.5555/12345678",
                        tokenized = Tokenized("doi", "10.5555/12345678")
                    ).withPk(1000)
                )
            )
            .withRelationships(
                listOf(
                    Relationship(
                        "citation",
                        // This whole item is left untouched because the ItemIdentifiers don't agree.
                        Item().withIdentifiers(
                            listOf(
                                Identifier(
                                    "http://dx.doi.org/10.6666/8764321",
                                    tokenized = Tokenized("doi", "10.6666/8764321")
                                ),
                                Identifier(
                                    "http://dx.doi.org/10.7777/567567567",
                                    tokenized = Tokenized("doi", "10.7777/567567567")
                                )
                            )
                        )
                    )
                )
            )

        assertEquals(
            expected,
            result,
            "When there are multiple ItemIdentifiers for the same Item, and they don't agree on the Item PK, the Item and its ItemIdentifiers should not be resolved."
        )
    }

    /**
     * If an Item has multiple ItemIdentifiers, and one of them doesn't resolve, then skip.
     * We'll need to do it via Read-Write resolution.
     */
    @Test
    fun `resolveReadOnly should not resolve Items where there are multiple ItemIdentifiers with any unknown`() {
        // The root has a single identifier, but the next one has two ItemIdentifiers.
        val input =
            Item()
                .withIdentifiers(
                    listOf(
                        Identifier(
                            "http://dx.doi.org/10.5555/12345678",
                            tokenized = Tokenized("doi", "10.5555/12345678")
                        )
                    )
                )
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withIdentifiers(
                                listOf(
                                    Identifier(
                                        "http://dx.doi.org/10.6666/8764321",
                                        tokenized = Tokenized("doi", "10.6666/8764321")
                                    ),
                                    Identifier(
                                        "http://dx.doi.org/10.7777/567567567",
                                        tokenized = Tokenized("doi", "10.7777/567567567")
                                    )
                                )
                            )
                        )
                    )
                )

        every {
            itemDao.getIdentifierMappingsRO(any())
        } returns mapOf(
            Tokenized("doi", "10.5555/12345678") to IdentifierPkItemPk(1000, 100),

            // One of the ItemIdentifiers is known, but "http://dx.doi.org/10.7777/567567567" isn't.
            Tokenized("doi", "10.6666/8764321") to IdentifierPkItemPk(1001, 101)
        )

        val result = resolver.resolveRO(input)

        val expected = Item().withPk(100)
            .withIdentifiers(
                listOf(
                    Identifier(
                        "http://dx.doi.org/10.5555/12345678",
                        tokenized = Tokenized("doi", "10.5555/12345678")
                    ).withPk(1000)
                )
            )
            .withRelationships(
                listOf(
                    Relationship(
                        "citation",
                        // This whole item is left untouched because the ItemIdentifiers don't unambiguously point to an Item.
                        // If it did attach some ItemIdentifiers and not others, read-write wouldn't be able to create the missing ones and retrieve the existing ones in the same transaction.
                        // And that's important in order t  o be able to roll back / retry conflicts.
                        Item().withIdentifiers(
                            listOf(
                                Identifier(
                                    "http://dx.doi.org/10.6666/8764321",
                                    tokenized = Tokenized("doi", "10.6666/8764321")
                                ),
                                Identifier(
                                    "http://dx.doi.org/10.7777/567567567",
                                    tokenized = Tokenized("doi", "10.7777/567567567")
                                )
                            )
                        )
                    )
                )
            )

        assertEquals(
            expected,
            result,
            "When there are multiple ItemIdentifiers for the same Item, and one or more isn't known, the Item and its ItemIdentifiers should not be resolved."
        )
    }
}