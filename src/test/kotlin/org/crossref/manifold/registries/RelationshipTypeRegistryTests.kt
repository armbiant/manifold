package org.crossref.manifold.registries

import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.crossref.manifold.itemgraph.RelationshipDao
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.FileNotFoundException

internal class RelationshipTypeRegistryTests {
    private val daoShared = mockk<RelationshipDao>()

    @BeforeEach
    fun reset() {
        clearMocks(daoShared)
    }

    @Test
    /**
     * This is a regression test for the file itself, to make sure what's checked in remains valid.
     */
    fun `load should succeed for production schema file`() {
        val relationshipDao = mockk<RelationshipDao>()

        // Constructor with default filename argument.
        val registry = RelationshipTypeRegistry(relationshipDao)

        // Because this uses a real file, we don't want to couple behaviour to the data by expecting real values.
        every { relationshipDao.resolveRelationshipType(any()) } returns 1

        Assertions.assertDoesNotThrow {
            registry.load()
        }
    }

    @Test
    fun `load should fail when schema file missing`() {
        val filename = "DOES_NOT_EXIST.schema.json"

        val registry = RelationshipTypeRegistry(daoShared, filename)

        Assertions.assertThrows(FileNotFoundException::class.java) {
            registry.load()
        }
    }

    /**
     * The root node of the schema must be the expected type, because the logic of the Registry
     * is coupled to the structure of the file.
     */
    @Test
    fun `load should fail when the root node is the wrong type`() {
        val filename = "schema/relationships.schema.bad-root.json"
        val registry = RelationshipTypeRegistry(daoShared, filename)

        val exception = Assertions.assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("root item"))
    }

    @Test
    fun `load should fail when the missing entry value`() {
        val filename = "schema/relationships.schema.missing-const.json"

        val registry = RelationshipTypeRegistry(daoShared, filename)

        every { daoShared.resolveRelationshipType("funds") } returns 2

        val exception = Assertions.assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("Missing 'const' key"))
    }

    @Test
    fun `load should fail when there's a duplicate relationship`() {
        val filename = "schema/relationships.schema.duplicate.json"

        val registry = RelationshipTypeRegistry(daoShared, filename)

        every { daoShared.resolveRelationshipType("cites") } returns 1
        every { daoShared.resolveRelationshipType("funds") } returns 2

        val exception = Assertions.assertThrows(Exception::class.java) {
            registry.load()
        }

        Assertions.assertTrue(exception.message!!.contains("Duplicate"))
    }

    /** Registry should load from the schema file and then allow us to resolve relationship types
     * against it.
     */
    @Test
    fun `load should register canonical identifiers with dao`() {
        val filename = "schema/relationships.schema.good.json"

        val relationshipDao = mockk<RelationshipDao>()
        val registry = RelationshipTypeRegistry(relationshipDao, filename)

        every { relationshipDao.resolveRelationshipType("cites") } returns 1
        every { relationshipDao.resolveRelationshipType("funds") } returns 2

        registry.load()

        // Ensure that every entry from the registry was registered.
        verify { relationshipDao.resolveRelationshipType("cites") }
        verify { relationshipDao.resolveRelationshipType("funds") }

        // And now check that the registry can be used with the loaded data.
        assertEquals(
            1,
            registry.resolve("cites"),
            "Should be able to resolve a known relationship type to its PK from the DAO."
        )

        assertEquals(
            2,
            registry.resolve("funds"),
            "Should be able to resolve a known relationship type to its PK from the DAO."
        )

        assertEquals(
            null,
            registry.resolve("NONEXISTENT"),
            "Should return null for unknown relationship type."
        )

        assertEquals(
            setOf(2L, 1L),
            registry.resolveMany(setOf("cites", "funds")),
            "Should be able to resolve a set of PKs to an unordered list of values."
        )

        assertEquals(
            "cites", registry.reverse(1),
            "Should be able to reverse a known PK back to its value."
        )

        assertEquals(
            "funds", registry.reverse(2),
            "Should be able to reverse a known PK back to its value."
        )

        // It's much more of a problem if we get a PK out of the database for which we don't know the corresponding value!
        val exception = Assertions.assertThrows(Exception::class.java) {
            registry.reverse(999)
        }
        assertTrue(exception.message!!.contains("Didn't recognise Relationship Type PK"))

    }
}