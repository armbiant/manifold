package org.crossref.manifold.registries

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.crossref.manifold.ConflictException
import org.crossref.manifold.itemgraph.IdentifierTypeDao
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.itemtree.Tokenized
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.FileNotFoundException

internal class IdentifierTypeRegistryTest {
    // This mock is used for tests of logic that doesn't interact with the DAO.
    private val identifierTypeDaoShared = mockk<IdentifierTypeDao>()

    @BeforeEach
    fun setup() {
        every { identifierTypeDaoShared.resolve("doi", false) } returns 1
        every { identifierTypeDaoShared.resolve("orcid", false) } returns 2
        every { identifierTypeDaoShared.resolve("somethingElse", false) } returns 3
        every { identifierTypeDaoShared.resolve("cr-item", false) } returns 4
    }

    @Test
    fun `registerCanonicalIdentifierType should allow re-registration of same pair`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)

        assertDoesNotThrow {
            registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)
        }
    }

    @Test
    fun `registerCanonicalIdentifierType should disallow re-mapping of prefix to different URI`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)

        assertThrows(ConflictException::class.java) {
            registry.registerCanonicalIdentifierType("doi", "http://orcid.org/", false)
        }
    }

    @Test
    fun `registerCanonicalIdentifierType should disallow re-mapping of URI to different prefix`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)

        assertThrows(ConflictException::class.java) {
            registry.registerCanonicalIdentifierType("orcid", "http://dx.doi.org/", false)
        }
    }

    @Test
    fun `registerCanonicalIdentifierType should disallow re-mapping of two prior mapped pairs`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)
        registry.registerCanonicalIdentifierType("orcid", "http://orcid.org/", false)

        assertThrows(ConflictException::class.java) {
            registry.registerCanonicalIdentifierType("orcid", "http://dx.doi.org/", false)
        }
    }

    @Test
    fun `registerSupplementaryInputMapping should disallow re-mapping of URIs`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)
        registry.registerSupplementaryInputMapping("doi", "http://dx.doi.org/")

        // This should be fine because it's a different URI.
        registry.registerSupplementaryInputMapping("doi", "http://doi.org/")

        // But this one maps an existing URI to a different prefix label, which is not allowed.
        assertThrows(ConflictException::class.java) {
            registry.registerSupplementaryInputMapping("somethingElse", "http://dx.doi.org/")
        }
    }

    @Test
    fun `should split ItemIdentifiers by registered canonical namespace prefix`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)

        // Register a couple to ensure it works with irrelevant entries too.
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)
        registry.registerCanonicalIdentifierType("orcid", "http://orcid.org/", false)

        val doiResult = registry.tokenizePersistentIdentifier("http://dx.doi.org/10.5555/12345678")
        assertEquals(Tokenized("doi", "10.5555/12345678"), doiResult)

        val orcidResult = registry.tokenizePersistentIdentifier("http://orcid.org/123456789")
        assertEquals(Tokenized("orcid", "123456789"), orcidResult)
    }

    @Test
    fun `should split ItemIdentifiers by registered supplementary namespace prefix`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)

        // Register a couple to ensure it works with irrelevant entries too.
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)
        registry.registerSupplementaryInputMapping("doi", "https://doi.org/")

        registry.registerCanonicalIdentifierType("orcid", "http://orcid.org/", false)
        registry.registerSupplementaryInputMapping("orcid", "https://orcid.org/")

        // Canonical works
        val doiCanonicalResult = registry.tokenizePersistentIdentifier("http://dx.doi.org/10.5555/12345678")
        assertEquals(Tokenized("doi", "10.5555/12345678"), doiCanonicalResult)

        // As does supplementary
        val doiSupplementaryResult = registry.tokenizePersistentIdentifier("https://doi.org/10.5555/12345678")
        assertEquals(Tokenized("doi", "10.5555/12345678"), doiSupplementaryResult)

        // Ditto for others in the same map.
        val orcidCanonicalResult = registry.tokenizePersistentIdentifier("http://orcid.org/123456789")
        assertEquals(Tokenized("orcid", "123456789"), orcidCanonicalResult)

        val orcidSupplementaryResult = registry.tokenizePersistentIdentifier("https://orcid.org/123456789")
        assertEquals(Tokenized("orcid", "123456789"), orcidSupplementaryResult)
    }

    @Test
    fun `should combine pairs back into canonical representations`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)

        // Register a couple to ensure it works with irrelevant entries too.
        registry.registerCanonicalIdentifierType("doi", "http://dx.doi.org/", false)
        registry.registerSupplementaryInputMapping("doi", "https://doi.org/")

        // Although two mappings can be translated into the same pair...
        val doiCanonicalResult = registry.tokenizePersistentIdentifier("http://dx.doi.org/10.5555/12345678")
        val doiSupplementaryResult = registry.tokenizePersistentIdentifier("https://doi.org/10.5555/12345678")
        assertEquals(doiCanonicalResult, doiSupplementaryResult)

        // The translation back goes only to the canonical representation.
        assertEquals("http://dx.doi.org/10.5555/12345678", registry.tokenizedToUri(doiCanonicalResult!!))
        assertEquals("http://dx.doi.org/10.5555/12345678", registry.tokenizedToUri(doiSupplementaryResult!!))
    }

    @Test
    fun `split should fail if the prefix is unrecognised`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)

        // Only irrelevant entries here.
        registry.registerCanonicalIdentifierType("orcid", "http://orcid.org/", false)
        registry.registerSupplementaryInputMapping("orcid", "https://orcid.org/")

        assertNull(registry.tokenizePersistentIdentifier("http://dx.doi.org/10.5555/12345678"))
    }

    @Test
    fun `expand should fail if the prefix is unrecognised`() {
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)

        // Only irrelevant entries here.
        registry.registerCanonicalIdentifierType("orcid", "http://orcid.org/", false)
        registry.registerSupplementaryInputMapping("orcid", "https://orcid.org/")

        assertThrows(Exception::class.java) {
            registry.tokenizedToUri(Tokenized("doi", "10.5555/12345678"))
        }
    }

    @Test
    fun `resolveItemTree should resolve all nodes`() {
        val unresolved =
            Item().withIdentifiers(listOf(Identifier("http://orcid.org/987654321")))
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withIdentifiers(listOf(Identifier("http://dx.doi.org/10.6666/8764321")))
                                .withRelationships(
                                    listOf(
                                        Relationship(
                                            "discusses",
                                            Item()
                                                // This one uses a supplementary mapping.
                                                .withIdentifiers(listOf(Identifier("https://doi.org/10.0000/0000")))
                                                .withRelationships(
                                                    listOf(
                                                        Relationship(
                                                            "citation",
                                                            // An Item with no ItemIdentifiers should be fine.
                                                            Item()
                                                        ),
                                                        Relationship(
                                                            "citation",
                                                            // An Item with unrecognised identifier type.
                                                            Item().withIdentifier(Identifier("http://www.example.com"))
                                                        )
                                                    )
                                                )
                                        )
                                    )
                                )
                        )
                    )
                )

        val expected =
            Item().withIdentifiers(
                listOf(
                    Identifier("http://orcid.org/987654321").withTokenized(
                        Tokenized(
                            "orcid",
                            "987654321"
                        )
                    )
                )
            )
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withIdentifiers(
                                listOf(
                                    Identifier("http://dx.doi.org/10.6666/8764321").withTokenized(
                                        Tokenized("doi", "10.6666/8764321")
                                    )
                                )
                            )
                                .withRelationships(
                                    listOf(
                                        Relationship(
                                            "discusses",
                                            Item()
                                                .withIdentifiers(
                                                    listOf(
                                                        Identifier("https://doi.org/10.0000/0000").withTokenized(
                                                            Tokenized("doi", "10.0000/0000")
                                                        )
                                                    )
                                                )
                                                .withRelationships(
                                                    listOf(
                                                        Relationship(
                                                            "citation",
                                                            Item()
                                                        ),
                                                        Relationship(
                                                            "citation",
                                                            // An Item with identifier unrecognised.
                                                            Item().withIdentifier(
                                                                Identifier("http://www.example.com").withTokenized(
                                                                    Tokenized(
                                                                        NON_PERSISTENT, "http://www.example.com"
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                        )
                                    )
                                )
                        )
                    )
                )

        val registry = IdentifierTypeRegistry(identifierTypeDaoShared)
        registry.registerCanonical(listOf(Triple("doi", "http://dx.doi.org/", false), Triple("orcid", "http://orcid.org/", false)))
        registry.registerSupplementary(listOf("doi" to "https://doi.org/"))
        val result = registry.tokenizeIdentifiersIn(unresolved)

        assertEquals(expected, result, "Expected Item Tree with all ItemIdentifiers converted to structured")
    }

    @Test
    /**
     * This is a regression test for the file itself, to make sure what's checked in remains valid.
     */
    fun `load should succeed for production schema file`() {
        val identifierTypeDao = mockk<IdentifierTypeDao>()

        // Constructor with default filename argument.
        val registry = IdentifierTypeRegistry(identifierTypeDao)

        // Because this uses a real file, we don't want to couple behaviour to the data by expecting real values.
        every { identifierTypeDao.resolve(any(), any()) } returns 1

        assertDoesNotThrow {
            registry.load()
        }
    }

    @Test
    fun `load should fail when schema file missing`() {
        val filename = "DOES_NOT_EXIST.schema.json"

        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        assertThrows(FileNotFoundException::class.java) {
            registry.load()
        }
    }

    /**
     * The root node of the schema must be the expected type, because the logic of the Registry
     * is coupled to the structure of the file.
     */
    @Test
    fun `load should fail when the root node is the wrong type`() {
        val filename = "schema/identifiers.schema.bad-root.json"
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        val exception = assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("root item"))
    }

    /** Each item can be either a primary or an alias, but not both.
     */
    @Test
    fun `load should fail when there's a prefix and alias on the same item`() {
        val filename = "schema/identifiers.schema.prefix-alias-conflict.json"
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        val exception = assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("not both"))
    }

    @Test
    fun `load should fail when there's an item with missing prefix or alias`() {
        val filename = "schema/identifiers.schema.prefix-alias-missing.json"
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        val exception = assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("must be supplied"))
    }

    @Test
    fun `load should fail when there's a duplicate prefix`() {
        val filename = "schema/identifiers.schema.duplicate-prefix.json"

        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        val exception = assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("Duplicate Prefix"))
    }

    @Test
    fun `load should fail when there's duplicate URI patterns`() {
        val filename = "schema/identifiers.schema.duplicate-uri.json"

        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        val exception = assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("Duplicate URI"))
    }

    @Test
    fun `load should fail when there's an item with an invalid URI pattern`() {
        val filename = "schema/identifiers.schema.bad-pattern.json"
        val registry = IdentifierTypeRegistry(identifierTypeDaoShared, filename)

        val exception = assertThrows(Exception::class.java) {
            registry.load()
        }

        assertTrue(exception.message!!.contains("Pattern must be present and start with"))
    }

    @Test
    fun `load should register canonical identifiers with dao`() {
        val filename = "schema/identifiers.schema.mix.json"

        val identifierTypeDao = mockk<IdentifierTypeDao>()
        val registry = IdentifierTypeRegistry(identifierTypeDao, filename)

        every { identifierTypeDao.resolve("cr-item", false) } returns 1
        every { identifierTypeDao.resolve("ror", false) } returns 2
        every { identifierTypeDao.resolve("doi", false) } returns 3

        registry.load()

        verify { identifierTypeDao.resolve("cr-item", false) }
        verify { identifierTypeDao.resolve("ror", false) }
        verify { identifierTypeDao.resolve("doi", false) }

        // Now use the file to do some tokenization.
        assertEquals(
            Tokenized("doi", "10.5555/12345678"),
            registry.tokenize("https://doi.org/10.5555/12345678"),
            "When a primary and secondary mappings are registered for DOI, DOI URIs can be tokenized."
        )

        assertEquals(
            Tokenized("doi", "10.5555/12345678"),
            registry.tokenize("https://dx.doi.org/10.5555/12345678"),
            "When a primary and secondary mappings are registered for DOI, DOI URIs using an aliased format can be tokenized."
        )

        assertEquals(
            Tokenized("doi", "10.5555/12345678"),
            registry.tokenize("https://doi.org/10.5555/12345678"),
            "When reversing tokenized to URL, the primary is used."
        )

        assertEquals(
            "https://doi.org/10.5555/12345678",
            registry.tokenizedToUri(
                Tokenized("doi", "10.5555/12345678")
            ),

            "When reversing tokenized to URL, the primary is used."
        )

        assertEquals(
            Identifier("https://doi.org/10.5555/12345678", Tokenized("doi", "10.5555/12345678"), 55),
            registry.tokenizedToIdentifier(Tokenized("doi", "10.5555/12345678"), 55),
            "Can convert Tokenized back to Identifier using registered prefixes."
        )
    }
}