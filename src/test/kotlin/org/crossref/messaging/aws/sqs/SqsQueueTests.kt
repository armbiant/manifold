package org.crossref.messaging.aws.sqs

import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import software.amazon.awssdk.services.sqs.model.SendMessageRequest
import software.amazon.awssdk.services.sqs.model.SendMessageResponse
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

class SqsQueueTests {
    private var sqsAsyncClient: SqsAsyncClient = mockk()
    private val queueName = "testQueue"
    private val queueUrl = "http://testQueue"
    private val messagePayload = "This is the message payload"
    private val message = MessageBuilder.withPayload(messagePayload).build()
    private val sqsQueue = SqsQueue(sqsAsyncClient, queueName, queueUrl)


    @BeforeEach
    fun `reset mocks`() {
        clearAllMocks()
    }

    @Test
    fun `valid request is sent`() {
        val sendMessageRequestSlot = slot<SendMessageRequest>()
        every {
            sqsAsyncClient.sendMessage(capture(sendMessageRequestSlot))
                .get(MessageChannel.INDEFINITE_TIMEOUT, TimeUnit.MILLISECONDS)
        } returns mockk()
        sqsQueue.send(message)
        assertEquals(sendMessageRequestSlot.captured.queueUrl(), queueUrl)
        assertEquals(sendMessageRequestSlot.captured.messageBody(), messagePayload)
    }

    @Test
    fun `successful send returns true`() {
        every {
            sqsAsyncClient.sendMessage(any<SendMessageRequest>())
                .get(MessageChannel.INDEFINITE_TIMEOUT, TimeUnit.MILLISECONDS)
        } returns mockk()
        assertTrue(sqsQueue.send(message))
    }

    @Test
    fun `timed out send returns false`() {
        every { sqsAsyncClient.sendMessage(any<SendMessageRequest>()) } answers {
            CompletableFuture.supplyAsync<SendMessageResponse> {
                Thread.sleep(1000)
                mockk()
            }
        }
        assertFalse(sqsQueue.send(message, 500))
    }
}
