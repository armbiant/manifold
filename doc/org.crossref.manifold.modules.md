# Package org.crossref.manifold.modules

All product features are implemented as Modules, i.e. a sub-package of this namespace. They integrate with Manifold by
registering a Manifest object and by implementing one of:

- Hooks, e.g. on startup or shutdown.
- REST API endpoints that transform Item Trees into responses.
- Ingestion APIs that insert Item Trees.
- Internal Event Listeners that may insert Item Trees.
- Registration of vocabularies of Relationship Types.
- Content Negotiation Representation Types.
- Translations out of, and into, external vocabularies.

Modules shouldn't do much besides translate data in and out of the Item Graph. If a module doesn't touch the Item Graph,
or needs extra database tables, it probably shouldn't be here.
